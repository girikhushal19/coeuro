const mongoose = require("mongoose");

const bannerSchema = new mongoose.Schema({
  images: { type: Array, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("banners", bannerSchema);
const mongoose = require("mongoose");
const Schema = require("mongoose");

const ForumSchema = new mongoose.Schema({
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  title: { type: String, default: null },
  description: { type: String, default: null },
  status: { type: Number, default: 1 },
  like: { type: Number, default: 0 },
  dislike: { type: Number, default: 0 },
  numberOfViews: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("forum", ForumSchema);
const mongoose = require("mongoose");
const Schema = require("mongoose");

const TermsConditionSchema = new mongoose.Schema({
  description: { type: String, default: null },
  
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("termsconditions", TermsConditionSchema);
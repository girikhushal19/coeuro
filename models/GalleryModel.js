const mongoose = require("mongoose");

const gallerySchema = new mongoose.Schema({
  images: { type: Array, default: null },
  file_type: { type: Number, default: 1 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("gallery", gallerySchema);
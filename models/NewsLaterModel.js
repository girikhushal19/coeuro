const mongoose = require("mongoose");
const Schema = require("mongoose");

const NewslaterSchema = new mongoose.Schema({
  email: { type: String, default: null },
  created_at:{type:Date,default:Date.now} 
});

 
module.exports = mongoose.model("newslater", NewslaterSchema);
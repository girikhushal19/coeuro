const mongoose = require("mongoose");

const colorSchema = new mongoose.Schema({
  color_name: { type: String, default: null },
  color_code: { type: String, default: null },
  status: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("colors", colorSchema);
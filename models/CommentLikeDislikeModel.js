const mongoose = require("mongoose");
const Schema = require("mongoose");

const CommentLikeDislikeSchema = new mongoose.Schema({
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  comment_id: [{ type: Schema.Types.ObjectId,ref:"comments" }],
  like_or_dislike: { type: Number, default: 1 },//1 = Like 2 = Dislike
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("commentlikedislike", CommentLikeDislikeSchema);
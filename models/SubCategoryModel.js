const mongoose = require("mongoose");
const Schema = require("mongoose");

const SubCategorySchema = new mongoose.Schema({
  category: [{ type: Schema.Types.ObjectId,ref:"category" }],
  price: { type: String, default: null },
  images: { type: Array, default: null },
  title: { type: String, default: null },
  description: { type: String, default: null },
  video_title: { type: Array, default: null },
  video: { type: Array, default: null },
  heading: { type: String, default: null },
  donation_type: { type: Number, default: 1 }, // 1=Price 2 = quantity
  status: { type: Number, default: 1 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("subcategory", SubCategorySchema);
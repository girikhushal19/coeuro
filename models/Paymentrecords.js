const mongoose = require("mongoose");

const Schema = require("mongoose");
const PaymentrecordsSchema = new mongoose.Schema({
  project_id: { type: String, default: null },
  project_name: { type: String, default: null },
  order_id: { type: Number, default: 0 },
  payment_id: { type: String, default: null },
  firstName: { type: String, default: null },
  lastName: { type: String, default: null },
  message: { type: String, default: null },
  secondFirstName: { type: String, default: null },
  secondLastName: { type: String, default: null },
  address: { type: String, default: null },
  addressLocation: {
   type: { type: String, default: null },
   coordinates: []
  },
  secondAddress: { type: String, default: null },
  secondLocation: {
   type: { type: String, default: null },
   coordinates: []
  },
  city: { type: String, default: null },
  zip_code: { type: String, default: null },
  pays: { type: String, default: null },

  email: { type: String, default: null },
  phone_code: { type: String, default: null },
  mobileNumber: { type: String, default: null },
  pays: { type: String, default: null },
  company_name: { type: String, default: null },
  category_id: [{ type: Schema.Types.ObjectId,ref:"category", default: null }],
  subcategory_id: [{ type: Schema.Types.ObjectId,ref:"subcategory", default: null }],
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  payment_mode: { type: String, default: null },
  category_title: { type: String, default: null },
  subcategory_title: { type: String, default: null },
  price: { type: String, default: null },
  is_additional: { type: Boolean, default: false }, // if yes then true , other wise false
  total_price: { type: String, default: null },
  quantity: { type: Number, default: 0 },
  description: { type: String, default: null },
  filePathCopy: { type: String, default: null },
  whatsappNumber: { type: Number, default: 0 },
  payment_type: { type: String, default: null },
  transaction_id: { type: String, default: null },
  payment_intent: { type: String, default: null },
  status: { type: Number, default: 0 },
  payment_initiate_date: { type: Date, default: Date.now },
  payment_success_date: { type: Date, default: null },
  countryCode: { type: String, default: null },
  whatsAppCountryCode: { type: String, default: null },
  firstName1: { type: String, default: null },
  lastName1: { type: String, default: null },
  firstName2: { type: String, default: null },
  lastName2: { type: String, default: null },
  firstName3: { type: String, default: null },
  lastName3: { type: String, default: null },
  firstName4: { type: String, default: null },
  lastName4: { type: String, default: null },
  firstName5: { type: String, default: null },
  lastName5: { type: String, default: null },
  firstName6: { type: String, default: null },
  lastName6: { type: String, default: null },
  is_invoice: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("paymentrecords", PaymentrecordsSchema);
const mongoose = require("mongoose");
const Schema = require("mongoose");

const ProjectSchema = new mongoose.Schema({
  category: [{ type: Schema.Types.ObjectId,ref:"category", default: null }],
  title: { type: String, default: null },
  smallDescription: { type: String, default: null },
  description: { type: String, default: null },
  images: { type: Array, default: null },   
  status: { type: Number, default: 1 },
  project_type: { type: Number, default: 0 }, // 0=Free 1 = Price
  is_price: { type: Number, default: 0 }, // 0=Free 1 = Price
  allTitle: { type: Array, default: [] },
  price: { type: Array, default: [] },   
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("project", ProjectSchema);
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  user_type: { type: String, enum: ['normal_user', 'pro_user'], default: 'normal_user' },
  firstName: { type: String, default: null },
  lastName: { type: String, default: null },
  email: { type: String, unique: true },
  userName: { type: String, unique: true },
  password: { type: String },
  token: { type: String, default: null },
  deviceToken: { type: String, default: null },
  status: { type: Number, default: 1 },
  deleted_at: { type: Number, default: 0 },
  loggedIn: { type: Number, default: 0 },

  countryCode: { type: String, default: null },
  mobileNumber: { type: Number, default: null },
  whatsAppCountryCode: { type: String, default: null },
  whatsAppMobileNumber: { type: Number, default: null },

  userImage: { type: String, default: null },
  city: { type: String, default: null },
  description: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("users", userSchema);
const mongoose = require("mongoose");
const Schema = require("mongoose");

const PushNotificationSchema = new mongoose.Schema({
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  title: { type: String, default: null },
  description: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("pushnotification", PushNotificationSchema);
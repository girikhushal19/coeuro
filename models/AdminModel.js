const mongoose = require("mongoose");

const adminSchema = new mongoose.Schema({
  firstName: { type: String, default: null },
  lastName: { type: String, default: null },
  email: { type: String, unique: true },
  userName: { type: String, unique: true },
  password: { type: String },
  token: { type: String, default: null },
  status: { type: Number, default: 0 },
  adminImage: { type: String, default: null },
  loggedin_time: { type: Date, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("adminusers", adminSchema);
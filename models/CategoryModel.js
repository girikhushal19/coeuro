const mongoose = require("mongoose");
const Schema = require("mongoose");

const CategorySchema = new mongoose.Schema({
  category: { type: String, default: null },
  price: { type: Number, default: null },
  images: { type: Array, default: null },
  video_title: { type: Array, default: null },
  video: { type: Array, default: null },
  heading: { type: String, default: null },
  description: { type: String, default: null },
  status: { type: Number, default: 1 },
  donation_type: { type: Number, default: 1 }, // 1=Price 2 = quantity
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("category", CategorySchema);
const mongoose = require("mongoose");
const Schema = require("mongoose");

const AdViewsSchema = new mongoose.Schema({
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  forum_id: [{ type: Schema.Types.ObjectId,ref:"forum" }],
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("adviews", AdViewsSchema);
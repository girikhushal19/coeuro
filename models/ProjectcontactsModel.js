const mongoose = require("mongoose");

const ProjectcontactsSchema = new mongoose.Schema({
  name: { type: String, default: null },
  email: { type: String, default: null },
  countryCode: { type: String, default: null },
  phone: { type: String, default: null },
  subject: { type: String, default: null },
  message: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("projectcontacts", ProjectcontactsSchema);
const mongoose = require("mongoose");
const Schema = require("mongoose");

const CommentsSchema = new mongoose.Schema({
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  forum_id: [{ type: Schema.Types.ObjectId,ref:"forum" }],
  comment: { type: String, default: null },
  status: { type: Number, default: 1 },
  like: { type: Number, default: 0 },
  dislike: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("comments", CommentsSchema);
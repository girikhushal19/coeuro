const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const SubCategoryModel = require("../../models/SubCategoryModel");
const ProjectModel = require("../../models/ProjectModel");
const NewsLaterModel = require("../../models/NewsLaterModel");

const GalleryModel = require("../../models/GalleryModel");
const ForumModel = require("../../models/ForumModel");
const BannerModel = require('../../models/BannerModel');
const PushNotificationModel = require('../../models/PushNotificationModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    host: 'mail.coeurorphelins.org',
    port: 465,
    auth: {
       user: 'noreply@coeurorphelins.org',
       pass: '*Ah}PRPwh=hQ'
    }
});
/*/*/
module.exports = {
  getBannerHomePage:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
      //console.log("get profile");
      try{
        //console.log(user_id);
        var categoryRecord = await BannerModel.find({}); 
        //console.log("categoryRecord"+categoryRecord);
        //console.log("categoryRecord"+categoryRecord.length);
        if(categoryRecord)
        {
          if(categoryRecord.length === 0)
          {
            res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
          }else{
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    base_url:base_url,
                    errorMessage: "Dossier de catégorie",
                    record:categoryRecord,
                });
          }
        }else{
          console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
        }
        
      }catch(err){
        console.log(err);
      }
  },
  getCategory:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
      //console.log("get profile");
      try{
        //console.log(user_id);
        await CategoryModel.find({ status:1 }).lean(true).exec((err, result)=>
        {
          if(err)
          {
            console.log(err);
          }else{
            if(result)
            {
              if(result.length === 0)
              {
                res.status(200)
                    .send({
                        error: true,
                        success: false,
                        base_url:base_url,
                        errorMessage: "Aucun enregistrement"
                    });
              }else{
                //console.log("total val"+result.length);
                var lastVal = result.length - 1;
                var x=0;
                result.forEach(object=>
                {
                  //console.log("xxxxx value"+x);
                  //console.log(object._id);
                  SubCategoryModel.countDocuments({ category:object._id }).exec((err, resultCount)=>
                  {
                    if(err)
                    {
                      console.log(err);
                    }else{
                      //console.log("hereeeeeeeeee");
                      if(resultCount == 0)
                      {
                        //console.log("ifff hereeeeeeeeee");
                        object.subcat_status=false;
                      }else{
                        //console.log("elseee hereeeeeeeeee");
                        object.subcat_status=true;
                      }
                    }
                    if(lastVal == x)
                    {
                      //console.log("lastVal"+lastVal);
                      //console.log(x);
                      //console.log(result);
                      res.status(200)
                        .send({
                            error: false,
                            success: true,
                            base_url:base_url,
                            errorMessage: "Enregistrement de la catégorie",
                            record:result,
                        });
                    }
                    x++;
                  });
                });
                 
              }
            }else{
              //console.log("here");
              res.status(200)
                    .send({
                        error: true,
                        success: false,
                        base_url:base_url,
                        errorMessage: "Pas d'enregistrement"
                    });
            }
          }
        });
         
        
      }catch(err){
        console.log(err);
      }
  },
  getSubCategory:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
      //console.log("get profile");
      try{
        var {category_id} = req.body;
        //console.log(user_id);
        var categoryRecord = await SubCategoryModel.find({ "category":category_id }); 
        //console.log("categoryRecord"+categoryRecord);
        //console.log("categoryRecord"+categoryRecord.length);
        if(categoryRecord)
        {
          if(categoryRecord.length === 0)
          {
            res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
          }else{
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    base_url:base_url,
                    errorMessage: "Enregistrement de la sous-catégorie",
                    record:categoryRecord,
                });
          }
        }else{
          console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
        }
        
      }catch(err){
        console.log(err);
      }
  },
  getProject:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
      //console.log("get profile");
      try{
        //console.log(user_id);
        var categoryRecord = await ProjectModel.find({ status:1,project_type:0 }); 
        //console.log("categoryRecord"+categoryRecord);
        //console.log("categoryRecord"+categoryRecord.length);
        if(categoryRecord)
        {
          if(categoryRecord.length === 0)
          {
            res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
          }else{
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    base_url:base_url,
                    errorMessage: "Dossier du projet",
                    record:categoryRecord,
                });
          }
        }else{
          console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
        }
        
      }catch(err){
        console.log(err);
      }
  },
  getPriceProject:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
      //console.log("get profile");
      try{
        //console.log(user_id);
        var categoryRecord = await ProjectModel.find({ status:1,project_type:1 }); 
        //console.log("categoryRecord"+categoryRecord);
        //console.log("categoryRecord"+categoryRecord.length);
        if(categoryRecord)
        {
          if(categoryRecord.length === 0)
          {
            res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
          }else{
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    base_url:base_url,
                    errorMessage: "Dossier du projet",
                    record:categoryRecord,
                });
          }
        }else{
          console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
        }
        
      }catch(err){
        console.log(err);
      }
  },
  getSingleProject:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
      //console.log("get profile");
      try{
        //console.log(user_id);
        let id = req.params.id;
        var categoryRecord = await ProjectModel.findOne({ _id:id }); 
        //console.log("categoryRecord"+categoryRecord);
        //console.log("categoryRecord"+categoryRecord.length);
        let cc = {};
        let arr = [];
        if(categoryRecord)
        {
          //cc["all_data"] = categoryRecord;
           if(categoryRecord.price)
           {
            //console.log("categoryRecord.price.length ",categoryRecord.price.length);
             
            if(categoryRecord.price.length > 0)
            {
              // console.log("here");
              // let objj = {"all_title":categoryRecord.allTitle,"all_price":categoryRecord.price};
              // categoryRecord.all = objj;
              // console.log("objj ",objj);
              // cc  = objj;
              const myArray_price = categoryRecord.price[0].split(",");
              const myArray_title = categoryRecord.allTitle[0].split(",");
              for(let x=0; x<myArray_price.length; x++)
              {
                let tt = "";
                if(myArray_title.length >= x)
                {
                  tt = myArray_title[x];
                }
                // console.log("x ",x);
                // console.log("tt ",tt);
                // console.log("price ",myArray_price[x]);
                let obj = {title:tt,price:myArray_price[x]};
                arr.push(obj);
              }
            }
           }
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    base_url:base_url,
                    errorMessage: "Dossier du projet",
                    record:categoryRecord,
                    array_record:arr,
                });
           
        }else{
          console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
        }
        
      }catch(err){
        console.log(err);
      }
  },
  getGalleryImage:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
      //console.log("get profile");
      try{
        //console.log(user_id);
        var categoryRecord = await GalleryModel.find({ file_type:1 }); 
        //console.log("categoryRecord"+categoryRecord);
        //console.log("categoryRecord"+categoryRecord.length);
        if(categoryRecord)
        {
          if(categoryRecord.length === 0)
          {
            res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
          }else{
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    base_url:base_url,
                    errorMessage: "Fiche de l'image de la galerie",
                    record:categoryRecord,
                });
          }
        }else{
          console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
        }
        
      }catch(err){
        console.log(err);
      }
  },
  getGalleryVideo:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
      //console.log("get profile");
      try{
        //console.log(user_id);
        var categoryRecord = await GalleryModel.find({ file_type:2 }); 
        //console.log("categoryRecord"+categoryRecord);
        //console.log("categoryRecord"+categoryRecord.length);
        if(categoryRecord)
        {
          if(categoryRecord.length === 0)
          {
            res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
          }else{
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    base_url:base_url,
                    errorMessage: "Enregistrement vidéo de la galerie",
                    record:categoryRecord,
                });
          }
        }else{
          console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
        }
        
      }catch(err){
        console.log(err);
      }
  },
  addCategorySubmit:async function(req,res,next)
  {
      //console.log("get profile");
    try{
      console.log("controller registration method");
      console.log(req.body);
    }catch(err){
        console.log(err);
    }
  },
  getOtherUserProfile:async function(req,res,next)
  {
    //console.log("get profile");
    try{

      var base_url = customConstant.base_url;
      var imagUrl = base_url+"public/uploads/userProfile/";
      var user_id = req.body.user_id;
      var total_like = total_forum = 0;
      //console.log(user_id);
      var userRecord = await UsersModel.findOne({ "_id":user_id }); 
      //console.log("userRecord"+userRecord);
      if(userRecord)
      {
        if(userRecord.lingth === 0)
        {
          res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Détails de l'utilisateur non valides"
              });
        }else{
          var String_qr = {};
          String_qr['user_id'] = user_id;
          //console.log(String_qr);
          ForumModel.find( String_qr , {like:1}).populate().lean(true).exec((err, result)=>
          {
            if(err)
            {
              console.log(err);
            }else{
              /*console.log("result ");
              console.log(typeof result);*/
              //console.log("result"+result);
              total_forum = result.length;
              if(result.length > 0)
              {
                for (let i = 0; i < result.length; i++)
                {
                  //console.log(result[i].like);
                  total_like = total_like + result[i].like;
                }
                var return_response = {"error":false,errorMessage:"Succès",
                    imagUrl:imagUrl,"record":userRecord,"total_forum":total_forum,"total_like":total_like};
                res.status(200).send(return_response);
              }else{
                
                var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
                res.status(200).send(return_response);
              }
            }
          });
          
        }
      }else{
        console.log("here");
        res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Détails de l'utilisateur non valides"
              });
      }
      
    }catch(err){
      console.log(err);
    }
  },
  allPushNotificationApi:async function(req,res,next)
  {
    try{
      PushNotificationModel.find({  }, {}).sort([['created_at', -1]]).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },

  submitEmailSubscribe:async(req,res)=>{
    try{
      var {email} = req.body;
      if(!email)
      {
        return res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "L'adresse électronique est requise"
          });
       
      }
      let is_exist = await NewsLaterModel.findOne({email});
      if(!is_exist)
      {
        NewsLaterModel.create({ 
          email
        }).then((result)=>{
            var html = "";
            //html += "Bonjour "+name+ ","; 
            //html += "Bonjour , Mehdi Jaki";
            //html += "<p>Un nouvel utilisateur s'est inscrit dont les coordonnées sont</p>";
            html += "<p>Email : "+email+"</p>"; 
            const messageee = {
                from: "noreply@coeurorphelins.org Coeur d'Orphelins", // Sender address
                to: "mehdi.jaki@yahoo.com",         // recipients
                //to: "johnwillims789@mailinator.com",         // recipients
                subject: "S'abonner à la lettre d'information", // Subject line
                html: html
            };
            transport.sendMail(messageee, function(err, info) {
                if (err) {
                  console.log(err);
                  //error_have = 1;
                  res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "Succès de l'abonnement au courrier électronique"
                  });
                }else{
                  console.log('mail has sent.');
                  res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "Succès de l'abonnement au courrier électronique"
                  });
                }
            });
            
        }).catch((error)=>{
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: error.message
            });
        });
      }else{
          return res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: "Ce courriel est déjà disponible"
            });
      }
      
    }catch(err){
      res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: err.message
      });
    }
  }
};
const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const SubCategoryModel = require("../../models/SubCategoryModel");
const ProjectModel = require("../../models/ProjectModel");
const GalleryModel = require("../../models/GalleryModel");
const ForumModel = require("../../models/ForumModel");
const CommentsModel = require("../../models/CommentsModel");
const AboutUsModel = require('../../models/AboutUsModel');
const CommentLikeDislikeModel = require('../../models/CommentLikeDislikeModel');
const ForumLikeDislikeModel = require('../../models/ForumLikeDislikeModel');
const AdViewsModel = require('../../models/AdViewsModel');
const PrivacyAndPolicyModel = require('../../models/PrivacyAndPolicyModel');
const TermsAndConditionModel = require('../../models/TermsAndConditionModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
/*/*/
module.exports = {
  addForum:async function(req,res,next)
  {
    try{
      ForumModel.count({ user_id:req.body.user_id,
          title:req.body.title,
          description:req.body.description },(errs,emailCount)=>
      {
        if(errs)
        {
          var return_response = {"error":true,success: false,errorMessage:errs};
              res.status(200)
              .send(return_response);
        }else{
          if(emailCount === 0)
          {
            ForumModel.create({
              user_id:req.body.user_id,
              title:req.body.title,
              description:req.body.description
            },function(err,result){
              if(err)
              {
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                //console.log(result);
                //console.log(result._id);

                var return_response = {"error":false,success: true,errorMessage:"Succès"};
                  res.status(200)
                  .send(return_response);
              }
            });
          }else{
            var return_response = {"error":true,success: false,errorMessage:"Ce forum est déjà prêt"};
              res.status(200)
              .send(return_response);
          }
        }
      });
    }catch(err){
        console.log(err);
         var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
    }
  },
  userAllForum:async function(req,res,next)
  {
    try{
      var user_id = req.body.user_id;
      const queryJoin = [
          {
            path:'user_id',
            select:['firstName','lastName']
          }
        ];
        var String_qr = {};
        String_qr['user_id'] = user_id;
      //console.log(String_qr);
      ForumModel.find( String_qr , {}).populate(queryJoin).sort([['created_at', -1]]).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas de dossier","record":result};
            res.status(200).send(return_response);
          }

        }
      });  
    }catch(err){
        console.log(err);
         var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
    }
  },
  allForum:async function(req,res,next)
  {
    try{
      var title = req.body.title;
      var String_qr = {}; 
      if(title && title != "")
      {
        //{ "abc": { $regex: '.*' + colName + '.*' } }
        //String_qr['title'] = { $regex: '.*' + title + '.*' };
        String_qr['title'] = { $regex: '.*' + title + '.*' };
      }
      var userImagePath = customConstant.base_url+"public/uploads/userProfile/";
      var userDefaultImagePath = customConstant.base_url+"public/uploads/userProfile/user-icon.jpeg";
      var fromindex = 0;
      var user_id = req.body.user_id;
      const queryJoin = [
          {
            path:'user_id',
            select:['firstName','lastName','userImage']
          }
        ]; 
      ForumModel.find(String_qr, {}).populate(queryJoin).sort([['created_at', -1]]).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
             result.forEach(object=>
             {
                CommentsModel.countDocuments({ forum_id:object._id }).exec((err4, commentCount)=>
                {
                  //console.log("countResult "+totalcount);
                  if(err4)
                  {
                    console.log(err4);
                  }else{
                    object.comment_count=commentCount;
                  }
                });

                //console.log(object);
                //console.log(object._id);
                ForumLikeDislikeModel.countDocuments({ forum_id:object._id,like_or_dislike:1 }).exec((err5, commentLikeDislikeCount)=>
                {
                  //console.log("countResult "+totalcount);
                  if(err5)
                  {
                    console.log(err5);
                  }else{
                    //console.log("commentLikeDislikeCount");
                    //console.log(commentLikeDislikeCount);
                    
                      object.like_total_count=commentLikeDislikeCount;
                   
                  }
                });



            })
            //console.log(JSON.stringify(result));


            UsersModel.countDocuments({ status:1 }).exec((err2, allActiveUserCount)=>
            {
              //console.log("countResult "+totalcount);
              if(err2)
              {
                console.log(err2);
              }else{
                UsersModel.countDocuments({ loggedIn:1 }).exec((err3, loggedInUser)=>
                {
                  //console.log("countResult "+totalcount);
                  if(err3)
                  {
                    console.log(err3);
                  }else{
                    CommentsModel.countDocuments({  }).exec((err4, commentCount)=>
                    {
                      //console.log("countResult "+totalcount);
                      if(err4)
                      {
                        console.log(err4);
                      }else{
                        var record = {"userImagePath":userImagePath,"userDefaultImagePath":userDefaultImagePath,"forumCount":result.length,"allUserCount":allActiveUserCount,"loggedInUser":loggedInUser,"commentCount":commentCount };
                        var return_response = {"error":false,errorMessage:"Succès","record":record,"result":result};
                        res.status(200).send(return_response);
                      }
                    });
                  }
                });
              }
            });
            
          }else{
            var return_response = {"error":false,errorMessage:"Pas de dossier","record":result};
            res.status(200).send(return_response);
          }

        }
      });  
    }catch(err){
        console.log(err);
         var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
    }
  },
  singleForum:async function(req,res,next)
  {
    try{
      var userImagePath = customConstant.base_url+"public/uploads/userProfile/";
      var userDefaultImagePath = customConstant.base_url+"public/uploads/userProfile/user-icon.jpeg";
      var fromindex = 0;
      var like_or_dislike = 0;
      var user_id = req.body.user_id;
      var allLikeCounterForum = 0;
      var allDislikeLikeCounterForum = 0;
      var id = req.body.id;

      ForumLikeDislikeModel.findOne({user_id:user_id,forum_id:id},{like_or_dislike:1}).exec((errForumLike,resultForumLike)=>{
      if(errForumLike)
      {
        var return_response = {"error":true,success: false,errorMessage:errForumLike};
          res.status(200)
          .send(return_response);
      }else{
        //console.log(resultForumLike);
        if(resultForumLike)
        {
          like_or_dislike = resultForumLike.like_or_dislike;
        }
        
      }
    });
      ForumLikeDislikeModel.countDocuments({forum_id:id,like_or_dislike:1}).exec((errForumLike,resultForumLikeCounter)=>{
      if(errForumLike)
      {
        var return_response = {"error":true,success: false,errorMessage:errForumLike};
          res.status(200)
          .send(return_response);
      }else{
        //console.log(resultForumLike);
        allLikeCounterForum = resultForumLikeCounter;
        
      }
    });
      ForumLikeDislikeModel.countDocuments({forum_id:id,like_or_dislike:2}).exec((errForumLike,resultForumDisLikeCounter)=>{
      if(errForumLike)
      {
        var return_response = {"error":true,success: false,errorMessage:errForumLike};
          res.status(200)
          .send(return_response);
      }else{
        //console.log(resultForumLike);
        allDislikeLikeCounterForum = resultForumDisLikeCounter;
        
      }
    });
      const queryJoin = [
          {
            path:'user_id',
            select:['firstName','lastName','userImage']
          }
        ]; 
      ForumModel.find({ _id:id },{_id:1,user_id:1,numberOfViews:1}).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{

          /*console.log("result ");
          console.log(typeof result);*/
          /*console.log("result"+JSON.stringify(result));
          console.log("result"+result[0].user_id[0]._id);*/
          if(result.length > 0)
          {
            var owner_id = result[0].user_id[0]._id;
            if(owner_id != user_id)
            {
              
              AdViewsModel.countDocuments({ user_id:user_id,forum_id:id }).exec((err6, adViewCount)=>
              {
                //console.log("countResult "+totalcount);
                if(err6)
                {
                  console.log(err6);
                }else{
                  //console.log("adViewCount");
                  //console.log(adViewCount);
                  if(adViewCount == 0)
                  {
                    AdViewsModel.create({
                      user_id:req.body.user_id,
                      forum_id:id
                    },function(err,result){
                      if(err)
                      {
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        //console.log(result);
                        //console.log(result._id);
 
                      }
                    });


                    //console.log("numberOfViews"+result[0].numberOfViews);
                    var views = result[0].numberOfViews + 1;
                    ForumModel.updateOne({ "_id":result[0]._id }, 
                    {numberOfViews:views}, function (uerr, docs) {
                        if (uerr){
                          //console.log(uerr);
                          res.status(200)
                            .send({
                                error: true,
                                success: false,
                                errorMessage: uerr
                            });
                      }else{
                           
                          }
                      });
                  }
                }
              });


              
            }
            CommentsModel.find({ forum_id:id }).populate(queryJoin).sort([['created_at', -1]]).lean(true).exec((err4, commentCount)=>
            {
              //console.log("countResult "+totalcount);
              if(err4)
              {
                console.log(err4);
              }else{

                commentCount.forEach(object=>
                {
                  //console.log(object);
                  //console.log(object._id);
                  CommentLikeDislikeModel.findOne({ comment_id:object._id,user_id:user_id },{like_or_dislike:1}).exec((err5, commentLikeDislikeCount)=>
                  {
                    //console.log("countResult "+totalcount);
                    if(err5)
                    {
                      console.log(err5);
                    }else{
                      //console.log("commentLikeDislikeCount");
                      //console.log(commentLikeDislikeCount);
                      if(commentLikeDislikeCount)
                      {

                        object.like_or_dislikeComment=commentLikeDislikeCount.like_or_dislike;
                      }else{
                        object.like_or_dislikeComment=0;
                      }
                    }
                  });
                  CommentLikeDislikeModel.countDocuments({ comment_id:object._id,like_or_dislike:1 }).exec((err5, commentLikeCount)=>
                  {
                    //console.log("countResult "+totalcount);
                    if(err5)
                    {
                      console.log(err5);
                    }else{
                      //console.log("commentLikeDislikeCount");
                      //console.log(commentLikeDislikeCount);
                      
                      object.commentLikeCount=commentLikeCount;
                    }
                  });
                  CommentLikeDislikeModel.countDocuments({ comment_id:object._id,like_or_dislike:2 }).exec((err5, commentDisLikeCount)=>
                  {
                    //console.log("countResult "+totalcount);
                    if(err5)
                    {
                      console.log(err5);
                    }else{
                      //console.log("commentLikeDislikeCount");
                      //console.log(commentLikeDislikeCount);
                      
                      object.commentDisLikeCount=commentDisLikeCount;
                    }
                  });
                })
                //console.log(JSON.stringify(result));
                /*var comment_like_or_dislike = 0;
                ForumLikeDislikeModel.findOne({user_id:user_id,forum_id:id},{like_or_dislike:1}).exec((errForumLike,resultForumLike)=>{
                  if(errForumLike)
                  {
                    var return_response = {"error":true,success: false,errorMessage:errForumLike};
                      res.status(200)
                      .send(return_response);
                  }else{
                    //console.log(resultForumLike);
                    like_or_dislike = resultForumLike.like_or_dislike;
                  }
                });*/





              ForumModel.find({ _id:id }).populate(queryJoin).lean(true).exec((err, result2)=>
              {
                if(err)
                {
                  console.log(err);
                }else{
                  //console.log(like_or_dislike);
                  var record = {"userImagePath":userImagePath,"userDefaultImagePath":userDefaultImagePath};
                  var return_response = {"error":false,errorMessage:"Succès",
                  "commentCount":commentCount.length,
                  "like_or_dislikeForum":like_or_dislike,
                  "allLikeCounterForum":allLikeCounterForum,
                  "allDislikeLikeCounterForum":allDislikeLikeCounterForum,
                  "record":record,"result":result2,"comment":commentCount};
                  res.status(200).send(return_response);
                }
              });
              }
            });
          }else{
            var return_response = {"error":false,errorMessage:"Pas de dossier","record":result};
            res.status(200).send(return_response);
          }

        }
      });  
    }catch(err){
        console.log(err);
         var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
    }
  },
  addComment:async function(req,res,next)
  {
    try{
      CommentsModel.create({
        user_id:req.body.user_id,
        forum_id:req.body.forum_id,
        comment:req.body.comment
      },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          //console.log(result);
          //console.log(result._id);

          var return_response = {"error":false,success: true,errorMessage:"Succès"};
            res.status(200)
            .send(return_response);
        }
      });
    }catch(err){
        console.log(err);
         var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
    }
  },
  likeForum:async function(req,res,next)
  {
    try{
      var updated_at = new Date();
      var forum_id = req.body.forum_id;
      var user_id = req.body.user_id;
      if(user_id)
      {
        if(forum_id)
        {
          var forumRecord = await ForumModel.find({ _id:req.body.forum_id }); 
          if(forumRecord)
          {
            if(forumRecord.length === 0)
            {
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "L'identifiant du forum n'est pas valide"
                });
            }else{
              ForumLikeDislikeModel.findOne({user_id:user_id,forum_id:forum_id},{_id:1}).exec((errForumLike,resultForumLike)=>{
                if(errForumLike)
                {
                  var return_response = {"error":true,success: false,errorMessage:errForumLike};
                    res.status(200)
                    .send(return_response);
                }else{
                  if(resultForumLike)
                  {
                    //console.log("resultForumLike");
                    //console.log(resultForumLike);
                    ForumLikeDislikeModel.findOneAndUpdate({ _id:resultForumLike._id },{
                      like_or_dislike:1,
                      updated_at:updated_at
                    },function(err,result){
                      if(err)
                      {
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        var return_response = {"error":false,success: true,errorMessage:"Succès"};
                                    res.status(200)
                                    .send(return_response);
                      }
                    });
                  }else{
                    ForumLikeDislikeModel.create({
                      user_id:req.body.user_id,
                      forum_id:req.body.forum_id,
                      like_or_dislike:1
                    },function(err,result){
                      if(err)
                      {
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        var return_response = {"error":false,success: true,errorMessage:"Succès"};
                                    res.status(200)
                                    .send(return_response);
                      }
                    });
                  }
                }
              });
            }
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  base_url:base_url,
                  errorMessage: "L'identifiant du forum n'est pas valide"
              });
          }
        }else{
          var return_response = {"error":true,success: false,errorMessage:"Identifiant du forum requis"};
              res.status(200).send(return_response);
        }
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Identifiant du utilisateur requis"};
              res.status(200).send(return_response);
      }
    }catch(err){
        console.log(err);
         var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
    }
  },
  disLikeForum:async function(req,res,next)
  {
    try{
      var updated_at = new Date();
      var forum_id = req.body.forum_id;
      var user_id = req.body.user_id;
      if(user_id)
      {
        if(forum_id)
        {
          var forumRecord = await ForumModel.find({ _id:req.body.forum_id }); 
          if(forumRecord)
          {
            if(forumRecord.length === 0)
            {
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "L'identifiant du forum n'est pas valide"
                });
            }else{
              ForumLikeDislikeModel.findOne({user_id:user_id,forum_id:forum_id},{_id:1}).exec((errForumLike,resultForumLike)=>{
                if(errForumLike)
                {
                  var return_response = {"error":true,success: false,errorMessage:errForumLike};
                    res.status(200)
                    .send(return_response);
                }else{
                  if(resultForumLike)
                  {
                    //console.log("resultForumLike");
                    //console.log(resultForumLike);
                    ForumLikeDislikeModel.findOneAndUpdate({ _id:resultForumLike._id },{
                      like_or_dislike:2,
                      updated_at:updated_at
                    },function(err,result){
                      if(err)
                      {
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        var return_response = {"error":false,success: true,errorMessage:"Succès"};
                                    res.status(200)
                                    .send(return_response);
                      }
                    });
                  }else{
                    ForumLikeDislikeModel.create({
                      user_id:req.body.user_id,
                      forum_id:req.body.forum_id,
                      like_or_dislike:2
                    },function(err,result){
                      if(err)
                      {
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        var return_response = {"error":false,success: true,errorMessage:"Succès"};
                                    res.status(200)
                                    .send(return_response);
                      }
                    });
                  }
                }
              });
            }
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  base_url:base_url,
                  errorMessage: "L'identifiant du forum n'est pas valide"
              });
          }
        }else{
          var return_response = {"error":true,success: false,errorMessage:"Identifiant du forum requis"};
              res.status(200).send(return_response);
        }
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Identifiant du utilisateur requis"};
              res.status(200).send(return_response);
      }
    }catch(err){
        console.log(err);
         var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
    }
  },
  likeComment:async function(req,res,next)
  {
    try{
      var updated_at = new Date();
      var comment_id = req.body.comment_id;
      var user_id = req.body.user_id;
      if(user_id)
      {
        if(comment_id)
        {
          var commentRecord = await CommentsModel.find({ _id:req.body.comment_id }); 
          if(commentRecord)
          {
            if(commentRecord.length === 0)
            {
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "L'identifiant du commentaire n'est pas valide"
                });
            }else{
              CommentLikeDislikeModel.findOne({user_id:user_id,comment_id:comment_id},{_id:1}).exec((errcommentLike,resultcommentLike)=>{
                if(errcommentLike)
                {
                  var return_response = {"error":true,success: false,errorMessage:errcommentLike};
                    res.status(200)
                    .send(return_response);
                }else{
                  if(resultcommentLike)
                  {
                    //console.log("resultcommentLike");
                    //console.log(resultcommentLike);
                    CommentLikeDislikeModel.findOneAndUpdate({ _id:resultcommentLike._id },{
                      like_or_dislike:1,
                      updated_at:updated_at
                    },function(err,result){
                      if(err)
                      {
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        CommentLikeDislikeModel.countDocuments({user_id:user_id,comment_id:comment_id,like_or_dislike:1}).exec((errcommentLikeCount,resultcommentLikeCount)=>{
                          if(errcommentLikeCount)
                          {
                            var return_response = {"error":true,success: false,errorMessage:errcommentLikeCount};
                              res.status(200)
                              .send(return_response);
                            }else{
                              //console.log("hereeeeee  resultcommentLikeCount");
                              //console.log(resultcommentLikeCount);
                              CommentsModel.findOneAndUpdate({ _id:comment_id },{
                                like:resultcommentLikeCount,
                                updated_at:updated_at
                              },function(err,result){
                                if(err)
                                {
                                  var return_response = {"error":true,success: false,errorMessage:err};
                                    res.status(200)
                                    .send(return_response);
                                }else{
                                  var return_response = {"error":false,success: true,errorMessage:"Succès"};
                                    res.status(200)
                                    .send(return_response);
                                }
                              });
                            }
                        });
                      }
                    });
                  }else{
                    CommentLikeDislikeModel.create({
                      user_id:req.body.user_id,
                      comment_id:req.body.comment_id,
                      like_or_dislike:1
                    },function(err,result){
                      if(err)
                      {
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        //console.log(result);
                        //console.log(result._id);
                        /*var old_like = commentRecord[0].like;
                        var new_like = old_like + 1;*/
                        CommentLikeDislikeModel.countDocuments({comment_id:comment_id,like_or_dislike:1}).exec((errcommentLikeCount,resultcommentLikeCount)=>{
                          if(errcommentLikeCount)
                          {
                            var return_response = {"error":true,success: false,errorMessage:errcommentLikeCount};
                              res.status(200)
                              .send(return_response);
                            }else{
                              //console.log(resultcommentLikeCount);
                              CommentsModel.findOneAndUpdate({ _id:comment_id },{
                                like:resultcommentLikeCount,
                                updated_at:updated_at
                              },function(err,result){
                                if(err)
                                {
                                  var return_response = {"error":true,success: false,errorMessage:err};
                                    res.status(200)
                                    .send(return_response);
                                }else{
                                  var return_response = {"error":false,success: true,errorMessage:"Succès"};
                                    res.status(200)
                                    .send(return_response);
                                }
                              });
                            }
                        });
                      }
                    });
                  }
                }
              });
            }
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  base_url:base_url,
                  errorMessage: "L'identifiant du commentaire n'est pas valide"
              });
          }
        }else{
          var return_response = {"error":true,success: false,errorMessage:"Identifiant du commentaire requis"};
              res.status(200).send(return_response);
        }
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Identifiant du utilisateur requis"};
              res.status(200).send(return_response);
      }
    }catch(err){
        console.log(err);
         var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
    }
  },
  disLikeComment:async function(req,res,next)
  {
    try{
      var updated_at = new Date();
      var comment_id = req.body.comment_id;
      var user_id = req.body.user_id;
      if(user_id)
      {
        if(comment_id)
        {
          var commentRecord = await CommentsModel.find({ _id:req.body.comment_id }); 
          if(commentRecord)
          {
            if(commentRecord.length === 0)
            {
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "L'identifiant du commentaire n'est pas valide"
                });
            }else{
              CommentLikeDislikeModel.findOne({user_id:user_id,comment_id:comment_id},{_id:1}).exec((errcommentLike,resultcommentLike)=>{
                if(errcommentLike)
                {
                  var return_response = {"error":true,success: false,errorMessage:errcommentLike};
                    res.status(200)
                    .send(return_response);
                }else{
                  if(resultcommentLike)
                  {
                    //console.log("resultcommentLike");
                    //console.log(resultcommentLike);
                    CommentLikeDislikeModel.findOneAndUpdate({ _id:resultcommentLike._id },{
                      like_or_dislike:2,
                      updated_at:updated_at
                    },function(err,result){
                      if(err)
                      {
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        CommentLikeDislikeModel.countDocuments({user_id:user_id,comment_id:comment_id,like_or_dislike:2}).exec((errcommentLikeCount,resultcommentLikeCount)=>{
                          if(errcommentLikeCount)
                          {
                            var return_response = {"error":true,success: false,errorMessage:errcommentLikeCount};
                              res.status(200)
                              .send(return_response);
                            }else{
                              //console.log("hereeeeee  resultcommentLikeCount");
                              //console.log(resultcommentLikeCount);
                              CommentsModel.findOneAndUpdate({ _id:comment_id },{
                                like:resultcommentLikeCount,
                                updated_at:updated_at
                              },function(err,result){
                                if(err)
                                {
                                  var return_response = {"error":true,success: false,errorMessage:err};
                                    res.status(200)
                                    .send(return_response);
                                }else{
                                  var return_response = {"error":false,success: true,errorMessage:"Succès"};
                                    res.status(200)
                                    .send(return_response);
                                }
                              });
                            }
                        });
                      }
                    });
                  }else{
                    CommentLikeDislikeModel.create({
                      user_id:req.body.user_id,
                      comment_id:req.body.comment_id,
                      like_or_dislike:2
                    },function(err,result){
                      if(err)
                      {
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        //console.log(result);
                        //console.log(result._id);
                        /*var old_like = commentRecord[0].like;
                        var new_like = old_like + 1;*/
                        CommentLikeDislikeModel.countDocuments({comment_id:comment_id,like_or_dislike:2}).exec((errcommentLikeCount,resultcommentLikeCount)=>{
                          if(errcommentLikeCount)
                          {
                            var return_response = {"error":true,success: false,errorMessage:errcommentLikeCount};
                              res.status(200)
                              .send(return_response);
                            }else{
                              //console.log(resultcommentLikeCount);
                              CommentsModel.findOneAndUpdate({ _id:comment_id },{
                                dislike:resultcommentLikeCount,
                                updated_at:updated_at
                              },function(err,result){
                                if(err)
                                {
                                  var return_response = {"error":true,success: false,errorMessage:err};
                                    res.status(200)
                                    .send(return_response);
                                }else{
                                  var return_response = {"error":false,success: true,errorMessage:"Succès"};
                                    res.status(200)
                                    .send(return_response);
                                }
                              });
                            }
                        });
                      }
                    });
                  }
                }
              });
            }
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  base_url:base_url,
                  errorMessage: "L'identifiant du commentaire n'est pas valide"
              });
          }
        }else{
          var return_response = {"error":true,success: false,errorMessage:"Identifiant du commentaire requis"};
              res.status(200).send(return_response);
        }
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Identifiant du utilisateur requis"};
              res.status(200).send(return_response);
      }
    }catch(err){
        console.log(err);
         var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
    }
  },
  getAboutUs:async function(req,res,next)
  {
    try{
      AboutUsModel.find( {}, {}).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });  
    }catch(err){
        console.log(err);
         var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
    }
  },

  getPrivacyPolicy:async function(req,res,next)
  {
    try{
      PrivacyAndPolicyModel.find( {}, {}).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });  
    }catch(err){
        console.log(err);
         var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
    }
  },

  getTermsCondition:async function(req,res,next)
  {
    try{
      TermsAndConditionModel.find( {}, {}).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });  
    }catch(err){
        console.log(err);
         var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
    }
  },
};
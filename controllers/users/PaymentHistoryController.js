const express = require('express');
const UsersModel = require("../../models/UsersModel");
const Paymentrecords = require("../../models/Paymentrecords");
const CategoryModel = require("../../models/CategoryModel");
const SubCategoryModel = require("../../models/SubCategoryModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
var FCM = require('fcm-push');
var serverKey = 'AAAA2rkZY-g:APA91bHh0ESTr-ZTgPxQ3_pJ4KZarMErxCwR2mJ3kTw9arsD1q_-KQEbLaqE0SXsTjSWwYa8EwuPOHkxzS3PjT1LUUC5e_U260-uFztSZQc-4hWdnCXO4PXniGtD7HwsmYTO1ukAi';
var fcm = new FCM(serverKey);
const customConstant = require('../../helpers/customConstant');

module.exports = {

	getFCMPushTesting:async function(req,res,next)
	{
		var message = {
		    to: "eWQOUQrAQCClAmKtc0Zhs3:APA91bE_PzCMMIuKWSDVUR21PvRlnYwtpoeB9PHPYex9mI53Qilv07FGFwjVISB-yVw5kGU2exZADaHu04XCCiv4CSMxfgMV8Jm7ZHuD12XlO4myCtrRwrki7eKKOLNU_DGv9xltB3N-", // required fill with device token or `/topics/${topicName}`
		    collapse_key: 'your_collapse_key', 
		    data: {
		        your_custom_data_key: 'your_custom_data_value'
		    },
		    notification: {
		        title: 'Title of your push notification',
		        body: 'Body of your push notification'
		    }
		};
		//callback style
		/*fcm.send(message, function(err, response){
		    if (err) {
		        console.log("Something has gone wrong!");
		    } else {
		        console.log("Successfully sent with response: ", response);
		    }
		});*/

		//promise style
		fcm.send(message)
		    .then(function(response){
	        console.log("Successfully sent with response: ", response);
	    })
	    .catch(function(err){
	        console.log("Something has gone wrong!");
	        console.error(err);
	    });
	},
	getOwnAllPayment:async function(req,res,next)
	{
		var base_url = customConstant.base_url;
    //console.log("get profile");
    try{
      var user_id = req.body.user_id;
      var total_payment = 0;
      //console.log(user_id);
      Paymentrecords.find({ "user_id":user_id,status:1 }).sort([['created_at', -1]]).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
        	if(result)
        {
          if(result.length === 0)
          {
            res.status(200)
                .send({
                    error: true,
                    success: false,
		                  imageUrl:base_url+"public/uploads/payments/",
                    errorMessage: "Pas d'enregistrement"
                });
          }else{
          	var last_value = result.length -1;
          	for (var i = 0; i < result.length; i++)
          	{
          		total_payment = parseFloat(total_payment) + parseFloat(result[i].price);

          		if(last_value == i)
          		{
          			var average_price = total_payment/result.length;
          			//console.log("for send response");
          			res.status(200)
		              .send({
		                  error: false,
		                  success: true,
		                  errorMessage: "Tous les enregistrements du détail des paiements",
		                  imageUrl:base_url+"public/uploads/payments/",
		                  total_payment_number:result.length,
		                  total_payment:total_payment,
		                  average_price:average_price,
		                  record:result,
		              });
          		}

          	}
          }
        }else{
          console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: "Pas d'enregistrement"
                });
        }
        }
      });
    }catch(err){
      console.log(err);
    }
	},
	getOwnAllPaymentHistory:async function(req,res,next)
	{
      //console.log("get profile");
      try{
				var base_url = customConstant.base_url;
        var user_id = req.body.user_id;
        var accessoires_image = [];
        var total_payment = 0;
        //console.log(user_id);
        Paymentrecords.find({ "user_id":user_id,status:1 }).lean(true).exec((err, result)=>
				{
					if(err)
					{
						console.log(err);
					}else{
						if(result)
					{
						if(result.length === 0)
						{
							res.status(200)
									.send({
											error: true,
											success: false,
											errorMessage: "Détail utilisateur non valide"
									});
						}else{
							var break_loop = result.length - 1;
							// for (let i = 0; i < result.length; i++)
							// {}
								/*let image_path = null;
								CategoryModel.findOne({_id:result[i].category_id},{"images":1}).exec((err, advertresult)=>
								{
									if(advertresult)
									{
										//console.log(advertresult);
										//console.log(advertresult.images[0].path);
										if(advertresult.images.length > 0)
										{
											image_path = advertresult.images[0].path;
										}
									}
								});*/
								//console.log("accessoires_image");
								var i=0;
								result.forEach(object=>{
									object.pdfUrl = base_url+"public/uploads/pdf-file/"+object._id+"_result.pdf";
									total_payment = parseFloat(total_payment) + parseFloat(object.price);
									if(i == break_loop)
									{
										res.status(200)
										.send({
												error: false,
												success: true,
												errorMessage: "Détails de l'utilisateur",
												total_payment_number:result.length,
												total_payment:total_payment,
												record:result,
										});
									}
									i++;
								});
								

							
							
						}
					}else{
						console.log("here");
						res.status(200)
									.send({
											error: true,
											success: false,
											errorMessage: "Détail utilisateur non valide"
									});
					}
					}
				});
      }catch(err){
        console.log(err);
      }
	},

};
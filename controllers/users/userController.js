const express = require('express');
const UsersModel = require("../../models/UsersModel");
const UsercontactsModel = require("../../models/UsercontactsModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');

const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    host: 'mail.coeurorphelins.org',
    port: 465,
    auth: {
       user: 'noreply@coeurorphelins.org',
       pass: '*Ah}PRPwh=hQ'
    }
});

/*/*/
module.exports = {
  userRegistration: async function(req, res,next)
  {
    //console.log("controller registration method");
    //console.log(req.body);
    //res.send("herfsdfe");
    var encryptedPassword = "";
       try {
        const { city,userName, email, password,user_type,firstName,lastName,mobileNumber,countryCode,whatsAppCountryCode,whatsAppMobileNumber } = req.body;
        // check if user already exist
        // Validate if user exist in our database
        encryptedPassword = await bcrypt.hash(password, 10);

        //console.log("xx"+xx);
      //console.log("encryptedPassword khushal "+encryptedPassword);

        var token =  jwt.sign(
          { user_id:  Math.random().toString(36).slice(2), email },
          "Porschists",
          //{
          //  expiresIn: "2h",
          //}
        );

        UsersModel.count({ email },(errs,emailCount)=>
        {
          if(errs)
          {
            console.log(errs);
          }else{
            //console.log(emailCount);
            if(emailCount === 0)
            {
              UsersModel.count({ mobileNumber },(userNameerrs,userNameCount)=>
              {
                if(userNameerrs)
                {
                  console.log(errs);
                }else{
                  if(userNameCount === 0)
                  {
                    
                    //Encrypt user password
                    //encryptedPassword = bcrypt.hash(password, 10);
                    //console.log(encryptedPassword);
                    //return false;
                    // Create token
                    // save user token
                    //user.token = token;
                    //encryptedPassword = "$2a$10$2XpyurefqmxN40GU0RVXQ.dVhX7DSxxiXw4PvH/w4IM6apJ6zlv4.";
                    //console.log("encryptedPassword"+encryptedPassword);
                    // Create user in our database
                    //console.log("herrrrrrrrrrrrrr");
                    const user =  UsersModel.create({
                      user_type,
                      countryCode,
                      mobileNumber,
                      firstName,
                      lastName,
                      userName: email.toLowerCase(),
                      email: email.toLowerCase(), // sanitize: convert email to lowercase
                      password: encryptedPassword,
                      token: token,
                      city: city,
                      whatsAppCountryCode,
                      whatsAppMobileNumber
                    });
                    // return new user
                    res.status(200)
                          .send({
                              error: false,
                              success: true,
                              errorMessage: "Inscription réussie"
                          });
                  }else{
                    //email exist
                    res.status(200)
                          .send({
                              error: true,
                              success: false,
                              errorMessage: "Le numéro de mobile existe déjà"
                          });
                  }
                }
                
              });
            }else{
              //email exist
              res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: "Email déjà existant"
                    });
            }
          }
        }); 
    }catch (err) {
        console.log(err);
    }
  },
  userLogin: async function(req,res,next)
  {
      //console.log("login function");
    try{
      var base_url = customConstant.base_url;
      var imagUrl = base_url+"public/uploads/userProfile/";
      // Get user input
      const { email, password,deviceToken } = req.body;
      // Validate user input
      // Validate if user exist in our database
      var user = "";
      user = await UsersModel.findOne({ email, status:1 ,"deleted_at": 0 });
      //console.log("user record"+user);
      if (user && (await bcrypt.compare(password, user.password)))
      {
        // Create token
        const token = jwt.sign(
          { user_id: user._id, email },
          "Porschists",
          /*process.env.TOKEN_KEY,
          {
            expiresIn: "2h",
          }*/
        );
        // save user token
        user.token = token;
        UsersModel.updateOne({ "email":email }, 
        {token:token,deviceToken:deviceToken,loggedIn:1}, function (uerr, docs) {
        if (uerr){
            console.log(uerr);
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: uerr,
                  //userRecord:user
              });
        }
        else{
          let response_record = { "_id":user._id,"user_type":user.user_type,"firstName":user.firstName,"lastName":user.lastName,"email":user.email,"token":user.token,"userImage":user.userImage };
            //console.log("User controller login method : ", docs);
            res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "Connexion réussie",
                  imagUrl:imagUrl,
                  userRecord:response_record
              });
        }
        });
        // user
        //res.status(200).json(user);
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Détails de connexion non valides"
          });
      }
    }catch(err) {
        console.log(err);
    }
  },
  getUserProfile:async function(req,res,next)
  {
      //console.log("get profile");
      try{
        var base_url = customConstant.base_url;
        var imagUrl = base_url+"public/uploads/userProfile/";
        //console.log(req.socket.remoteAddress);
        //console.log("my IP is  "+req.ip);
        var user_id = req.body.user_id;
        //console.log(user_id);
        var userRecord = await UsersModel.findOne({ "_id":user_id }); 
        //console.log("userRecord"+userRecord);
        if(userRecord)
        {
          if(userRecord.length === 0)
          {
            res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: "Détail de l'utilisateur non valide"
                });
          }else{
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    errorMessage: "Détail de l'utilisateur",
                    imagUrl:imagUrl,
                    userRecord:userRecord,
                });
          }
        }else{
          //console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: "Détail de l'utilisateur non valide"
                });
        }
        
      }catch(err){
        console.log(err);
      }
  },  
  updateUserProfile:async function(req,res,next)
  {
      //console.log("update profile");
      //console.log(req.body);
      //console.log(req.file.filename);
      try{
        var user_id = req.body.user_id;
        var email = req.body.email;
        var mobileNumber = req.body.mobileNumber;
        var firstName = req.body.firstName;
        var lastName = req.body.lastName;
        var city = req.body.city;
        var description = req.body.description;
        var countryCode = req.body.countryCode;
        var whatsAppCountryCode = req.body.whatsAppCountryCode;
        var whatsAppMobileNumber = req.body.whatsAppMobileNumber;

         

        //console.log(user_id);
        var userRecord = await UsersModel.findOne({ "_id":user_id }); 
        //console.log("userRecord"+userRecord);
        if(req.file)
        {
          var userImage = req.file.filename;
          var oldFileName = userRecord.userImage;
          var uploadDir = './public/uploads/userProfile/';
          let fileNameWithPath = uploadDir + oldFileName;
          console.log(fileNameWithPath);

          if (fs.existsSync(fileNameWithPath))
          {
            fs.unlink(uploadDir + oldFileName, (err) => 
            {
              console.log("unlink file error "+err);
            });
          }



        }else{
          if(userRecord.userImage)
          {
            var userImage = userRecord.userImage;
          }else{
            var userImage = null;
          }
        }
        if(userRecord)
        {
          
          var userRecordEmailCheck = await UsersModel.findOne({ "_id":{$ne:user_id},"email":email }); 
          console.log("userRecordEmailCheck"+userRecordEmailCheck);
          if(userRecordEmailCheck)
          {
            res.status(200)
              .send({
                error: true,
                success: false,
                errorMessage: "Cet email existe déjà chez un autre utilisateur"
            });
          }else{
            
            var userRecordMobileCheck = await UsersModel.findOne({ "_id":{$ne:user_id},"mobileNumber":mobileNumber }); 
            console.log("userRecordMobileCheck"+userRecordMobileCheck);
            if(userRecordMobileCheck)
            {
              res.status(200)
                .send({
                  error: true,
                  success: false,
                  errorMessage: "Ce numéro de téléphone mobile existe déjà pour un autre utilisateur"
              });
            }else{
              UsersModel.findByIdAndUpdate({ _id: user_id },{ email: email.toLowerCase(),mobileNumber:mobileNumber,firstName:firstName,lastName:lastName,userImage:userImage,city:city ,description:description,countryCode:countryCode,whatsAppCountryCode:whatsAppCountryCode,whatsAppMobileNumber:whatsAppMobileNumber }, function (err, user) {
                if (err) {
                  res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: "Un problème est survenu",
                        errorOrignalMessage: err
                    });
                  
                } else {
                  console.log("L'enregistrement a été mis à jour avec succès");
                    res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "L'enregistrement a été mis à jour avec succès"
                    });
                }
               })
            }
          }
        }else{
          //console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: "Détails de l'utilisateur non valides"
                });
        }
        
      }catch(err){
        console.log(err);
      }
  }, 
  changePassword:async function(req,res,next)
  {
    //console.log("update profile");
    //console.log(req.body);
    //console.log(req.file.filename);
    try{
      var user_id = req.body.user_id;
      var password = req.body.password;
      var new_password = req.body.new_password;
      var encryptedPassword = await bcrypt.hash(new_password, 10);

      //console.log(user_id);
      var userRecord = await UsersModel.findOne({ "_id":user_id }); 
      //console.log("userRecord"+userRecord);
      if(userRecord)
      {
        if (userRecord && (await bcrypt.compare(password, userRecord.password)))
        {
          UsersModel.updateOne({ "_id":user_id }, 
          {password:encryptedPassword}, function (uerr, docs) {
          if (uerr){
              //console.log(uerr);
              res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: uerr,
                        //userRecord:user
                    });
          }else{
            //let response_record = { "_id":user._id,"user_type":user.user_type,"firstName":user.firstName,"lastName":user.lastName,"email":user.email,"token":user.token };
              //console.log("User controller login method : ", docs);
              res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Succès",
                        //userRecord:response_record
                    });
          }
          });
        }else{
          //res.status(400).send("Invalid Credentials");
          res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: "Ancien mot de passe invalide"
                  });
        }
      }else{
        console.log("here");
        res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Détails de l'utilisateur non valides"
              });
      }
      
    }catch(err){
      console.log(err);
    }

  }, 
  logOut:async function(req,res,next)
  {
    //console.log("update profile");
    //console.log(req.body);
    //console.log(req.file.filename);
    try{
      var user_id = req.body.user_id;

      //console.log(user_id);
      var userRecord = await UsersModel.findOne({ "_id":user_id }); 
      console.log("userRecord"+userRecord);
      if(userRecord)
      {
        UsersModel.updateOne({ "_id":user_id }, 
        {loggedIn:0,deviceToken:null}, function (uerr, docs) {
        if (uerr){
            //console.log(uerr);
            res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: uerr,
                      //userRecord:user
                  });
        }else{
            res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "Succès",
                      //userRecord:response_record
                  });
        }
        });
      }else{
        console.log("here");
        res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Détails de l'utilisateur non valides"
              });
      }
    }catch(err){
      console.log(err);
    }

  },
  fromAdminContactUs: async function(req, res,next)
  {
    console.log(req.body);
    //res.send("herfsdfe");
    try {
      UsercontactsModel.create({
        fullName:req.body.fullName,
        email:req.body.email,
        subject:req.body.subject,
        message:req.body.message
      },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          //console.log(result);
          //console.log(result._id);
          var return_response = {"error":false,success: true,errorMessage:"Succès"};
            res.status(200)
            .send(return_response);
        }
      });
    }catch (err) {
      console.log(err);
    }
  },
  forgotPassword:async function(req,res,next)
  {
    try{
      var randomNumber = Array.from(Array(8), () => Math.floor(Math.random() * 36).toString(36)).join('');
      var encryptedPassword = await bcrypt.hash(randomNumber, 10);

      var email = req.body.email;
      UsersModel.find({ email:email, status:1 ,"deleted_at": 0 }, {}).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          if(result.length > 0)
          {
            //console.log("here");
            //console.log(result);
            const message = {
                from: "noreply@coeurorphelins.org Coeur d'Orphelins", // Sender address
                to: result[0].email,         // recipients
                subject: "Mot de passe oublié", // Subject line
                html: "<b>"+randomNumber+"</b>" +" Ceci est votre nouveau mot de passe, vous pouvez vous connecter avec ce mot de passe dans notre application et vous pouvez changer le mot de passe après la connexion." // Plain text body
            };
            transport.sendMail(message, function(err, info) {
                if (err) {
                  console.log(err);
                  error_have = 1;
                } else {
                  console.log('mail has sent.');
                  //console.log(info);
                }
            });
            UsersModel.updateOne({ "_id":result[0]._id }, 
              {password:encryptedPassword}, function (uerr, docs) {
              if (uerr){
                //console.log(uerr);
                res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: uerr
                  });
            }else{
                res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "Votre mot de passe a été modifié avec succès, vous pouvez vérifier votre e-mail"
                  });
                }
            });

          }else{
            var return_response = {"error":true,success: false,errorMessage:"Compte e-mail non valide ou compte inactif"};
            res.status(200)
            .send(return_response);
          }
        }
      });
    }catch (err) {
      console.log(err);
    }
  },
  deleteAC:async function(req,res,next)
  {
    try{
      var email = req.body.email;
      UsersModel.find({ email:email, status:1 ,"deleted_at": 0 }, {}).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          if(result.length > 0)
          {
            //console.log("here");
            //console.log(result);
            var html = "";
            html += "Bonjour "+result[0].firstName+' '+result[0].lastName+ ",";

            html += "<p>Votre compte a été supprimé à votre demande. Merci encore de faire partie de la communauté Coeur d'Orphelins.</p>";
            
            html += "<p>Nous espérons avoir le plaisir de vous revoir !</p>";
            
            html += "<p>Bien Cordialement</p>";
            html += "<p>L'équipe de Coeur d'Orphelins</p>";
            
            const message = {
                from: "noreply@coeurorphelins.org Coeur d'Orphelins", // Sender address
                to: result[0].email,         // recipients
                subject: "Coeur d'Orphelins a supprimé votre compte", // Subject line
                html: html
            };
            transport.sendMail(message, function(err, info) {
                if (err) {
                  console.log(err);
                  //error_have = 1;
                        UsersModel.updateOne({ "_id":result[0]._id }, 
                    {deleted_at:1}, function (uerr, docs) {
                    if (uerr){
                      //console.log(uerr);
                      res.status(200)
                        .send({
                            error: true,
                            success: false,
                            errorMessage: uerr
                        });
                  }else{
                      res.status(200)
                        .send({
                            error: false,
                            success: true,
                            errorMessage: "Votre compte est supprimé"
                        });
                      }
                  });
                } else {
                  console.log('mail has sent.');
                  //console.log(info);
                  UsersModel.updateOne({ "_id":result[0]._id }, 
                    {deleted_at:1}, function (uerr, docs) {
                    if (uerr){
                      //console.log(uerr);
                      res.status(200)
                        .send({
                            error: true,
                            success: false,
                            errorMessage: uerr
                        });
                  }else{
                      res.status(200)
                        .send({
                            error: false,
                            success: true,
                            errorMessage: "Votre compte est supprimé"
                        });
                      }
                  });
                }
            });
          }else{
            var return_response = {"error":true,success: false,errorMessage:"Compte e-mail non valide ou compte inactif"};
            res.status(200)
            .send(return_response);
          }
        }
      });
    }catch (err) {
      console.log(err);
    }
  },
};
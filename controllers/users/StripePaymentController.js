const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const SubCategoryModel = require("../../models/SubCategoryModel");
const ProjectModel = require("../../models/ProjectModel");
const GalleryModel = require("../../models/GalleryModel");
const Paymentrecords = require("../../models/Paymentrecords");
const PaypalModel = require("../../models/PaypalModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant'); 

const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
	host: 'mail.coeurorphelins.org',
	port: 465,
	auth: {
		 user: 'noreply@coeurorphelins.org',
		 pass: '*Ah}PRPwh=hQ'
	}
});
 
console.log("calling helper");  

//setTimeout(stripeCredential(),3000);

//console.log(customConstant.stripeCredential());  
 

const stripe = require('stripe')('sk_test_51JxuJ2D9JhiIjZ9Se2ykRvwGEcQngtzAZtkY9WkxJmkTCAvBPA1Oi4Thv000MD38Lb8kPdpModMDS0mFxswKgoir00quvPI7Yk');

const paypal = require('paypal-rest-sdk');

// paypal.configure({
//   'mode': 'live', //sandbox or live
//   'client_id': 'ASSxCpsPOrlQwa4S82AmsDUJR_0qJEZWXhi35rE3816jQdKo2_Frb8zFvxYbRNYkoq3QBr_2xpEeCq6_',
//   'client_secret': 'EIfubNMWsfm8AQI5gCkBApsypVvQKGZXqcvhNq6dtNw-JjNf8FivUhxKIaKIKkZo8G17Ko8tRYHUsm71'
// });

/*/*/
module.exports = {
	getTestStripePayment:async function(req,res,next)
	{
		/*const paymentIntent = await stripe.paymentIntents.create({
			  amount: 1099,
			  currency: 'eur',
			  payment_method_types: ['card'],
		});
		console.log(paymentIntent);*/
		stripe.customers
		  .create({
		    email: 'customer@example.com',
		  })
		  .then((customer) => {
		    // have access to the customer object
		    return stripe.invoiceItems
		      .create({
		        customer: customer.id, // set the customer id
		        amount: 2500, // 25
		        currency: 'usd',
		        description: 'One-time setup fee',
		      })
		      .then((invoiceItem) => {
		        return stripe.invoices.create({
		          collection_method: 'send_invoice',
		          customer: invoiceItem.customer,
		        });
		      })
		      .then((invoice) => {
		        // New invoice created on a new customer
		      })
		      .catch((err) => {
		        // Deal with an error
		      });
		  });
		/*const session =   stripe.checkout.sessions.create({
			amount: 1099,
			currency: 'eur',
		    payment_method_types: ['card'],
		    mode: 'payment',
		    customer: customerId,
		    success_url: "http://localhost:3000/api/getTestStripePaymentSuccess",
		    cancel_url: "http://localhost:3000/api/getTestStripePaymentCancel",
		    line_items: [
			    {
			      price: "Test khushal",
			      quantity: 1,
			      //tax_rates: [
			        //customer.metadata.country === 'japan' ? taxRateJpn : taxRateZero
			      //]
			    }
		  	]
			});
		stripe.redirectToCheckout({ sessionId: session.id })*/
	},
	testStripePayment:async function(req,res,next)
	{
		/*var customer_id = "";
		stripe.customers
		  .create({
		    email: 'customer@example.com',
		  })
		  .then((customer) => {
		  	console.log(customer.id);
		  	customer_id = customer.id;
		  });*/
		  const session = await stripe.checkout.sessions.create({
		    line_items: [
		      {
		        price_data: {
		          currency: 'eur',
		          product_data: {
		            name: 'T-shirt',
		          },
		          unit_amount: 2000,
		        },
		        quantity: 1,
		        description: 'One-time setup fee',
		      },
		    ],
		    mode: 'payment',
		    //description: 'One-time setup fee',
		    success_url: 'http://localhost:3000/api/testSuccessStripe',
		    cancel_url: 'http://localhost:3000/api/testCancelStripe',
		  });
		  console.log(session);
		  res.redirect(303, session.url);
	},
	testCancelStripe:async function(req,res,next)
	{
		console.log("cancel");
	},
	testSuccessStripe:async function(req,res,next)
	{
		console.log("success");
		//console.log(req);
		//pi_3LOGivD9JhiIjZ9S18m1Ba8o
		const stripe = require('stripe')('sk_test_51JxuJ2D9JhiIjZ9Se2ykRvwGEcQngtzAZtkY9WkxJmkTCAvBPA1Oi4Thv000MD38Lb8kPdpModMDS0mFxswKgoir00quvPI7Yk');

		const session = await stripe.checkout.sessions.retrieve(
		  'cs_test_a1cxJSPc5gJ3z7vTU7K78Y0teg4jKIJGk5qeZbTDyVrBrs7dUiauzVXnzR'
		);
		console.log(session);
		console.log(session.payment_status);
		if(session)
		{
			if(session.payment_status == "paid")
			{
				res.status(200)
		          .send({
		              error: false,
		              success: true,
		              errorMessage: "Payment success"
		          });
			}else{
				res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: "Not paid"
                  });
			}
		}else{
			res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Some error"
              });
		}
	},
	getDonationStripePayment:async function(req,res,next)
	{
		var filePathCopy = null;
		var order_id = Math.floor(10000000 + Math.random() * 90000000);
		var base_url = customConstant.base_url;
		//console.log(req.query.payment_id);Paymentrecords
		var user_id = req.query.user_id;
		var payment_id = req.query.payment_id;
		var type = req.query.type;
		var price = req.query.price;
		var category_title = subcategory_title = description = "";
		if(payment_id && type && user_id && price)
		{
			/*console.log(payment_id);
			console.log(type);
			return false;*/
			if(type == "subCategory")
			{
				var subCategoryRecord = await SubCategoryModel.find({ "_id":payment_id }); 
				if(subCategoryRecord)
				{
					//console.log(subCategoryRecord[0].category);
					var category_id = subCategoryRecord[0].category;
					var categoryRecord = await CategoryModel.find({ "_id":category_id }); 
					if(categoryRecord)
					{
						/*console.log(categoryRecord);
						console.log(categoryRecord[0].category);*/
						var subcategory_id = payment_id;
						var category_title = categoryRecord[0].category;
						var subcategory_title = subCategoryRecord[0].title;
						var description = subCategoryRecord[0].description;
						//var price = subCategoryRecord[0].price;
						var payment_mode = type;
						var payment_initiate_date = new Date();
						
						if(subCategoryRecord[0].images)
						{
							//console.log(subCategoryRecord[0].images);
							if(subCategoryRecord[0].images.length > 0)
							{
								var imagePath = subCategoryRecord[0].images[0].path;
								//console.log(imagePath);
								var imageName = subCategoryRecord[0].images[0].filename;
								var imagePath = subCategoryRecord[0].images[0].path;
								//console.log(imagePath);

								//const filePath = './uploads/my-image.png';
								filePathCopy = './public/uploads/payments/'+imageName;
								fs.copyFile(imagePath, filePathCopy, (err) =>
								{
								  if (err) throw err;
								  console.log('File Copy Successfully.');
								});

							}
						}

						/*console.log(" here  filePathCopy");
						console.log(filePathCopy);return false;*/

						Paymentrecords.create({
							order_id:order_id,
		          payment_id:payment_id,
		          category_id:category_id,
		          filePathCopy:imageName,
		          subcategory_id:subcategory_id,
		          user_id:user_id,
		          payment_mode:payment_mode,
		          category_title:category_title,
		          subcategory_title:subcategory_title,
		          description:description,
		          price:price,
		          payment_type:"Stripe",
		          status:0,
		          payment_initiate_date:payment_initiate_date,
							countryCode:countryCode,
						whatsAppCountryCode:whatsAppCountryCode,
		        },function(err,result){
		          if(err)
		          {
		            var return_response = {"error":true,success: false,errorMessage:err};
		              res.status(200)
		              .send(return_response);
		          }else{
		          	var lastInsertId = result._id;
		          	//console.log(lastInsertId);
		          	var redirect_url_web = base_url+"api/getDonationStripePaymentNew?id="+lastInsertId+"&price="+price+"&title="+subcategory_title;
							  res.redirect(303, redirect_url_web);
		          }
		        });
					}else{
						res.status(200)
			        .send({
		            error: true,
		            success: false,
		            errorMessage: "Identifiant de paiement non valide"
		        });
					}
				}else{
					res.status(200)
		        .send({
	            error: true,
	            success: false,
	            errorMessage: "Identifiant de paiement non valide"
	        });
				}
			}else if(type == "Category")
			{
					var category_id = payment_id;
					var categoryRecord = await CategoryModel.find({ "_id":category_id }); 
					if(categoryRecord)
					{
						//console.log(categoryRecord);
						//console.log(categoryRecord[0].category);
						//return false;
						var subcategory_id = payment_id;
						var category_title = categoryRecord[0].category;
						var subcategory_title = null;
						var description = null;
						//var price = categoryRecord[0].price;
						var payment_mode = type;
						var payment_initiate_date = new Date();
						
						if(categoryRecord[0].images)
						{
							//console.log(categoryRecord[0].images);
							if(categoryRecord[0].images.length > 0)
							{
								var imagePath = categoryRecord[0].images[0].path;
								console.log(imagePath);
								var imageName = categoryRecord[0].images[0].filename;
								var imagePath = categoryRecord[0].images[0].path;
								//console.log(imagePath);

								//const filePath = './uploads/my-image.png';
								filePathCopy = './public/uploads/payments/'+imageName;
								fs.copyFile(imagePath, filePathCopy, (err) =>
								{
								  if (err) throw err;
								  console.log('File Copy Successfully.');
								});

							}
						}
						//return false;


						Paymentrecords.create({
							order_id:order_id,
		          payment_id:payment_id,
		          category_id:category_id,
		          filePathCopy:imageName,
		          subcategory_id:null,
		          user_id:user_id,
		          payment_mode:payment_mode,
		          category_title:category_title,
		          subcategory_title:null,
		          description:null,
		          price:price,
		          payment_type:"Stripe",
		          status:0,
		          payment_initiate_date:payment_initiate_date,
		        },function(err,result){
		          if(err)
		          {
		            var return_response = {"error":true,success: false,errorMessage:err};
		              res.status(200)
		              .send(return_response);
		          }else{
		          	var lastInsertId = result._id;
		          	//console.log(lastInsertId);
		          	var redirect_url_web = base_url+"api/getDonationStripePaymentNew?id="+lastInsertId+"&price="+price+"&title="+category_title;
							  res.redirect(303, redirect_url_web);
		          }
		        });
					}else{
						res.status(200)
			        .send({
		            error: true,
		            success: false,
		            errorMessage: "Identifiant de paiement non valide"
		        });
					}
			}else{
				res.status(200)
	        .send({
	            error: true,
	            success: false,
	            errorMessage: "L'identifiant du paiement, le type de paiement et l'identifiant de l'utilisateur sont tous nécessaires"
	        });
			}
		}else{
			res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de paiement, le type de paiement, l'identifiant de l'utilisateur et le prix sont tous requis"
        });
		}
		  /**/
	},
	getDonationStripePaymentNew:async function(req,res,next)
	{
		var updated_at = new Date();
		var base_url = customConstant.base_url; 
		var lastInsertId = req.query.id;
		var price = req.query.price;
		var title = req.query.title;
		/*console.log(lastInsertId);
		console.log(price);
		console.log(title);*/
		const session = await stripe.checkout.sessions.create({
	    line_items: [
	      {
	        price_data: {
	          currency: 'eur',
	          product_data: {
	            name: title,
	          },
	          unit_amount: price * 100,
	        },
	        quantity: 1,
	        //description: lastInsertId,
	      },
	    ],
	    mode: 'payment',
	    //description: 'One-time setup fee',
	    success_url: base_url+'api/getDonationStripeSuccess?session_id={CHECKOUT_SESSION_ID}',
	    cancel_url: base_url+'api/getDonationStripeCancel',
	  });
	  //console.log(session);
	  console.log(session.id);
	  console.log(session.payment_intent);

	  Paymentrecords.findOneAndUpdate({ _id:lastInsertId },{
      transaction_id:session.id,
      payment_intent:session.payment_intent,
      updated_at:updated_at
    },function(err,result){
    	if(err)
	    {
	      var return_response = {"error":true,success: false,errorMessage:err};
	        res.status(200)
	        .send(return_response);
	    }else{
	      res.redirect(303, session.url);
	    }
    });
	  
	},
	getDonationStripeSuccess:async function(req,res,next)
	{
		//console.log("success stripe");
		//console.log(req.query.session_id);
		//var session_ID = req
		var updated_at = new Date();
		const stripe = require('stripe')('sk_test_51JxuJ2D9JhiIjZ9Se2ykRvwGEcQngtzAZtkY9WkxJmkTCAvBPA1Oi4Thv000MD38Lb8kPdpModMDS0mFxswKgoir00quvPI7Yk');

		const session = await stripe.checkout.sessions.retrieve(
		  req.query.session_id
		);
		//console.log(session);
		//console.log(session.payment_status);
		if(session)
		{
			if(session.payment_status == "paid")
			{

				Paymentrecords.findOneAndUpdate({ transaction_id:req.query.session_id },{
		      status:1,
		      payment_success_date:updated_at,
		      updated_at:updated_at
		    },function(err,result){
		    	if(err)
			    {
			      var return_response = {"error":true,success: false,errorMessage:err};
			        res.status(200)
			        .send(return_response);
			    }else{
						//console.log(req.query.session_id);return false;
						Paymentrecords.findOne({transaction_id:req.query.session_id},{user_id:1,price:1}).exec((errPayRec,resultPayRec)=>{
							if(errPayRec)
							{
								var return_response = {"error":true,success: false,errorMessage:errPayRec};
									res.status(200)
									.send(return_response);
							}else{
								// console.log("hereeeee");
								// 	console.log(resultPayRec);
								// 	return false;
								if(resultPayRec)
								{
									
									UsersModel.findOne({_id:resultPayRec.user_id},{email:1}).exec((errUser,resultUser)=>{
										if(errUser)
										{
											var return_response = {"error":true,success: false,errorMessage:errUser};
											res.status(200)
											.send(return_response);
										}else{
											if(resultUser)
											{
												var html = "";
												html+= "<p>Merci pour votre généreux don à Cœur d'Orphelins. Nous sommes ravis de bénéficier de votre soutien. Grâce à votre don, nous avons pu atteindre des objectifs et continuer à travailler dans ce sens.</p>";
												html+= "<p>Vous faites vraiment la différence pour nous, et nous vous en sommes extrêmement reconnaissants !</p>";
												html+= "<p>Montant du don : "+resultPayRec.price+" € </p>";
												const message = {
													from: "noreply@coeurorphelins.org Coeur d'Orphelins", // Sender address
													to: resultUser.email,         // recipients
													subject: "Merci pour le récent don fait à Coeur d'Orphelins", // Subject line
													html: html
												};
												transport.sendMail(message, function(err, info) {
														if (err) {
															res.status(200)
															.send({
																	error: false,
																	success: true,
																	errorMessage: "Succès du paiement"
															});														
														} else {
															//console.log('mail has sent.');
															res.status(200)
															.send({
																	error: false,
																	success: true,
																	errorMessage: "Succès du paiement"
															});
														}
												});
											}else{
												res.status(200)
												.send({
														error: false,
														success: true,
														errorMessage: "Succès du paiement"
												});
											}
										}
									});
								}else{
									res.status(200)
										.send({
												error: false,
												success: true,
												errorMessage: "Succès du paiement"
										});
								}
							}
						});
			    }
		    });
			}else{
				res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: "Non payé"
                  });
			}
		}else{
			res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Une erreur"
              });
		}
	},
	getDonationStripeCancel:async function(req,res,next)
	{
		console.log("cancel stripe");
		res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: "Not paid"
      });
	},


	testPaypalPayment:async function(req,res,next)
	{
		 const create_payment_json = {
	    "intent": "sale",
	    "payer": {
	        "payment_method": "paypal"
	    },
	    "redirect_urls": {
	        "return_url": "https://eff6-2405-201-300c-9846-7509-55eb-7262-4ded.in.ngrok.io/api/testPaypalSuccessPayment",
	        "cancel_url": "https://eff6-2405-201-300c-9846-7509-55eb-7262-4ded.in.ngrok.io/api/testPaypalCancelPayment"
	    },
	    "transactions": [{
	        "item_list": {
	            "items": [{
	                "name": "Product name",
	                "sku": "001",
	                "price": "1.00",
	                "currency": "EUR",
	                "quantity": 1
	            }]
	        },
	        "amount": {
	            "currency": "EUR",
	            "total": "1.00"
	        },
	        "description": 134
	    }]
		};

		paypal.payment.create(create_payment_json, function (error, payment) {
		  if (error) {
		      throw error;
		  } else {
		      for(let i = 0;i < payment.links.length;i++){
		        if(payment.links[i].rel === 'approval_url'){
		          res.redirect(payment.links[i].href);
		        }
		      }
		  }
		});

	},
	testPaypalSuccessPayment:async function(req,res,next)
	{
		console.log("success paypal");

	  const payerId = req.query.PayerID;
	  const paymentId = req.query.paymentId;
	  console.log("payerId");
	  console.log(payerId);
	  const execute_payment_json = {
	    "payer_id": payerId,
	    "transactions": [{
	        "amount": {
	            "currency": "EUR",
	            "total": "1.00"
	        }
	    }]
	  };

		// Obtains the transaction details from paypal
		  paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
		      //When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
		    if (error) {
		        console.log(error.response);
		        throw error;
		    } else {
		        console.log(JSON.stringify(payment));
		        console.log(JSON.stringify(payment.id));
		        console.log(JSON.stringify(payment.transactions[0].description));
		        res.send('Success');
		    }
		});
	},
	testPaypalCancelPayment:async function(req,res,next)
	{
		console.log("cancel paypal");
	},
	getPaypalDonationPayment:async function(req,res,next)
	{
		var order_id = Math.floor(10000000 + Math.random() * 90000000);
		var base_url = customConstant.base_url;
		//console.log(req.query.payment_id);Paymentrecords
		var user_id = req.query.user_id;
		var payment_id = req.query.payment_id;
		var type = req.query.type;
		var price = req.query.price;
		var category_title = subcategory_title = description = "";
		if(payment_id && type && user_id && price)
		{
			/*console.log(payment_id);
			console.log(type);
			return false;*/
			if(type == "subCategory")
			{
				var subCategoryRecord = await SubCategoryModel.find({ "_id":payment_id }); 
				if(subCategoryRecord)
				{
					//console.log(subCategoryRecord[0].category);
					var category_id = subCategoryRecord[0].category;
					var categoryRecord = await CategoryModel.find({ "_id":category_id }); 
					if(categoryRecord)
					{
						/*console.log(categoryRecord);
						console.log(categoryRecord[0].category);*/
						var subcategory_id = payment_id;
						var category_title = categoryRecord[0].category;
						var subcategory_title = subCategoryRecord[0].title;
						var description = subCategoryRecord[0].description;
						//var price = subCategoryRecord[0].price;
						var payment_mode = type;
						var payment_initiate_date = new Date();
						

						if(subCategoryRecord[0].images)
						{
							//console.log(subCategoryRecord[0].images);
							if(subCategoryRecord[0].images.length > 0)
							{
								var imagePath = subCategoryRecord[0].images[0].path;
								//console.log(imagePath);
								var imageName = subCategoryRecord[0].images[0].filename;
								var imagePath = subCategoryRecord[0].images[0].path;
								//console.log(imagePath);

								//const filePath = './uploads/my-image.png';
								filePathCopy = './public/uploads/payments/'+imageName;
								fs.copyFile(imagePath, filePathCopy, (err) =>
								{
								  if (err) throw err;
								  console.log('File Copy Successfully.');
								});

							}
						}


						Paymentrecords.create({
							order_id:order_id,
		          payment_id:payment_id,
		          category_id:category_id,
		          filePathCopy:imageName,
		          subcategory_id:subcategory_id,
		          user_id:user_id,
		          payment_mode:payment_mode,
		          category_title:category_title,
		          subcategory_title:subcategory_title,
		          description:description,
		          price:price,
		          payment_type:"Paypal",
		          status:0,
		          payment_initiate_date:payment_initiate_date,
		        },function(err,result){
		          if(err)
		          {
		            var return_response = {"error":true,success: false,errorMessage:err};
		              res.status(200)
		              .send(return_response);
		          }else{
		          	var lastInsertId = result._id;
		          	console.log(lastInsertId);
		          	var redirect_url_web = base_url+"api/getPaypalDonationPaymentNew?id="+lastInsertId+"&price="+price+"&title="+subcategory_title;
							  res.redirect(303, redirect_url_web);
		          }
		        });
					}else{
						res.status(200)
			        .send({
		            error: true,
		            success: false,
		            errorMessage: "Identifiant de paiement non valide"
		        });
					}
				}else{
					res.status(200)
		        .send({
	            error: true,
	            success: false,
	            errorMessage: "Identifiant de paiement non valide"
	        });
				}
			}else if(type == "Category")
			{
					var category_id = payment_id;
					var categoryRecord = await CategoryModel.find({ "_id":category_id }); 
					if(categoryRecord)
					{
						//console.log(categoryRecord);
						//console.log(categoryRecord[0].category);
						//return false;
						var subcategory_id = payment_id;
						var category_title = categoryRecord[0].category;
						var subcategory_title = null;
						var description = null;
						//var price = categoryRecord[0].price;
						var payment_mode = type;
						var payment_initiate_date = new Date();
						
						if(categoryRecord[0].images)
						{
							//console.log(categoryRecord[0].images);
							if(categoryRecord[0].images.length > 0)
							{
								var imagePath = categoryRecord[0].images[0].path;
								//console.log(imagePath);
								var imageName = categoryRecord[0].images[0].filename;
								var imagePath = categoryRecord[0].images[0].path;
								//console.log(imagePath);

								//const filePath = './uploads/my-image.png';
								filePathCopy = './public/uploads/payments/'+imageName;
								fs.copyFile(imagePath, filePathCopy, (err) =>
								{
								  if (err) throw err;
								  console.log('File Copy Successfully.');
								});

							}
						}


						Paymentrecords.create({
							order_id:order_id,
		          payment_id:payment_id,
		          category_id:category_id,
		          filePathCopy:imageName,
		          subcategory_id:null,
		          user_id:user_id,
		          payment_mode:payment_mode,
		          category_title:category_title,
		          subcategory_title:null,
		          description:null,
		          price:price,
		          payment_type:"Paypal",
		          status:0,
		          payment_initiate_date:payment_initiate_date,
		        },function(err,result){
		          if(err)
		          {
		            var return_response = {"error":true,success: false,errorMessage:err};
		              res.status(200)
		              .send(return_response);
		          }else{
		          	var lastInsertId = result._id;
		          	console.log(lastInsertId);
		          	var redirect_url_web = base_url+"api/getPaypalDonationPaymentNew?id="+lastInsertId+"&price="+price+"&title="+category_title;
							  res.redirect(303, redirect_url_web);
		          }
		        });
					}else{
						res.status(200)
			        .send({
		            error: true,
		            success: false,
		            errorMessage: "Identifiant de paiement non valide"
		        });
					}
			}else{

			}
		}else{
			res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant de paiement, le type de paiement, l'identifiant de l'utilisateur et le prix sont tous requis"
        });
		}
		  

	},
	getPaypalDonationPaymentNew:async function(req,res,next)
	{
		await PaypalModel.findOne({}).exec((errPaypal,resultPaypal)=>{
			if(errPaypal)
			{
				var return_response = {"error":true,success: false,errorMessage:errPaypal};
					res.status(200)
					.send(return_response);
			}else{
				if(resultPaypal)
				{
					paypal.configure({
						'mode': resultPaypal.mode, //sandbox or live
						'client_id': resultPaypal.client_id,
						'client_secret': resultPaypal.client_secret
					});

					var updated_at = new Date();
					var base_url = customConstant.base_url; 
					var lastInsertId = req.query.id;
					var price = req.query.price;
					var title = req.query.title;
					var return_url = base_url+"api/getPaypalDonationSuccess";
					var cancel_url = base_url+"api/getPaypalDonationCancel";
					const create_payment_json = {
						"intent": "sale",
						"payer": {
								"payment_method": "paypal"
						},
						"redirect_urls": {
								"return_url": return_url,
								"cancel_url": cancel_url
						},
						"transactions": [{
								"item_list": {
										"items": [{
												"name": title,
												"sku": lastInsertId,
												"price": price,
												"currency": "EUR",
												"quantity": 1
										}]
								},
								"amount": {
										"currency": "EUR",
										"total": price
								},
								"description": lastInsertId
						}]
					};

					paypal.payment.create(create_payment_json, function (error, payment) {
						if (error) {
								throw error;
						} else {
								for(let i = 0;i < payment.links.length;i++){
									if(payment.links[i].rel === 'approval_url'){
										res.redirect(payment.links[i].href);
									}
								}
						}
					});
				}else{
					var return_response = {"error":true,success: false,errorMessage:"ID Paypal non valide"};
					res.status(200)
					.send(return_response);
				}
			}
		});
		
	},
	getPaypalDonationSuccess:async function(req,res,next)
	{
		await PaypalModel.findOne({}).exec((errPaypal,resultPaypal)=>{
			if(errPaypal)
			{
				var return_response = {"error":true,success: false,errorMessage:errPaypal};
					res.status(200)
					.send(return_response);
			}else{
				if(resultPaypal)
				{
					//console.log("success paypal");
					var updated_at = new Date();
					const payerId = req.query.PayerID;
					const paymentId = req.query.paymentId;
					const token = req.query.token;
					//console.log("payerId");
					//console.log(payerId);
					//console.log(token);
					paypal.configure({
						'mode': resultPaypal.mode, //sandbox or live
						'client_id': resultPaypal.client_id,
						'client_secret': resultPaypal.client_secret
					});
					const execute_payment_json = {
						"payer_id": payerId
					};
					//Obtains the transaction details from paypal							//When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
						paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
						if (error)
						{
							console.log(error.response);
							throw error;
						}else{
								//console.log(JSON.stringify(payment));
								//console.log(JSON.stringify(payment.id));
								var payment_table_id = JSON.stringify(payment.transactions[0].description);
								payment_table_id = JSON.parse(payment_table_id);
								//console.log("payment_table_id");
								//console.log(payment_table_id);
								Paymentrecords.findOneAndUpdate({ _id:payment_table_id },{
									status:1,
									transaction_id:payerId,
									payment_intent:token,
									payment_success_date:updated_at,
									updated_at:updated_at
								},function(err,result){
									if(err)
									{
										var return_response = {"error":true,success: false,errorMessage:err};
											res.status(200)
											.send(return_response);
									}else{
										Paymentrecords.findOne({_id:payment_table_id},{user_id:1,price:1}).exec((errPayRec,resultPayRec)=>{
											if(errPayRec)
											{
												var return_response = {"error":true,success: false,errorMessage:errPayRec};
													res.status(200)
													.send(return_response);
											}else{
												// console.log("hereeeee");
												// 	console.log(resultPayRec);
												// 	return false;
												if(resultPayRec)
												{
													
													UsersModel.findOne({_id:resultPayRec.user_id},{email:1}).exec((errUser,resultUser)=>{
														if(errUser)
														{
															var return_response = {"error":true,success: false,errorMessage:errUser};
															res.status(200)
															.send(return_response);
														}else{
															if(resultUser)
															{
																var html = "";
																html+= "<p>Merci pour votre généreux don à Cœur d'Orphelins. Nous sommes ravis de bénéficier de votre soutien. Grâce à votre don, nous avons pu atteindre des objectifs et continuer à travailler dans ce sens.</p>";
																html+= "<p>Vous faites vraiment la différence pour nous, et nous vous en sommes extrêmement reconnaissants !</p>";
																html+= "<p>Montant du don : "+resultPayRec.price+" € </p>";
																const message = {
																	from: "noreply@coeurorphelins.org Coeur d'Orphelins", // Sender address
																	to: resultUser.email,         // recipients
																	subject: "Merci pour le récent don fait à Coeur d'Orphelins", // Subject line
																	html: html
																};
																transport.sendMail(message, function(err, info) {
																		if (err) {
																			res.status(200)
																			.send({
																					error: false,
																					success: true,
																					errorMessage: "Succès du paiement"
																			});														
																		} else {
																			//console.log('mail has sent.');
																			res.status(200)
																			.send({
																					error: false,
																					success: true,
																					errorMessage: "Succès du paiement"
																			});
																		}
																});
															}else{
																res.status(200)
																.send({
																		error: false,
																		success: true,
																		errorMessage: "Succès du paiement"
																});
															}
														}
													});
												}else{
													res.status(200)
														.send({
																error: false,
																success: true,
																errorMessage: "Succès du paiement"
														});
												}
											}
										});
									}
								});
								//res.send('Success');
						}
					});
				}else{
					var return_response = {"error":true,success: false,errorMessage:"ID Paypal non valide"};
					res.status(200)
					.send(return_response);
				}

			}

		});
	},
	getPaypalDonationCancel:async function(req,res,next)
	{
		console.log("cancel paypal");
		res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: "Non payé"
      });
	},
	getPaypalProjectPaymentSuccess:async function(req,res,next)
	{
		await PaypalModel.findOne({}).exec((errPaypal,resultPaypal)=>{
			if(errPaypal)
			{
				var return_response = {"error":true,success: false,errorMessage:errPaypal};
					res.status(200)
					.send(return_response);
			}else{
				if(resultPaypal)
				{
					//console.log("success paypal");
					var updated_at = new Date();
					const payerId = req.query.PayerID;
					const paymentId = req.query.paymentId;
					const token = req.query.token;
					//console.log("payerId");
					//console.log(payerId);
					//console.log(token);
					paypal.configure({
						'mode': resultPaypal.mode, //sandbox or live
						'client_id': resultPaypal.client_id,
						'client_secret': resultPaypal.client_secret
					});
					const execute_payment_json = {
						"payer_id": payerId
					};
					//Obtains the transaction details from paypal							//When error occurs when due to non-existent transaction, throw an error else log the transaction details in the console then send a Success string reposponse to the user.
						paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
						if (error)
						{
							console.log(error.response);
							throw error;
						}else{
								//console.log(JSON.stringify(payment));
								//console.log(JSON.stringify(payment.id));
								var payment_table_id = JSON.stringify(payment.transactions[0].description);
								payment_table_id = JSON.parse(payment_table_id);
								//console.log("payment_table_id");
								//console.log(payment_table_id);
								Paymentrecords.findOneAndUpdate({ _id:payment_table_id },{
									status:1,
									transaction_id:payerId,
									payment_intent:token,
									payment_success_date:updated_at,
									updated_at:updated_at
								},function(err,result){
									if(err)
									{
										var return_response = {"error":true,success: false,errorMessage:err};
											res.status(200)
											.send(return_response);
									}else{
										Paymentrecords.findOne({_id:payment_table_id},{user_id:1,price:1}).exec((errPayRec,resultPayRec)=>{
											if(errPayRec)
											{
												var return_response = {"error":true,success: false,errorMessage:errPayRec};
													res.status(200)
													.send(return_response);
											}else{
												// console.log("hereeeee");
												// 	console.log(resultPayRec);
												// 	return false;
												if(resultPayRec)
												{
													
													UsersModel.findOne({_id:resultPayRec.user_id},{email:1}).exec((errUser,resultUser)=>{
														if(errUser)
														{
															var return_response = {"error":true,success: false,errorMessage:errUser};
															res.status(200)
															.send(return_response);
														}else{
															if(resultUser)
															{
																var html = "";
																html+= "<p>Merci pour votre généreux don à Cœur d'Orphelins. Nous sommes ravis de bénéficier de votre soutien. Grâce à votre don, nous avons pu atteindre des objectifs et continuer à travailler dans ce sens.</p>";
																html+= "<p>Vous faites vraiment la différence pour nous, et nous vous en sommes extrêmement reconnaissants !</p>";
																html+= "<p>Montant du don : "+resultPayRec.price+" € </p>";
																const message = {
																	from: "noreply@coeurorphelins.org Coeur d'Orphelins", // Sender address
																	to: resultUser.email,         // recipients
																	subject: "Merci pour le récent don fait à Coeur d'Orphelins", // Subject line
																	html: html
																};
																transport.sendMail(message, function(err, info) {
																		if (err) {
																			res.status(200)
																			.send({
																					error: false,
																					success: true,
																					errorMessage: "Succès du paiement"
																			});														
																		} else {
																			//console.log('mail has sent.');
																			res.status(200)
																			.send({
																					error: false,
																					success: true,
																					errorMessage: "Succès du paiement"
																			});
																		}
																});
															}else{
																res.status(200)
																.send({
																		error: false,
																		success: true,
																		errorMessage: "Succès du paiement"
																});
															}
														}
													});
												}else{
													res.status(200)
														.send({
																error: false,
																success: true,
																errorMessage: "Succès du paiement"
														});
												}
											}
										});
									}
								});
								//res.send('Success');
						}
					});
				}else{
					var return_response = {"error":true,success: false,errorMessage:"ID Paypal non valide"};
					res.status(200)
					.send(return_response);
				}

			}

		});
	},
	getPaypalProjectPaymentCancel:async function(req,res,next)
	{
		console.log("cancel paypal");
		res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: "Non payé"
      });
	},
	doDonation:async function(req,res,next)
	{
		var base_url = customConstant.base_url;
		var order_id = Math.floor(10000000 + Math.random() * 90000000);
		//console.log(req.body.payment_id);Paymentrecords
		var user_id = req.body.user_id;
		var payment_id = req.body.payment_id;
		var type = req.body.type;
		var price = req.body.price;
		var payment_type = req.body.payment_type;
		var firstName = req.body.firstName;
		var lastName = req.body.lastName;
		var email = req.body.email;
		var mobileNumber = req.body.mobileNumber;
		var pays = req.body.pays;
		var company_name = req.body.company_name;
		var whatsappNumber = req.body.whatsappNumber;

		var countryCode = req.body.countryCode;
		var whatsAppCountryCode = req.body.whatsAppCountryCode;


		var firstName1 = req.body.firstName1;
		var lastName1 = req.body.lastName1;
		var firstName2 = req.body.firstName2;
		var lastName2 = req.body.lastName2;
		var firstName3 = req.body.firstName3;
		var lastName3 = req.body.lastName3;
		var firstName4 = req.body.firstName4;
		var lastName4 = req.body.lastName4;
		var firstName5 = req.body.firstName5;
		var lastName5 = req.body.lastName5;
		var firstName6 = req.body.firstName6;
		var lastName6 = req.body.lastName6;

		var category_title = subcategory_title = description = "";
		/*console.log(payment_id);
		console.log(type);
		return false;*/
		if(type == "subCategory")
		{
			var subCategoryRecord = await SubCategoryModel.find({ "_id":payment_id }); 
			if(subCategoryRecord)
			{
				//console.log(subCategoryRecord[0].category);
				var category_id = subCategoryRecord[0].category;
				var categoryRecord = await CategoryModel.find({ "_id":category_id }); 
				if(categoryRecord)
				{
					/*console.log(categoryRecord);
					console.log(categoryRecord[0].category);*/
					var subcategory_id = payment_id;
					var category_title = categoryRecord[0].category;
					var subcategory_title = subCategoryRecord[0].title;
					var description = subCategoryRecord[0].description;
					//var price = subCategoryRecord[0].price;
					var payment_mode = type;
					var payment_initiate_date = new Date();
					

					if(subCategoryRecord[0].images)
					{
						//console.log(subCategoryRecord[0].images);
						if(subCategoryRecord[0].images.length > 0)
						{
							var imagePath = subCategoryRecord[0].images[0].path;
							//console.log(imagePath);
							var imageName = subCategoryRecord[0].images[0].filename;
							var imagePath = subCategoryRecord[0].images[0].path;
							//console.log(imagePath);

							//const filePath = './uploads/my-image.png';
							filePathCopy = './public/uploads/payments/'+imageName;
							fs.copyFile(imagePath, filePathCopy, (err) =>
							{
							  if (err) throw err;
							  console.log('File Copy Successfully.');
							});

						}
					}



					Paymentrecords.create({
						order_id:order_id,
	          firstName:firstName,
	          lastName:lastName,
	          email:email,
	          mobileNumber:mobileNumber,
	          pays:pays,
	          company_name:company_name,
	          payment_id:payment_id,
	          category_id:category_id,
	          subcategory_id:subcategory_id,
	          filePathCopy:imageName,
	          user_id:user_id,
	          payment_mode:payment_mode,
	          category_title:category_title,
	          subcategory_title:subcategory_title,
	          description:description,
	          price:price,
	          payment_type:payment_type,
	          status:0,
	          payment_initiate_date:payment_initiate_date,
						countryCode:countryCode,
						whatsAppCountryCode:whatsAppCountryCode,
						whatsappNumber:whatsappNumber,
						firstName1:firstName1,
						lastName1:lastName1,
						firstName2:firstName2,
						lastName2:lastName2,
						firstName3:firstName3,
						lastName3:lastName3,
						firstName4:firstName4,
						lastName4:lastName4,
						firstName5:firstName5,
						lastName5:lastName5,
						firstName6:firstName6,
						lastName6:lastName6,
	        },function(err,result){
	          if(err)
	          {
	            var return_response = {"error":true,success: false,errorMessage:err};
	              res.status(200)
	              .send(return_response);
	          }else{

	          	var lastInsertId = result._id;
	          	console.log(lastInsertId);
	          	if(payment_type == "Paypal")
	          	{
	          		var redirect_url_web = base_url+"api/getPaypalDonationPaymentNew?id="+lastInsertId+"&price="+price+"&title="+subcategory_title;
	          	}else{
	          		var redirect_url_web = base_url+"api/getDonationStripePaymentNew?id="+lastInsertId+"&price="+price+"&title="+subcategory_title;
	          	}
						  //res.redirect(303, redirect_url_web);
						  res.status(200)
				        .send({
			            error: false,
			            success: true,
			            errorMessage: "Succès",
			            redirect_url_web: redirect_url_web,
			        });
	          }
	        });
				}else{
					res.status(200)
		        .send({
	            error: true,
	            success: false,
	            errorMessage: "Identité de paiement invalide"
	        });
				}
			}else{
				res.status(200)
	        .send({
            error: true,
            success: false,
            errorMessage: "Identité de paiement invalide"
        });
			}
		}else if(type == "Category")
		{
				var category_id = payment_id;
				var categoryRecord = await CategoryModel.find({ "_id":category_id }); 
				if(categoryRecord)
				{
					//console.log(categoryRecord);
					//console.log(categoryRecord[0].category);
					//return false;
					var subcategory_id = payment_id;
					var category_title = categoryRecord[0].category;
					var subcategory_title = null;
					var description = null;
					//var price = categoryRecord[0].price;
					var payment_mode = type;
					var payment_initiate_date = new Date();
					
					if(categoryRecord[0].images)
					{
						//console.log(categoryRecord[0].images);
						if(categoryRecord[0].images.length > 0)
						{
							var imagePath = categoryRecord[0].images[0].path;
							//console.log(imagePath);
							var imageName = categoryRecord[0].images[0].filename;
							var imagePath = categoryRecord[0].images[0].path;
							//console.log(imagePath);

							//const filePath = './uploads/my-image.png';
							filePathCopy = './public/uploads/payments/'+imageName;
							fs.copyFile(imagePath, filePathCopy, (err) =>
							{
							  if (err) throw err;
							  console.log('File Copy Successfully.');
							});

						}
					}

						
					Paymentrecords.create({
						order_id:order_id,
						firstName:firstName,
	          lastName:lastName,
	          email:email,
	          mobileNumber:mobileNumber,
	          pays:pays,
	          company_name:company_name,
	          payment_id:payment_id,
	          category_id:category_id,
	          subcategory_id:null,
	          filePathCopy:imageName,
	          user_id:user_id,
	          payment_mode:payment_mode,
	          category_title:category_title,
	          subcategory_title:null,
	          description:null,
	          price:price,
	          payment_type:payment_type,
	          status:0,
	          payment_initiate_date:payment_initiate_date,
						countryCode:countryCode,
						whatsAppCountryCode:whatsAppCountryCode,
						whatsappNumber:whatsappNumber,
						firstName1:firstName1,
						lastName1:lastName1,
						firstName2:firstName2,
						lastName2:lastName2,
						firstName3:firstName3,
						lastName3:lastName3,
						firstName4:firstName4,
						lastName4:lastName4,
						firstName5:firstName5,
						lastName5:lastName5,
						firstName6:firstName6,
						lastName6:lastName6,
	        },function(err,result){
	          if(err)
	          {
	            var return_response = {"error":true,success: false,errorMessage:err};
	              res.status(200)
	              .send(return_response);
	          }else{

	          	var lastInsertId = result._id;
	          	console.log(lastInsertId);
	          	

	          	if(payment_type == "Paypal")
	          	{
	          		var redirect_url_web = base_url+"api/getPaypalDonationPaymentNew?id="+lastInsertId+"&price="+price+"&title="+category_title;
	          	}else{
	          		var redirect_url_web = base_url+"api/getDonationStripePaymentNew?id="+lastInsertId+"&price="+price+"&title="+category_title;
	          	}
						  //res.redirect(303, redirect_url_web);
						  res.status(200)
				        .send({
			            error: false,
			            success: true,
			            errorMessage: "Succès",
			            redirect_url_web: redirect_url_web,
			        });
	          }
	        });
				}else{
					res.status(200)
		        .send({
	            error: true,
	            success: false,
	            errorMessage: "Identifiant de paiement non valide"
	        });
				}
		}else{
			res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'identifiant du paiement, le type de paiement et l'identifiant de l'utilisateur sont tous nécessaires."
        });
		}
	},
	submitProjectPayment:async function(req,res)
	{
		try{
			//console.log(req.body);
			let { project_id,firstName,lastName,message,secondFirstName,secondLastName,address,latitude,longitude,secondAddress,secondLatitude,secondLongitude,city,zip_code,pays,email,phone_code,mobileNumber,price,is_additional,total_price,user_id,project_name } = req.body;
			let payment_type = "Paypal";
			if(!project_id)
			{
				return res.status(200)
				.send({
						error: true,
						success: false,
						errorMessage: "L'identifiant du projet est requis"
				});
			}
			if(is_additional == true || is_additional == "true")
			{
				is_additional = true;
			}else{
				is_additional = false;
			}
			var addressLocation =  {};
			if(latitude && longitude)
			{
				latitude = parseFloat(latitude);
				longitude = parseFloat(longitude);
				addressLocation =  {
					type: "Point",
					coordinates: [longitude,latitude]
				};
			}
			var secondLocation =  {};
			if(secondLatitude && secondLongitude)
			{
				secondLatitude = parseFloat(secondLatitude);
				secondLongitude = parseFloat(secondLongitude);
				secondLocation =  {
					type: "Point",
					coordinates: [secondLongitude,secondLatitude]
				};
			}
			await Paymentrecords.create({
				project_id,firstName,lastName,message,secondFirstName,secondLastName,address,addressLocation,secondAddress,secondLocation,city,zip_code,pays,email,phone_code,mobileNumber,price,is_additional,total_price,payment_type,user_id,project_name
			}).then((result)=>{

				var base_url = customConstant.base_url; 

				return res.status(200)
				.send({
						error: false,
						success: true,
						errorMessage: "Veuillez poursuivre le paiement de votre don",
						url:base_url+"api/getProjectDonation?id="+result._id
				});
			}).catch((e)=>{
				return res.status(200)
				.send({
						error: true,
						success: false,
						errorMessage: e.message
				});
			})
		}catch(e)
		{
			return res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: e.message
      });
		}
	},
	submitProjectPaymentSecond:async function(req,res)
	{
		try{
			//console.log(req.body);
			let { project_name,firstName,lastName,address,latitude,longitude,secondAddress,secondLatitude,secondLongitude,city,zip_code,pays,email,phone_code,mobileNumber,price,is_additional,total_price,user_id } = req.body;
			let payment_type = "Paypal";
			if(!project_name)
			{
				return res.status(200)
				.send({
						error: true,
						success: false,
						errorMessage: "Le nom du projet est requis"
				});
			}
			if(is_additional == true || is_additional == "true")
			{
				is_additional = true;
			}else{
				is_additional = false;
			}
			var addressLocation =  {};
			if(latitude && longitude)
			{
				latitude = parseFloat(latitude);
				longitude = parseFloat(longitude);
				addressLocation =  {
					type: "Point",
					coordinates: [longitude,latitude]
				};
			}
			var secondLocation =  {};
			if(secondLatitude && secondLongitude)
			{
				secondLatitude = parseFloat(secondLatitude);
				secondLongitude = parseFloat(secondLongitude);
				secondLocation =  {
					type: "Point",
					coordinates: [secondLongitude,secondLatitude]
				};
			}
			await Paymentrecords.create({
				project_name,firstName,lastName,address,addressLocation,secondAddress,secondLocation,city,zip_code,pays,email,phone_code,mobileNumber,price,is_additional,total_price,payment_type,user_id
			}).then((result)=>{

				var base_url = customConstant.base_url; 

				return res.status(200)
				.send({
						error: false,
						success: true,
						errorMessage: "Veuillez poursuivre le paiement de votre don",
						url:base_url+"api/getProjectDonation?id="+result._id
				});
			}).catch((e)=>{
				return res.status(200)
				.send({
						error: true,
						success: false,
						errorMessage: e.message
				});
			})
		}catch(e)
		{
			return res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: e.message
      });
		}
	},
	submitProjectPaymentThird:async function(req,res)
	{
		try{
			//console.log(req.body);
			let { project_name,firstName,lastName,address,latitude,longitude,secondAddress,secondLatitude,secondLongitude,city,zip_code,pays,email,phone_code,mobileNumber,price,quantity,total_price,user_id,category_id } = req.body;
			if(quantity)
			{
				quantity = parseFloat(quantity);
			}
			let payment_type = "Paypal";
			if(!project_name)
			{
				return res.status(200)
				.send({
						error: true,
						success: false,
						errorMessage: "Le nom du projet est requis"
				});
			}
		 
			var addressLocation =  {};
			if(latitude && longitude)
			{
				latitude = parseFloat(latitude);
				longitude = parseFloat(longitude);
				addressLocation =  {
					type: "Point",
					coordinates: [longitude,latitude]
				};
			}
			var secondLocation =  {};
			if(secondLatitude && secondLongitude)
			{
				secondLatitude = parseFloat(secondLatitude);
				secondLongitude = parseFloat(secondLongitude);
				secondLocation =  {
					type: "Point",
					coordinates: [secondLongitude,secondLatitude]
				};
			}
			await Paymentrecords.create({
				project_name,firstName,lastName,address,addressLocation,secondAddress,secondLocation,city,zip_code,pays,email,phone_code,mobileNumber,price,quantity,total_price,payment_type,user_id,category_id
			}).then((result)=>{

				var base_url = customConstant.base_url; 

				return res.status(200)
				.send({
						error: false,
						success: true,
						errorMessage: "Veuillez poursuivre le paiement de votre don",
						url:base_url+"api/getProjectDonation?id="+result._id
				});
			}).catch((e)=>{
				return res.status(200)
				.send({
						error: true,
						success: false,
						errorMessage: e.message
				});
			})
		}catch(e)
		{
			return res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: e.message
      });
		}
	},
	getProjectDonation:async function(req,res,next)
	{
		try{
			let resultPaypal = await PaypalModel.findOne({});
			if(resultPaypal)
				{
					paypal.configure({
						'mode': resultPaypal.mode, //sandbox or live
						'client_id': resultPaypal.client_id,
						'client_secret': resultPaypal.client_secret
					});

					var updated_at = new Date();
					var base_url = customConstant.base_url; 
					var lastInsertId = req.query.id;
					console.log("lastInsertId ", lastInsertId);
					let data_rec = await Paymentrecords.findOne({_id:lastInsertId});
					if(!data_rec)
					{
						return res.status(200)
						.send({
								error: true,
								success: false,
								errorMessage: "ID de paiement invalide"
						});
					}else{
						let price = parseFloat(data_rec.total_price);
						var return_url = base_url+"api/getPaypalProjectPaymentSuccess";
						var cancel_url = base_url+"api/getPaypalProjectPaymentCancel";
						const create_payment_json = {
							"intent": "sale",
							"payer": {
									"payment_method": "paypal"
							},
							"redirect_urls": {
									"return_url": return_url,
									"cancel_url": cancel_url
							},
							"transactions": [{
									"item_list": {
											"items": [{
													"name": "Coeurorphelins",
													"sku": lastInsertId,
													"price": price,
													"currency": "EUR",
													"quantity": 1
											}]
									},
									"amount": {
											"currency": "EUR",
											"total": price
									},
									"description": lastInsertId
							}]
						};

						paypal.payment.create(create_payment_json, function (error, payment) {
							if (error) {
									throw error;
							} else {
									for(let i = 0;i < payment.links.length;i++){
										if(payment.links[i].rel === 'approval_url'){
											res.redirect(payment.links[i].href);
										}
									}
							}
						});
					}
					
				}else{
					var return_response = {"error":true,success: false,errorMessage:"ID Paypal non valide"};
					res.status(200)
					.send(return_response);
				}
		}catch(e)
		{
			return res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: e.message
      });
		}
	

		
	},
}
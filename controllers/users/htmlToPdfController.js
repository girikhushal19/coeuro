const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const SubCategoryModel = require("../../models/SubCategoryModel");
const ProjectModel = require("../../models/ProjectModel");
const GalleryModel = require("../../models/GalleryModel");
const ForumModel = require("../../models/ForumModel");
const CommentsModel = require("../../models/CommentsModel");
const AboutUsModel = require('../../models/AboutUsModel');
const CommentLikeDislikeModel = require('../../models/CommentLikeDislikeModel');
const ForumLikeDislikeModel = require('../../models/ForumLikeDislikeModel');
const AdViewsModel = require('../../models/AdViewsModel');
const PrivacyAndPolicyModel = require('../../models/PrivacyAndPolicyModel');
const TermsAndConditionModel = require('../../models/TermsAndConditionModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
var html_to_pdf = require('html-pdf-node');
var pdf = require('dynamic-html-pdf');
const puppeteer = require('puppeteer');
const Paymentrecords = require("../../models/Paymentrecords");


/*/*/
module.exports = {

  testHtmlToPdf:async function(req,res,next)
  {
    try{
      (async () => {
        console.log("hereeeeeee");
        /*var html = "";
        html += "<!DOCTYPE html>";
        html += "<html>";
        html += "<head>";
        html += "<title>HTML content</title>";
        html += "</head>";
        html += "<body>";
        html += "<h1>Sample</h1>";
        html += "<div>";
        html += "<p>";
        html += "</p><ul>";
        html += "<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>";
        html += "<li>Integer interdum felis nec orci mattis, ac dignissim mauris commodo.</li>";
        html += "</ul>";
        html += "<p></p>";
        html += "<p>";
        html += "</p><ul>";
        html += "<li>In et augue non turpis faucibus tincidunt a et lectus.</li>";
        html += "<li>Nulla congue nisi vel diam hendrerit, at pulvinar massa aliquam.</li>";
        html += "</ul>";
        html += "<p></p>";
        html += "</div>";
        html += "</body>";
        html += "</html>";

        // Create a browser instance
        const browser = await puppeteer.launch();
        // Create a new page
        const page = await browser.newPage();
        //Get HTML content from HTML file
        //const html = fs.readFileSync('sample.html', 'utf-8');
        await page.setContent(html, { waitUntil: 'domcontentloaded' });
        // To reflect CSS used for screens instead of print
        await page.emulateMediaType('screen');
        // Downlaod the PDF
        const pdf = await page.pdf({
          path: './public/uploads/pdf-file/result.pdf',
          margin: { top: '100px', right: '50px', bottom: '100px', left: '50px' },
          printBackground: true,
          format: 'A4',
        });
        // Close the browser instance
        var return_response = {"error":false,errorMessage:"Succès"};
        res.status(200).send(return_response);
        //await browser.close();
        await res.status(200).send(return_response);
        */
        
        const browser = await puppeteer.launch();

        // Create a new page
        const page = await browser.newPage();
      
        // Website URL to export as pdf
        const website_url = 'http://localhost:4200/invoiceOrder/1'; 
      
        // Open URL in current page
        await page.goto(website_url, { waitUntil: 'networkidle0' }); 
      
        //To reflect CSS used for screens instead of print
        await page.emulateMediaType('screen');
      
      // Downlaod the PDF
        const pdf = await page.pdf({
          path: './public/uploads/pdf-file/result.pdf',
          margin: { top: '100px', right: '50px', bottom: '100px', left: '50px' },
          printBackground: true,
          format: 'A4',
        });
        // Close the browser instance
        
        var return_response = {"error":false,errorMessage:"Succès"};
        await res.status(200).send(return_response);
      })();
    }catch(error){
      console.log(error);
    }
  },
  createPdfForPaymentRecords:async function(req,res,next)
  {
    try{
      (async () => {
          

        const browser = await puppeteer.launch({
          args: ['--no-sandbox', '--disable-setuid-sandbox'],
        });
        // Create a new page
        const page = await browser.newPage();
        var base_url_front_and = customConstant.base_url_front_and;
        //console.log(base_url_front_and);return false;
        var id = req.query.id;
        const website_url = base_url_front_and+'invoiceOrder/'+id; 
        console.log(website_url);
        // Open URL in current page
        await page.goto(website_url, { waitUntil: 'networkidle0' }); 
        //To reflect CSS used for screens instead of print
        await page.emulateMediaType('screen');
        // Downlaod the PDF
        const pdf = await page.pdf({
          path: './public/uploads/pdf-file/'+id+'_result.pdf',
          margin: { top: '100px', right: '50px', bottom: '100px', left: '50px' },
          printBackground: true,
          format: 'A4',
        });
        var return_response = {"error":false,errorMessage:"Succès"};
        await res.status(200).send(return_response);

      })();
    }catch(error){
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  },

  getSinglePaymentRecord:async function(req,res,next)
  {
    try{
      var id = req.body.id;
      Paymentrecords.updateOne({ "_id":id }, 
      {is_invoice:1}, function (uerr, docs) {
          if (uerr){
            //console.log(uerr);
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: uerr
              });
        }else{
            Paymentrecords.findOne({_id:id},{"firstName":1,"lastName":1,"email":1,"mobileNumber":1,"category_title":1,"price":1,"payment_initiate_date":1,"order_id":1,"payment_type":1}).exec((err,result)=>{
              if(err)
              {
                var return_response = {"error":true,errorMessage:err};
                res.status(200).send(return_response);
              }else{
                if(result)
                {
                  var return_response = {"error":false,errorMessage:"Success",record:result};
                  res.status(200).send(return_response);
                }else{
                  var return_response = {"error":true,errorMessage:"ID non valide"};
                  res.status(200).send(return_response);
                }
              }
            }); 
          }
        });
    }catch(error){
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  }
};
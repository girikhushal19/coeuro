const express = require('express');
const UsersModel = require("../../models/UsersModel");
const AdminModel = require('../../models/AdminModel');
const GalleryModel = require('../../models/GalleryModel');
const BannerModel = require('../../models/BannerModel');
const PushNotificationModel = require('../../models/PushNotificationModel');
const AboutUsModel = require('../../models/AboutUsModel');
const StripeModel = require('../../models/StripeModel');
const PaypalModel = require('../../models/PaypalModel');
const SettingModel = require('../../models/SettingModel');
const DonationTitleModel = require('../../models/DonationTitleModel');
const ProjectcontactsModel = require('../../models/ProjectcontactsModel');

const PrivacyAndPolicyModel = require('../../models/PrivacyAndPolicyModel');
const TermsAndConditionModel = require('../../models/TermsAndConditionModel');

const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');

var fcm = require('fcm-notification');
var FCM = new fcm('privatekey.json');

const app = express();
const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    host: 'mail.coeurorphelins.org',
    port: 465,
    auth: {
       user: 'noreply@coeurorphelins.org',
       pass: '*Ah}PRPwh=hQ'
    }
});

/*/*/
module.exports = {

  sendPushNotificationSubmit:async function(req,res,next)
  {
      try{
        /*console.log(req.files);
        console.log(req.files[0]);
        console.log(req.files[1]);*/
        //console.log(req.body);
      	var all_user = req.body.all_user; 	
        var title = req.body.title;   
      	var description = req.body.description; 	
      	/*console.log(title);
      	console.log(description);
      	return false;*/
        var user_array = [];
        for(let x=0; x<all_user.length; x++)
        {
          //console.log(all_user[x].item_id);
          user_array.push(all_user[x].item_id);
        }
        //console.log(user_array);
        var String_qr = {status:1,deviceToken: { $ne: null }};
  	    UsersModel.find( {"_id" : { $in : user_array }},{deviceToken:1} ).populate().lean(true).exec((err, result)=>
  	    {
  	        if(err)
  	        {
  	          console.log(err);
  	        }else{
              if(result.length > 0)
              {
                //console.log(result);
                var lastVal = result.length - 1; 
                for (var i = 0; i < result.length; i++)
                {
                  //console.log(result[i].email);
                  var message = {
                    data: {    //This is only optional, you can send any data
                        score: '850',
                        time: '2:45'
                    },
                    notification:{
                        title : title,
                        body : description
                    },
                    token : result[i].deviceToken
                  };
                  FCM.send(message, function(err, response) {
                      if(err){
                        ///console.log('error found', err);
                      }else {
                        ///console.log('response here', response);
                      }
                  });
                  if(lastVal == i)
                  {
                    PushNotificationModel.create({
                        user_id:user_array,
                        title:title,
                        description:description,
                      },function(err,result){
                       if(err)
                       {
                          var return_response = {"error":true,success: false,errorMessage:err};
                            res.status(200)
                            .send(return_response);
                      }else{
                          var return_response = {"error":false,success: true,errorMessage:"Succès"};
                            res.status(200)
                            .send(return_response);
                      }
                    });
                  }
                }
              }else{
                  PushNotificationModel.create({
                     user_id:user_array,
                    title:title,
                    description:description,
                  },function(err,result){
                   if(err)
                   {
                      var return_response = {"error":true,success: false,errorMessage:err};
                        res.status(200)
                        .send(return_response);
                  }else{
                      var return_response = {"error":false,success: true,errorMessage:"Succès"};
                        res.status(200)
                        .send(return_response);
                  }
                });
              }
  	        }
  	    });
      }catch (err) {
          console.log(err);
      }
  },
  getSingleAboutUsPage:async function(req,res,next)
  {
    AboutUsModel.find({  }, {}).lean(true).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        var return_response = {"error":false,success: true,errorMessage:"success","result":result};
              res.status(200)
              .send(return_response);
      }
    });
  },
  getSingleStripe:async function(req,res,next)
  {
    StripeModel.find({  }, {}).lean(true).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        var return_response = {"error":false,success: true,errorMessage:"success","result":result};
              res.status(200)
              .send(return_response);
      }
    });
  },
  editStripeSubmit:async function(req,res,next)
  {
    try{
      StripeModel.findOneAndUpdate({ _id:req.body.edit_id  },{
          stripe_key:req.body.stripe_key,
          stripe_secret:req.body.stripe_secret,
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },
  getSinglePaypal:async function(req,res,next)
  {
    PaypalModel.find({  }, {}).lean(true).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        if(result.length > 0)
        {
          var return_response = {"error":false,success: true,errorMessage:"success","record":result};
          res.status(200)
          .send(return_response);
        }else{
          var return_response = {"error":true,success: true,errorMessage:"success","record":result};
          res.status(200)
          .send(return_response);
        }
       
      }
    });
  },
  editPaypalSubmit:async function(req,res,next)
  {
    try{
      PaypalModel.findOneAndUpdate({ _id:req.body.edit_id  },{
          client_id:req.body.client_id,
          client_secret:req.body.client_secret,
          mode:req.body.mode,
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },
  editAboutUsPageSubmit:async function(req,res,next)
  {
    try{
      AboutUsModel.findOneAndUpdate({ _id:req.body.edit_id  },{
          description:req.body.html
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },

  getSingleTermsConditionPage:async function(req,res,next)
  {
    TermsAndConditionModel.find({  }, {}).lean(true).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        var return_response = {"error":false,success: true,errorMessage:"success","result":result};
              res.status(200)
              .send(return_response);
      }
    });
  },
  editTermsConditionPageSubmit:async function(req,res,next)
  {
    try{
      TermsAndConditionModel.findOneAndUpdate({ _id:req.body.edit_id  },{
          description:req.body.html
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },
  getSinglePrivacyPolicyPage:async function(req,res,next)
  {
    PrivacyAndPolicyModel.find({  }, {}).lean(true).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        var return_response = {"error":false,success: true,errorMessage:"success","result":result};
              res.status(200)
              .send(return_response);
      }
    });
  },
  editPrivacyPolicyPageSubmit:async function(req,res,next)
  {
    try{
      PrivacyAndPolicyModel.findOneAndUpdate({ _id:req.body.edit_id  },{
          description:req.body.html
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },

  settingSubmit:async function(req,res,next)
  {
    try{
      console.log(req.body);
      var {additional_price,access_pur_tax_percent } = req.body;
      var taxResult = await SettingModel.findOne({attribute_key:"additional_price"},{_id:1});
      // await SettingModel.findOneAndUpdate({ attribute_key:"access_pur_tax_percent"  },{
      //   attribute_value:access_pur_tax_percent
      // });
      if(taxResult)
      {
        await SettingModel.findOneAndUpdate({ _id:taxResult._id  },{
          attribute_value:additional_price
        }).then((result)=>{
          var return_response = {"error":false,success: true,errorMessage:"Succès"};
          res.status(200)
          .send(return_response);
        }).catch((error)=>{
          var return_response = {"error":true,success: false,errorMessage:error.message};
          res.status(200)
          .send(return_response);
        });
      }else{
        await SettingModel.create({
          attribute_key:"additional_price",
          attribute_value:additional_price
        }).then((result)=>{
          var return_response = {"error":false,success: true,errorMessage:"Succès"};
          res.status(200)
          .send(return_response);
        }).catch((error)=>{
          var return_response = {"error":true,success: false,errorMessage:error.message};
          res.status(200)
          .send(return_response);
        });
      }
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },
  getSetting:async function(req,res,next)
  {
    SettingModel.find({  }, {}).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        var return_response = {"error":false,success: true,errorMessage:"success","result":result};
              res.status(200)
              .send(return_response);
      }
    });
  },
  addDonationTitleSubmit:async(req,res)=>{
    try{
      let {id,title} = req.body;
      if(!id)
      {
        await DonationTitleModel.create({title}).then((result)=>{
          var return_response = {"error":false,success: true,errorMessage:"Titre ajouté succès complet"};
          return res.status(200).send(return_response);
        }).catch((error)=>{
          var return_response = {"error":true,success: false,errorMessage:error.message};
          return res.status(200).send(return_response);
        });
      }else{
        await DonationTitleModel.updateOne({_id:id},{title}).then((result)=>{
          var return_response = {"error":false,success: true,errorMessage:"Titre mis à jour succès pleinement"};
          return res.status(200).send(return_response);
        }).catch((error)=>{
          var return_response = {"error":true,success: false,errorMessage:error.message};
          return res.status(200).send(return_response);
        });
      }
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
      return res.status(200).send(return_response);
    }
  },
  allDonationTitle:async(req,res)=>{
    try{
      await DonationTitleModel.find({}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Titre mis à jour succès pleinement",record:result};
        return res.status(200).send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message,record:result};
        return res.status(200).send(return_response);
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
      return res.status(200).send(return_response);
    }
  },
  deleteDonationTitle:async(req,res)=>{
    try{
      var id = req.body.id;
      if(!id)
      {
        var return_response = {"error":true,success: false,errorMessage:"Une pièce d'identité est requise" };
        return res.status(200).send(return_response);
      }
      await DonationTitleModel.deleteOne({_id:id}).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Enregistrer les succès supprimés dans leur intégralité" };
        return res.status(200).send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:error.message };
        return res.status(200).send(return_response);
      });
    }catch(error)
    {
      var return_response = {"error":true,success: false,errorMessage:error.message};
      return res.status(200).send(return_response);
    }
  },
  donationContactUs: async function(req, res,next)
  {
    console.log(req.body);
    //res.send("herfsdfe");
    try {
      let {name,email,countryCode,phone,subject,message} = req.body;
      ProjectcontactsModel.create({
        name:req.body.name,
        email:req.body.email,
        countryCode:req.body.countryCode,
        phone:req.body.phone,
        subject:req.body.subject,
        message:req.body.message
      },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err.message};
            res.status(200)
            .send(return_response);
        }else{
          //console.log(result);
          //console.log(result._id);
          var html = "";
          //html += "Bonjour , Mehdi Jaki";
          //html += "<p>Un nouvel utilisateur tente de vous connecter dont les coordonnées sont les suivantes</p>";
          html += "<p>Nom : "+name+"</p>";
          html += "<p>Email : "+email+"</p>";
          html += "<p>Téléphone : "+countryCode+" "+phone+"</p>";
          html += "<p>Sujet : "+subject+"</p>";
          html += "<p>Message : "+message+"</p>";
          const messageee = {
              from: "noreply@coeurorphelins.org Coeur d'Orphelins", // Sender address
              to: "mehdi.jaki@yahoo.com",         // recipients
              //to: "johnwillims789@mailinator.com",         // recipients
              subject: "Projet humanitaire", // Subject line
              html: html
          };
          transport.sendMail(messageee, function(err, info) {
              if (err) {
                console.log(err);
                //error_have = 1;
                var return_response = {"error":false,success: true,errorMessage:"Nous vous contacterons très rapidement"};
                res.status(200)
                .send(return_response);
              } else {
                console.log('mail has sent.');
                var return_response = {"error":false,success: true,errorMessage:"Nous vous contacterons très rapidement"};
                res.status(200)
                .send(return_response);
              }
          });

          
        }
      });
    }catch (err) {
      console.log(err);
    }
  }
};
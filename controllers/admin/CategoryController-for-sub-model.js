const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const SubModelsModel = require("../../models/SubModelsModel");
const AdminModel = require('../../models/AdminModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();

/*/*/
module.exports = {
	addModelsSubmit: async function(req, res,next)
  {
  	//console.log("all data");
  	//console.log(req.body);
    //console.log(req.body.all_text_data);
  	try{
  		var { parent_id,model_name  } = req.body;
      //var fileImage = req.file.modelFile;
      //console.log(req.file.filename);
      if(req.file)
      {
        var fileImage = req.file.filename;
      }else{
        var fileImage = null;
      }
      //console.log("model_name"+model_name);
      console.log("parent_id"+parent_id);
      if(parent_id == "" || parent_id == "null")
      {
        console.log("here if condition");
        parent_id = null;
      }
      ModelsModel.count({model_name},(error,CategoryCount)=>{
        if(error)
        {
          console.log(error);
        }else{
          if(CategoryCount == 0)
          {
              ModelsModel.create({
                parent_id,
                fileImage,
                model_name
              });
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    errorMessage: "Modèles ajoutés avec succès"
                });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Modèles déjà existants"
              });
          }
        }
      }) 
      
  		
  	}catch(err){
  		console.log(err);
  	}
  },
  getModel: async function(req, res,next)
  {
    //console.log("all data");
    //console.log(req.body);
    try{
      allModels = await ModelsModel.find({ parent_id: null });
      if(allModels){
        res.status(200)
            .send({
            error: false,
            success: true,
            errorMessage: "Succès",
            result: allModels
        });
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
            .send({
            error: true,
            success: false,
            errorMessage: "Pas d'enregistrement"
        });
      }
    }catch(err){
      console.log(err);
    }
  },
  allModelList: async function(req, res,next)
  {
    //console.log("all data");
    //console.log(req.body);
    var total = 0;
    var perPageRecord = 4;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      ModelsModel.find({parent_id:null }, {}).skip(fromindex).limit(perPageRecord).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("totalPageNumber"+totalPageNumber);
          //console.log(result);
          if(result.length >0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
          }else{
            var return_response = {"error":true,errorMessage: "Pas d'enregistrement","record":result};
          }
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  modelTotalCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 4;
    //console.log("all data");
    //console.log(req.body);
    try{
      ModelsModel.count({ parent_id:null },(err,totalcount)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          total = totalcount;
          totalPageNumber = total / perPageRecord;
          //console.log("totalPageNumber"+totalPageNumber);
          totalPageNumber = Math.ceil(totalPageNumber);
          //console.log("totalPageNumber"+totalPageNumber);
          //console.log("totalcount new fun"+totalcount);
          console.log("all record count "+totalcount);
          var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  allSubModel: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 4;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      SubModelsModel.find({  }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          console.log("result"+result);
          var xx = result.length - 1;
          for(let i=0; i<result.length; i++)
          {
            result[i].newKey = "value here";
            console.log("in side loop "+result[i]);
            console.log("in side loop "+result[i].model_name);
            if(xx == i)
            {
              var return_response = {"error":false,errorMessage:"success","record":result};
              res.status(200).send(return_response);
            }
          }
          /*if(result.length >0)
          {
            var return_response = {"error":false,errorMessage:"success","record":result};
          }else{
            var return_response = {"error":true,errorMessage: "No Record","record":result};
          }
          res.status(200).send(return_response);*/
         // var return_response = {"error":false,errorMessage:"success","record":result};
            //  res.status(200).send(return_response);
        }
      });

    }catch(err){
      console.log(err);
    }
  },
};
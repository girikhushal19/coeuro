const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const SubCategoryModel = require("../../models/SubCategoryModel");
const ProjectModel = require("../../models/ProjectModel");
const GalleryModel = require("../../models/GalleryModel");
const ForumModel = require("../../models/ForumModel");
const CommentsModel = require("../../models/CommentsModel");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
const json2csv = require('json2csv').parse;
const { Parser } = require('json2csv');
/*/*/
module.exports = {
  allForumCountDashboard:async function(req,res,next)
  {
    try{
      ForumModel.countDocuments({}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          ForumModel.countDocuments({status:1}).exec((err2, result2)=>
          {
            if(err2)
            {
              console.log(err2);
            }else{
              ForumModel.countDocuments({status:2}).exec((err3, result3)=>
              {
                if(err3)
                {
                  console.log(err3);
                }else{
                  var record = {"allForum":result,"allActiveForum":result2,"allInactiveForum":result3};
                  var return_response = {"error":false,errorMessage:"Succès","record":record};
                  res.status(200).send(return_response);
                }
              });  
            }
          });  
        }
      });  
    }catch(err){
      //console.log(err);
      var return_response = {"error":true,success: false,errorMessage:err};
        res.status(200).send(return_response);
    }
  },
  allLikeDislikeCount:async function(req,res,next)
  {
    try{
      var likeCount = disLikeCount = 0;
      ForumModel.aggregate([
        { $group: { _id: null, likeCount: { $sum: "$like" } } }
      ]).exec((err, result)=>
      {
        if(result.length > 0)
        {
          //console.log(result[0]);
          likeCount = result[0].likeCount;
        }
        if(err)
        {
          //console.log(err);
        }else{
          ForumModel.aggregate([
            { $group: { _id: null, dislikeCount: { $sum: "$dislike" } } }
          ]).exec((err2, result2)=>
          {
            if(err2)
            {
                //console.log(err2);
            }else{
              if(result2)
              {
                disLikeCount = result2[0].dislikeCount;
                //console.log(result2);
                var record = { "likeCount":likeCount,"dislikeCount":disLikeCount };
                var return_response = {"error":false,success: true,errorMessage:"Succès",record:record};
                res.status(200).send(return_response);
              }else{
                var record = { "likeCount":likeCount,"dislikeCount":disLikeCount };
                var return_response = {"error":false,success: true,errorMessage:"Succès",record:record};
                res.status(200).send(return_response);
              }
            }
            

          });  
        }
      });  
    }catch(err){
      //console.log(err);
      var return_response = {"error":true,success: false,errorMessage:err};
        res.status(200).send(return_response);
    }
  },
  allForumAdmin:async function(req,res,next)
  {
    //mongoose.set('debug', true);
    var { firstName,lastName,mobileNumber,email,title,status,description } = req.body;
    var String_qr = {}; 
    //String_qr['firstName'] = firstName;
    if(firstName && firstName != "")
    {
      //{ "abc": { $regex: '.*' + colName + '.*' } }
      String_qr['firstName'] = { $regex: '.*' + firstName + '.*' };
    }
    if(lastName && lastName != "")
    {
      //{ "abc": { $regex: '.*' + colName + '.*' } }
      String_qr['lastName'] = { $regex: '.*' + lastName + '.*' };
    }
    if(email && email != "")
    {
      //{ "abc": { $regex: '.*' + colName + '.*' } }
      String_qr['email'] = { $regex: '.*' + email + '.*' };
    }
    if(mobileNumber && mobileNumber != "")
    {
      //{ "abc": { $regex: '.*' + colName + '.*' } }
      String_qr['mobileNumber'] = { $regex: '.*' + mobileNumber + '.*' };
    }
    if(title && title != "")
    {
      //{ "abc": { $regex: '.*' + colName + '.*' } }
      //String_qr['title'] = { $regex: '.*' + title + '.*' };
      String_qr['title'] = { $regex: '.*' + title + '.*' };
    }
    if(description && description != "")
    {
      //{ "abc": { $regex: '.*' + colName + '.*' } }
      String_qr['description'] = { $regex: '.*' + description + '.*' };
    }
    if(status && status != "")
    {
      //{ "abc": { $regex: '.*' + colName + '.*' } }
      String_qr['status'] = status;
    }
    //console.log(String_qr);
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      const queryJoin = [
          {
            path:'user_id',
            select:['firstName','lastName','userImage','email','mobileNumber']
          }
        ]; 
      ForumModel.find(String_qr, {}).populate(queryJoin).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Tous les enregistrements du forum","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  allRecentForumDashboard:async function(req,res,next)
  {
    //mongoose.set('debug', true);
    var String_qr = {}; 
    String_qr['status'] = 1;
    //console.log(String_qr);
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    // if(req.body.numofpage == 0)
    // {
    //   fromindex = 0;
    // }else{
    //   fromindex = perPageRecord * req.body.numofpage
    // }
    try{
      const queryJoin = [
          {
            path:'user_id',
            select:['firstName','lastName','userImage','email','mobileNumber']
          }
        ]; 
      ForumModel.find(String_qr, {}).populate(queryJoin).skip(fromindex).limit(perPageRecord).sort([['created_at', -1]]).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Tous les enregistrements du forum","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(err){
      console.log(err);
    }
  },
  allForumCount:async function(req,res,next)
  {
      let total = 0;
      var totalPageNumber = 1;
      var perPageRecord = 10;
      //console.log("here");
      //console.log(req.body);
      try{
         ForumModel.countDocuments({  }).exec((err, totalcount)=>
          {
            //console.log("countResult "+totalcount);
            if(err)
            {
              console.log(err);
            }else{
              total = totalcount;
              totalPageNumber = total / perPageRecord;
              //console.log("totalPageNumber"+totalPageNumber);
              totalPageNumber = Math.ceil(totalPageNumber);
              //console.log("totalPageNumber"+totalPageNumber);
              //console.log("totalcount new fun"+totalcount);
              //console.log("all record count "+totalcount);
              var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
              res.status(200).send(return_response);
            }
          });
         
      }catch(err){
        console.log(err);
      }
  },
  updateForumApi:async function(req,res,next)
  {
    try{
      ForumModel.findOneAndUpdate({ _id:req.body.id  },{
          status:req.body.status
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },

  exportCsvForumApi:async function(req,res,next)
  {
    const queryJoin = [
          {
            path:'user_id',
            select:['firstName','lastName','email','mobileNumber']
          }
        ]; 
      ForumModel.find({}, {}).populate(queryJoin).lean(true).exec((err, data)=>
      {
      if(err)
      {
        console.log(err);
      }else{
        //console.log("result ");
        //console.log(typeof result);
        //console.log("result"+result);
        if(data.length > 0)
        {
          
          data.forEach(object=>
          {
            //console.log(object.user_id[0].firstName);
            object.firstName = object.user_id[0].firstName;
            object.lastName = object.user_id[0].lastName;
            object.email = object.user_id[0].email;
            object.mobileNumber = object.user_id[0].mobileNumber;
            if(object.status==1)
            {
              object.status="Actif";
            }
            else
            {
              object.status="Inactif";
            }
          })
          //console.log(data);
          //console.log(JSON.stringify(data));
            
           //var data = [{id:1,name:"John",lname:"USA",email:"USA"},{id:1,name:"Ronny",lname:"Germany",email:"Germany"}];
           var fields = ['id', 'firstName','lastName', 'email','mobileNumber','title','description','status'];


            //const fields = ['field1', 'field2', 'field3'];
            const opts = { fields };
            const parser = new Parser(opts);
            const csv = parser.parse(data);
            //var path=  Date.now()+'.csv'; 
            var path=  'forumData.csv'; 
                   fs.writeFile(path, csv, function(err,data) {
                    if (err) {throw err;}
                    else{ 
                      res.download(path); // This is what you need
                    }
                });
        }else{
          var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
          res.status(200).send(return_response);
        }

      }
    });
  },
  detailForum:async function(req,res,next)
  {
    try{
      var userImagePath = customConstant.base_url+"public/uploads/userProfile/";
      var userDefaultImagePath = customConstant.base_url+"public/uploads/userProfile/user-icon.jpeg";
      var fromindex = 0;
      var like_or_dislike = 0;
      var user_id = req.body.user_id;
      var allLikeCounterForum = 0;
      var allDislikeLikeCounterForum = 0;
      var id = req.body.id;

      const queryJoin = [
          {
            path:'user_id',
            select:['firstName','lastName','userImage']
          }
        ];
      ForumModel.find({ _id:id },{}).populate(queryJoin).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          /*console.log("result ");
          console.log(typeof result);*/
          /*console.log("result"+JSON.stringify(result));
          console.log("result"+result[0].user_id[0]._id);*/
          if(result.length > 0)
          {
            CommentsModel.find({ forum_id:id }).populate(queryJoin).sort([['created_at', -1]]).lean(true).exec((err4, commentCount)=>
            {
              //console.log("countResult "+totalcount);
              if(err4)
              {
                console.log(err4);
              }else{
                var return_response = {"error":false,errorMessage:"Pas de dossier","record":result,"comment":commentCount};
                res.status(200).send(return_response);
              }
            });
          }else{
            var return_response = {"error":false,errorMessage:"Pas de dossier","record":result};
            res.status(200).send(return_response);
          }

        }
      });  
    }catch(err){
      console.log(err);
      var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200)
          .send(return_response);
    }
  },

};
const express = require('express');
const UsersModel = require("../../models/UsersModel");
const AdminModel = require('../../models/AdminModel');
const GalleryModel = require('../../models/GalleryModel');
const BannerModel = require('../../models/BannerModel');
const PushNotificationModel = require('../../models/PushNotificationModel');
const ProjectModel = require('../../models/ProjectModel');
const Paymentrecords = require('../../models/Paymentrecords');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
//var json2csv = require('json2csv').Parser; 
const json2csv = require('json2csv').parse;
const { Parser } = require('json2csv');

 

/*/*/
module.exports = {
	exportCsvUserApi:async function(req,res,next)
	{
		UsersModel.find({ }, { _id:1,firstName:1,lastName:1,email:1,mobileNumber:1,city:1,description:1,status:1 }).lean(true).exec((err, data)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        //console.log("result ");
        //console.log(typeof result);
        //console.log("result"+result);
        if(data.length > 0)
        {
          //console.log("here");
          //console.log(result);


          /*const csvFields = ['id', 'name','lname', 'email'];
			    const json2csvParser = new Json2csvParser({ csvFields });
			    const csv = json2csvParser.parse(result);
			 
			    console.log(csv);
			 
			     res.setHeader("Content-Type", "text/csv");
			     res.setHeader("Content-Disposition", "attachment; filename=users.csv");
			 
			     res.status(200).end(csv);*/

			    data.forEach(object=>
          {
            if(object.status==1)
            {
		        	object.status="Actif";
		        }
		        else
		        {
		         	object.status="Inactif";
		        }
          })
          //console.log(JSON.stringify(data));
            


			     //var data = [{id:1,name:"John",lname:"USA",email:"USA"},{id:1,name:"Ronny",lname:"Germany",email:"Germany"}];
			     var fields = ['id', 'firstName','lastName', 'email','mobileNumber','city','description','status'];


			     	//const fields = ['field1', 'field2', 'field3'];
						const opts = { fields };
			     	const parser = new Parser(opts);
  					const csv = parser.parse(data);
  					//var path=  Date.now()+'.csv'; 
  					var path=  'userData.csv'; 
                   fs.writeFile(path, csv, function(err,data) {
                    if (err) {throw err;}
                    else{ 
                      res.download(path); // This is what you need
                    }
                });

                  /*var csv = json2csv({ data: data, fields: fields });
                  var path=  Date.now()+'.csv'; 
                   fs.writeFile(path, csv, function(err,data) {
                    if (err) {throw err;}
                    else{ 
                      res.download(path); // This is what you need
                    }
                }); */



			      




        }else{
          var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
          res.status(200).send(return_response);
        }

      }
    });
	},
	allUsers:async function(req,res,next)
	{
		var { firstName,lastName,mobileNumber,email,city,status } = req.body;
  	/*console.log(firstName);
  	console.log(lastName);
  	console.log(email);*/

	  	var String_qr = {}; 
	    //String_qr['firstName'] = firstName;
	    if(firstName && firstName != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['firstName'] = { $regex: '.*' + firstName + '.*' };
	    }
	    if(lastName && lastName != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['lastName'] = { $regex: '.*' + lastName + '.*' };
	    }
	    if(email && email != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['email'] = { $regex: '.*' + email + '.*' };
	    }
	    if(mobileNumber && mobileNumber != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['mobileNumber'] = { $regex: '.*' + mobileNumber + '.*' };
	    }
	    if(city && city != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['city'] = { $regex: '.*' + city + '.*' };
	    }
	    if(status && status != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['status'] = status;
	    }
			
			String_qr['deleted_at'] = 0;
	  	//console.log(String_qr);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);

	    	
	      UsersModel.find( String_qr, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
	    }
	},
	getAllActiveUserAdminApi:async function(req,res,next)
	{
	    try{
	    	//mongoose.set('debug', true);String_qr['deleted_at'] = 0;
	      UsersModel.find( {status:1,deviceToken: { $ne: null },deleted_at:0}, {firstName:1,lastName:1}).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	          	/*var abc = [];
	          	//console.log(result);
	          	for (var _i = 0; _i < result.length; _i++)
			        {
			          
			          abc.push({item_id: result[_i]._id,item_text: result[_i].firstName+" "+result[_i].lastName}); 

			          console.log(abc);
			          //this.dropdownListNew[_i] = { item_id: this.record[_i]._id, item_text: this.record[_i].firstName+" "+this.record[_i].lastName };
			        }*/
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
	    }
	},
	allUsersCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");String_qr['deleted_at'] = 0;
	    //console.log(req.body);
	    try{
	       UsersModel.countDocuments({ deleted_at:0 }).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	getSingleUserApi:async function(req,res,next)
	{
	    //console.log("here");
	    //console.log(req.body);
	    try{
	       UsersModel.findOne({ _id:req.body.id }).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            var return_response = {"error":false,errorMessage:"Succès","record":totalcount};
	            res.status(200).send(return_response);
	          }
	        });
	    }catch(err){
	      console.log(err);
	    }
	},
	updateUserStatusApi: async function(req, res,next)
	{
		//console.log(req.body.status);
	    UsersModel.updateOne({ "_id":req.body.id }, 
	    {status:req.body.status}, function (uerr, docs) {
	    if (uerr){
	      //console.log(uerr);
	      res.status(200)
	        .send({
	            error: true,
	            success: false,
	            errorMessage: uerr
	        });
	    }
	    else{
	      res.status(200)
	        .send({
	            error: false,
	            success: true,
	            errorMessage: "Succès complet de la mise à jour du statut"
	        });
	      }
	    });
	},

	editUserSubmit: async function(req,res,next)
	{
		//console.log("hereeee");
	    //console.log(req.body);
	    //console.log(req.body);
      	//console.log(req.files);
      	//console.log(req.file.filename);

	    var firstName = req.body.firstName;
	    var lastName = req.body.lastName;
	    var email = req.body.email;
	    var mobileNumber = req.body.mobileNumber;
	    var city = req.body.city;
	    var description = req.body.description;
	    var id = req.body.id;
	    UsersModel.count({ email:email , '_id': {$ne: id} },(errs,emailCount)=>
        {
          if(errs)
          {
            console.log(errs);
          }else{
          	//console.log(emailCount);
          	if(emailCount == 1)
          	{
          		res.status(200)
			        .send({
			            error: true,
			            success: false,
			            errorMessage: "Cet email est prêt à être utilisé par un autre utilisateur"
			        });
          	}else{
      		UsersModel.count({ mobileNumber:mobileNumber , '_id': {$ne: id} },(errs,emailCount)=>
		        {
		          if(errs)
		          {
		            console.log(errs);
		          }else{
		          	//console.log(emailCount);
		          	if(emailCount == 1)
		          	{
		          		res.status(200)
					        .send({
					            error: true,
					            success: false,
					            errorMessage: "Ce numéro de mobile est prêt à être utilisé par un autre utilisateur"
					        });
		          	}else{
		          		//console.log("hereeeee");	
		          		UsersModel.findOne({ "_id":id }).exec((err, userRecord)=>{
		          			if(err)
					        {
					          //console.log("herrrrrerr");
					          console.log(err);
					        }else{
					        	//console.log(userRecord);
					        	if(userRecord)
				          		{
				          		if(req.files.length > 0)
							        {
							          /*console.log("iffff");
							          console.log(req.files);return false;*/
							          var userImage = req.files[0].filename;
							          //console.log(req.files[0].filename);
							          var oldFileName = userRecord.userImage;
							          var uploadDir = 'public/uploads/userProfile/';
							          let fileNameWithPath = uploadDir + oldFileName;
							          //console.log(fileNameWithPath);

							          if (fs.existsSync(fileNameWithPath))
							          {
							            fs.unlink(fileNameWithPath, (err) => 
							            {
							              console.log("unlink file error "+err);
							            });
							          }
							        }else{
							          //console.log("elseee");
							          if(userRecord.userImage)
							          {
							            var userImage = userRecord.userImage;
							          }else{
							            var userImage = null;
							          }
							        }
							        /*console.log("userImage");
							        console.log(userImage);
							        return false;*/
				          			UsersModel.updateOne({ "_id":id }, 
								    {
								    	firstName:firstName,
								    	lastName:lastName,
								    	email:email,
								    	mobileNumber:mobileNumber,
								    	city:city,
								    	description:description,
								    	userImage:userImage,
								    }, function (uerr, docs) {
								    if (uerr){
								      //console.log(uerr);
								      res.status(200)
								        .send({
								            error: true,
								            success: false,
								            errorMessage: uerr
								        });
								    }
								    else{
								      res.status(200)
								        .send({
								            error: false,
								            success: true,
								            errorMessage: "Succès complet de la mise à jour du statut"
								        });
								      }
								   });
				          		}else{
				          			res.status(200)
								        .send({
								            error: true,
								            success: false,
								            errorMessage: "Identifiant d'utilisateur invalide"
								     });
				          		}
					        }
		          		});
		          	}
		          }
		        });
          	}
          }
        });
	},
	
	allUsersDonation:async function(req,res,next)
	{
		//mongoose.set('debug', true);
		var { firstName,lastName,mobileNumber,email,category,price } = req.body;
  	/*console.log(firstName);
  	console.log(lastName);
  	console.log(email);*/

	  	var String_qr = {}; 
	    //String_qr['firstName'] = firstName;
	    if(firstName && firstName != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['firstName'] = { $regex: '.*' + firstName + '.*' };
	    }
	    if(lastName && lastName != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['lastName'] = { $regex: '.*' + lastName + '.*' };
	    }
	    if(email && email != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['email'] = { $regex: '.*' + email + '.*' };
	    }
	    if(mobileNumber && mobileNumber != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['mobileNumber'] = { $regex: '.*' + mobileNumber + '.*' };
	    }
	    if(category && category != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['category_title'] = { $regex: '.*' + category + '.*' };
	    }
	    if(price && price != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['price'] = { $regex: '.*' + price + '.*' };
	    }
	    String_qr['status'] = 1;


    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      Paymentrecords.find(String_qr, {}).skip(fromindex).limit(perPageRecord).sort({created_at:-1}).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
	},
	getDonationDetail:async function(req,res,next)
	{
		try{
			await Paymentrecords.aggregate([
				{
					$match:{
						_id:mongoose.Types.ObjectId(req.body.id)
					}
				},
				{
					$addFields:{
						"pro_id":"$project_id"
					}
				},
				{
					$lookup:{
						from:"projects",
						let:{"project_id":{"$toObjectId":"$pro_id"}},
						pipeline:[
							{
								$match:{
									$expr:{
										$eq:["$_id","$$project_id"]
									}
								}
							}
						],
						as:"project_record"
					}
				}
			]).then((result)=>{
				//console.log("result ", result);
				if(result.length > 0)
				{
						var return_response = {"error":false,errorMessage:"Succès","record":result};
								res.status(200).send(return_response);
				}else{
					var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
					res.status(200).send(return_response);
				}
			}).catch((error)=>{
				var return_response = {"error":true,errorMessage:error.message };
					res.status(200).send(return_response);
			});

			/*
			Paymentrecords.find({_id:req.body.id}, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });*/
		}catch(error){
			var return_response = {"error":true,errorMessage:error};
      res.status(200).send(return_response);
		}
	},
	allRecentDonationDashboard:async function(req,res,next)
	{
		//mongoose.set('debug', true);
		 
  	/*console.log(firstName);
  	console.log(lastName);
  	console.log(email);*/

	  var String_qr = {}; 
	  String_qr['status'] = 1;

    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    // if(req.body.numofpage == 0)
    // {
    //   fromindex = 0;
    // }else{
    //   fromindex = perPageRecord * req.body.numofpage
    // }
    try{
      Paymentrecords.find(String_qr, {firstName:1,lastName:1,email:1,price:1,status:1}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
	},
	allUsersDonationCount:async function(req,res,next)
	{
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
       Paymentrecords.countDocuments({ status:1 }).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
	},
	allDonationCountDashboard:async function(req,res,next)
	{
    try{
       Paymentrecords.countDocuments({ status:1 }).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            var return_response = {"error":false,errorMessage:"Succès","record":totalcount};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
	},
	allUsersCountDashboard:async function(req,res,next)
	{
	  try{
    	UsersModel.countDocuments({  }).exec((err, allUserCount)=>
      {
        //console.log("countResult "+totalcount);
        if(err)
        {
          console.log(err);
        }else{
		    	UsersModel.countDocuments({ status:1 }).exec((err1, allActiveUserCount)=>
		      {
		        //console.log("countResult "+totalcount);
		        if(err1)
		        {
		          console.log(err1);
		        }else{
				    	UsersModel.countDocuments({ status:2 }).exec((err2, allInActiveUserCount)=>
				      {
				        //console.log("countResult "+totalcount);
				        if(err2)
				        {
				          console.log(err2);
				        }else{
				        	var record = { "allUserCount":allUserCount,"allActiveUserCount":allActiveUserCount,"allInActiveUserCount":allInActiveUserCount };
				        	var return_response = {"error":false,errorMessage:"Succès","record":record};
            			res.status(200).send(return_response);
				        }
				      });
		        }
		      });
        }
      });
	  }catch(err){
	    console.log(err);
	  }
	},
	exportCsvUsersDonationApi:async function(req,res,next)
	{
		Paymentrecords.find({ status:1 }, { _id:1,firstName:1,lastName:1,email:1,mobileNumber:1,category_title:1,subcategory_title:1,price:1,description:1,status:1 }).lean(true).exec((err, data)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        //console.log("result ");
        //console.log(typeof result);
        //console.log("result"+result);
        if(data.length > 0)
        {
          //console.log("here");
          //console.log(result);
			    data.forEach(object=>
          {
            if(object.status==1)
            {
		        	object.status="Complète";
		        }
		        else
		        {
		         	object.status="Incomplet";
		        }
          })
          //console.log(JSON.stringify(data));
            


			     //var data = [{id:1,name:"John",lname:"USA",email:"USA"},{id:1,name:"Ronny",lname:"Germany",email:"Germany"}];
			     var fields = ['id', 'firstName','lastName', 'email','mobileNumber','category_title','subcategory_title','price','description','status'];
			     	//const fields = ['field1', 'field2', 'field3'];
						const opts = { fields };
			     	const parser = new Parser(opts);
  					const csv = parser.parse(data);
  					//var path=  Date.now()+'.csv'; 
  					var path=  'UsersDonation.csv'; 
                 fs.writeFile(path, csv, function(err,data) {
                  if (err) {throw err;}
                  else{ 
                    res.download(path); // This is what you need
                  }
              });
        }else{
          var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
          res.status(200).send(return_response);
        }

      }
    });
	},
};
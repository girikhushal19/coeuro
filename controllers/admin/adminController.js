const express = require('express');
const UsersModel = require("../../models/UsersModel");
const AdminModel = require('../../models/AdminModel');
const GalleryModel = require('../../models/GalleryModel');
const NewsLaterModel = require('../../models/NewsLaterModel');
const BannerModel = require('../../models/BannerModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();

/*/*/
module.exports = {
  adminLogin: async function(req, res,next)
  {
    // Get user input
    const { email, password } = req.body;
    var encryptedPassword = await bcrypt.hash(password, 10);
    /*console.log(encryptedPassword);
    return false;*/
    // Validate user input
    // Validate if user exist in our database
    var user = "";
    user = await AdminModel.findOne({ email });
    var date_time = new Date();
    if (user && (await bcrypt.compare(password, user.password)))
    {
      //console.log("user record"+user);
      // Create token
      const token = jwt.sign(
        { user_id: user._id, email },
        "Coeurophelins",
        /*process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }*/
      );
      // save user token
      user.token = token;
      AdminModel.updateOne({ "email":email }, 
      {token:token,loggedin_time:date_time}, function (uerr, docs) {
        if (uerr){
            console.log(uerr);
            res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: uerr,
                      //userRecord:user
                  });
        }else if(user.status == 0)
        {
          res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: "Le compte est désactivé",
          });
        }else{
          //console.log("User controller login method : ", docs);
          res.status(200)
              .send({
                error: false,
                success: true,
                errorMessage: "Succès de connexion complet",
                userRecord:user
          });
        }
      });
      // user
      //res.status(200).json(user);
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: "Connexion invalide"
                });
      }
  },
  getAdminProfile: async function(req, res,next)
  {
    try{
      AdminModel.findOne({  }).exec((err, userRecord)=>{
        if(err)
        {
          //console.log("herrrrrerr");
          console.log(err);
        }else{
          //console.log(userRecord);
            var return_response = {"error":false,errorMessage:"success","record":userRecord};
            res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  editAdminProfileSubmitApi: async function(req,res,next)
  {
      //console.log("hereeee");
      //console.log(req.body);
      //console.log(req.body);
      //console.log(req.files);
      //console.log(req.file.filename);

      var firstName = req.body.firstName;
      var lastName = req.body.lastName;
      var email = req.body.email;
      
      var id = req.body.id;
      
      AdminModel.findOne({ "_id":id }).exec((err, userRecord)=>{
        if(err)
      {
        //console.log("herrrrrerr");
        console.log(err);
      }else{
        //console.log(userRecord);
        if(userRecord)
          {
            if(req.files)
            {
              //console.log("iffff");
              var userImage = req.files[0].filename;
              //console.log(req.files[0].filename);
              var oldFileName = userRecord.userImage;
              var uploadDir = 'public/uploads/adminProfile/';
              let fileNameWithPath = uploadDir + oldFileName;
              //console.log(fileNameWithPath);

              if (fs.existsSync(fileNameWithPath))
              {
                fs.unlink(fileNameWithPath, (err) => 
                {
                  console.log("unlink file error "+err);
                });
              }
            }else{
              //console.log("elseee");
              if(userRecord.userImage)
              {
                var userImage = userRecord.userImage;
              }else{
                var userImage = null;
              }
            }
            /*console.log("userImage");
            console.log(userImage);
            return false;*/
            AdminModel.updateOne({ "_id":id }, 
            {
              firstName:firstName,
              lastName:lastName,
              email:email,
              adminImage:userImage,
            }, function (uerr, docs) {
            if (uerr){
              //console.log(uerr);
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: uerr
                });
            }
            else{
              res.status(200)
                .send({
                    error: false,
                    success: true,
                    errorMessage: "Succès complet de la mise à jour du statut"
                });
              }
           });
          }else{
            res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: "Identifiant d'utilisateur invalide"
         });
        }
      }
    });    
  },
  adminRegistration: async function(req,res,next)
  {
    //console.log("controller registration method");
    //console.log(req.body);
    //res.send("herfsdfe");
    var encryptedPassword = "";
       try {
        const { userName, email, password , firstName, lastName } = req.body;
        // check if user already exist
        // Validate if user exist in our database
        encryptedPassword = await bcrypt.hash(password, 10);

        //console.log("xx"+xx);
      //console.log("encryptedPassword khushal "+encryptedPassword);

        var token =  jwt.sign(
          { user_id:  Math.random().toString(36).slice(2), email },
          "Porschists",
          //{
          //  expiresIn: "2h",
          //}
        );

        AdminModel.count({ email },(errs,emailCount)=>
        {
          if(errs)
          {
            console.log(errs);
          }else{
            //console.log(emailCount);
            if(emailCount === 0)
            {
              AdminModel.count({ userName },(userNameerrs,userNameCount)=>
              {
                if(userNameerrs)
                {
                  console.log(errs);
                }else{
                  if(userNameCount === 0)
                  {
                    
                    //Encrypt user password
                    //encryptedPassword = bcrypt.hash(password, 10);
                    //console.log(encryptedPassword);
                    //return false;
                    // Create token
                    // save user token
                    //user.token = token;
                    //encryptedPassword = "$2a$10$2XpyurefqmxN40GU0RVXQ.dVhX7DSxxiXw4PvH/w4IM6apJ6zlv4.";
                    //console.log("encryptedPassword"+encryptedPassword);
                    // Create user in our database
                    //console.log("herrrrrrrrrrrrrr");
                    const user =  AdminModel.create({
                      lastName,
                      firstName,
                      userName,
                      email: email.toLowerCase(), // sanitize: convert email to lowercase
                      password: encryptedPassword,
                      token: token,
                    });
                    // return new user
                    res.status(200)
                          .send({
                              error: false,
                              success: true,
                              errorMessage: "Succès complet de l'enregistrement"
                          });
                  }else{
                    //email exist
                    res.status(200)
                          .send({
                              error: true,
                              success: false,
                              errorMessage: "Le nom d'utilisateur existe déjà"
                          });
                  }
                }
                
              });
            }else{
              //email exist
              res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: "Email déjà existant"
                    });
            }
          }
        }); 
    }catch (err) {
        console.log(err);
    }
  },
  adminChangePasswordSubmitApi: async function(req,res,next)
  {
    //console.log(req.body);
    var old_password = req.body.old_password;
    var new_password = req.body.new_password;
    var encryptedPassword = await bcrypt.hash(new_password, 10);
    user = await AdminModel.findOne({  });
    
    if(user && (await bcrypt.compare(old_password, user.password)))
    {
      if(new_password.length < 8)
      {
        var return_response = {"error":true,success: false,errorMessage:"La longueur du nouveau mot de passe ne doit pas être inférieure à huit chiffres."};
            res.status(200)
            .send(return_response);
      }else{
        //console.log("hereee");
        AdminModel.updateOne({  }, 
        {
          password:encryptedPassword,
        }, function (uerr, docs) {
        if (uerr){
          //console.log(uerr);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: uerr
            });
        }
        else{
          res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Succès total du changement de mot de passe"
            });
          }
       });
      }
    }else{
      res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'ancien mot de passe est incorrect"
      });
    }
  },
  addGalleryImageSubmit: async function(req,res,next)
  {
    try{
      /*console.log(req.files);
      console.log(req.files[0]);
      console.log(req.files[1]);*/
      //console.log(req.files.file);
      var images = [];
      if(req.files)
      {
        if(req.files)
        {
          var p_r_i = req.files;
          for(let m=0; m<p_r_i.length; m++)
          {
            let images_name_obj = { "filename":req.files[m].filename,"path":req.files[m].path  };
             images.push(images_name_obj);
            }
            //console.log(exterior_image);
        }

        GalleryModel.create({
          images:images,
          file_type:1
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Pas d'image"};
              res.status(200)
              .send(return_response);
      }
    }catch (err) {
        console.log(err);
    }
  },
  addGalleryVideoSubmit: async function(req,res,next)
  {
    try{
      /*console.log(req.files);
      console.log(req.files[0]);
      console.log(req.files[1]);*/
      //console.log(req.files.file);
      var images = [];
      if(req.files)
      {
        if(req.files)
        {
          var p_r_i = req.files;
          for(let m=0; m<p_r_i.length; m++)
          {
            let images_name_obj = { "filename":req.files[m].filename,"path":req.files[m].path  };
             images.push(images_name_obj);
            }
            //console.log(exterior_image);
        }

        GalleryModel.create({
          images:images,
          file_type:2
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Pas de vidéo"};
              res.status(200)
              .send(return_response);
      }
    }catch (err) {
        console.log(err);
    }
  },
  addBannerImageSubmit: async function(req,res,next)
  {
    try{
      /*console.log(req.files);
      console.log(req.files[0]);
      console.log(req.files[1]);*/
      //console.log(req.files.file);
      var images = [];
      if(req.files)
      {
        if(req.files)
        {
          var p_r_i = req.files;
          for(let m=0; m<p_r_i.length; m++)
          {
            let images_name_obj = { "filename":req.files[m].filename,"path":req.files[m].path  };
             images.push(images_name_obj);
            }
            //console.log(exterior_image);
        }

        BannerModel.create({
          images:images
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
      }else{
        var return_response = {"error":true,success: false,errorMessage:"Pas d'image"};
              res.status(200)
              .send(return_response);
      }
    }catch (err) {
        console.log(err);
    }
  },
  allBannerCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
       BannerModel.countDocuments({  }).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"success","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allBanner: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      BannerModel.find({ }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  deleteBanner: async function(req, res,next){
    var categoryRecord = await BannerModel.find({ _id:req.body.id}); 
    if(categoryRecord)
    {
      //console.log(categoryRecord);
      //console.log(categoryRecord[0].images[0].filename);
      //var uploadDir = './public/uploads/userProfile/';

      let fileNameWithPath = categoryRecord[0].images[0].path;
      //console.log(fileNameWithPath);
      if (fs.existsSync(fileNameWithPath))
      {
        fs.unlink(fileNameWithPath, (err) => 
        {
          console.log("unlink file error "+err);
        });
      }
       
      BannerModel.deleteOne({
        _id:req.body.id
      },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Succès"};
            res.status(200)
            .send(return_response);
        }
      });
    }
  },
  allNewslatterCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
       NewsLaterModel.countDocuments({  }).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"success","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allNewslatter: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      NewsLaterModel.find({ }, {}).sort({created_at:-1}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  deleteNewslatter: async function(req, res,next){
    var categoryRecord = await NewsLaterModel.find({ _id:req.body.id}); 
    if(categoryRecord)
    {
      NewsLaterModel.deleteOne({
        _id:req.body.id
      },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          var return_response = {"error":false,success: true,errorMessage:"Succès"};
            res.status(200)
            .send(return_response);
        }
      });
    }else{
      var return_response = {"error":true,success: false,errorMessage:""};
            res.status(200)
            .send(return_response);
    }
  },
};
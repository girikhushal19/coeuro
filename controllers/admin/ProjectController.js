const express = require('express');
const UsersModel = require("../../models/UsersModel");
const AdminModel = require('../../models/AdminModel');
const GalleryModel = require('../../models/GalleryModel');
const BannerModel = require('../../models/BannerModel');
const PushNotificationModel = require('../../models/PushNotificationModel');
const ProjectModel = require('../../models/ProjectModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const json2csv = require('json2csv').parse;
const { Parser } = require('json2csv');
 

/*/*/
module.exports = {
	allProject:async function(req,res,next)
	{
			//mongoose.set('debug', true);
			var String_qr = {}; 
	    var { title,status,description } = req.body;
			if(title && title != "")
	    {
	      //{ "abc": { $regex: '.*' + colName + '.*' } }
	      //String_qr['title'] = { $regex: '.*' + title + '.*' };
	      String_qr['title'] = { $regex: '.*' + title + '.*' };
	    }
	    if(description && description != "")
	    {
	      //{ "abc": { $regex: '.*' + colName + '.*' } }
	      String_qr['description'] = { $regex: '.*' + description + '.*' };
	    }
	    if(status && status != "")
	    {
	      //{ "abc": { $regex: '.*' + colName + '.*' } }
	      String_qr['status'] = status;
	    }
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	      const queryJoin = [
	          {
	            path:'category',
	            select:['category']
	          }
	        ];

	      ProjectModel.find(String_qr, {}).populate(queryJoin).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
	    }
	},
	getSingleProject:async function(req,res,next)
	{
    try{
      ProjectModel.find({ _id:req.body.id }, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
	allProjectCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    try{
	       ProjectModel.countDocuments({  }).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	updateProjectStatusApi: async function(req, res,next)
	{
	    ProjectModel.updateOne({ "_id":req.body.id }, 
	    {status:req.body.status}, function (uerr, docs) {
	    if (uerr){
	      //console.log(uerr);
	      res.status(200)
	        .send({
	            error: true,
	            success: false,
	            errorMessage: uerr
	        });
	    }
	    else{
	      res.status(200)
	        .send({
	            error: false,
	            success: true,
	            errorMessage: "Succès complet de la mise à jour du statut"
	        });
	      }
	    });
	},
	allPushNotification:async function(req,res,next)
	{
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{


	      PushNotificationModel.find({  }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
	    }
	},
	allPushNotificationCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    try{
	       PushNotificationModel.countDocuments({  }).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	deletePushNotification: async function(req, res,next)
	{
	    PushNotificationModel.deleteOne({
	        _id:req.body.id
	      },function(err,result){
	        if(err)
	        {
	          var return_response = {"error":true,success: false,errorMessage:err};
	            res.status(200)
	            .send(return_response);
	        }else{
	          var return_response = {"error":false,success: true,errorMessage:"Succès"};
	            res.status(200)
	            .send(return_response);
	        }
	      });
	},
	allVideo:async function(req,res,next)
	{
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	

	      GalleryModel.find({ file_type:2 }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Plan mis à jour avec succès","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
	    }
	  },
	allVideoCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    try{
	       GalleryModel.countDocuments({ file_type:2 }).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	deleteVideo: async function(req, res,next)
	{
	    GalleryModel.deleteOne({
	        _id:req.body.id
	      },function(err,result){
	        if(err)
	        {
	          var return_response = {"error":true,success: false,errorMessage:err};
	            res.status(200)
	            .send(return_response);
	        }else{
	          var return_response = {"error":false,success: true,errorMessage:"Succès"};
	            res.status(200)
	            .send(return_response);
	        }
	      });
	},
	allGalleryImage:async function(req,res,next)
	{
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
    	

      GalleryModel.find({ file_type:1 }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
	},
	singleGalleryImage:async function(req,res,next)
	{
     
    try{
      GalleryModel.find({ _id:req.body.id }, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
	},
	allGalleryImageCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    try{
	       GalleryModel.countDocuments({ file_type:1 }).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
	    }
	},
	deleteGalleryImage: async function(req, res,next)
	{
	    GalleryModel.deleteOne({
	        _id:req.body.id
	      },function(err,result){
	        if(err)
	        {
	          var return_response = {"error":true,success: false,errorMessage:err};
	            res.status(200)
	            .send(return_response);
	        }else{
	          var return_response = {"error":false,success: true,errorMessage:"Succès"};
	            res.status(200)
	            .send(return_response);
	        }
	      });
	},
	exportCsvProjectApi: async function(req, res,next)
	{
		try{
			const queryJoin = [
        {
          path:'category',
          select:['category']
        }
      ];
      ProjectModel.find({  }, {}).populate(queryJoin).lean(true).exec((err, data)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(data.length > 0)
          {
             data.forEach(object=>
          {
            //console.log(object.user_id[0].firstName);
            object.category = object.category[0].category;
            if(object.status==1)
            {
              object.status="Actif";
            }
            else
            {
              object.status="Inactif";
            }
          })
          //console.log(data);
          //console.log(JSON.stringify(data));
            
           //var data = [{id:1,name:"John",lname:"USA",email:"USA"},{id:1,name:"Ronny",lname:"Germany",email:"Germany"}];
           var fields = ['_id', 'category','title', 'description','status'];


            //const fields = ['field1', 'field2', 'field3'];
            const opts = { fields };
            const parser = new Parser(opts);
            const csv = parser.parse(data);
            //var path=  Date.now()+'.csv'; 
            var path=  'projectData.csv'; 
              fs.writeFile(path, csv, function(err,data) {
                if (err) {throw err;}
                else{ 
                  res.download(path); // This is what you need
                }
            });
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });
		}catch(err){
	      console.log(err);
	  }
	},
};
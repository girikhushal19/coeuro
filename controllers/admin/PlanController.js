const express = require('express');
const UsersModel = require("../../models/UsersModel");
const CategoryModel = require("../../models/CategoryModel");
const ModelsModel = require("../../models/ModelsModel");
const AttributeModel = require("../../models/AttributeModel");
const UserPlanModel = require("../../models/UserPlanModel");
const ProUserPlanModel = require("../../models/ProUserPlanModel");
const TopUrgentPlan = require("../../models/TopUrgentPlan");
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');
/*/*/
module.exports = {
  addPlanNormalUser:async function(req,res,next)
  {
    console.log(req.body);
    try{
      var { day_number , price } = req.body;
          UserPlanModel.count({day_number:day_number,price:price},(errs, allReadyLike)=>
          {
            //console.log("allReadyLike"+allReadyLike);
            if(allReadyLike == 0)
            {
              UserPlanModel.create({
              user_type:"normal_user",
              day_number,
              price
            });
            var return_response = {"error":false,success: true,errorMessage:"Plan ajouté avec succès"};
            res.status(200)
                    .send(return_response);
            }else{
              var return_response = {"error":true,success: false,errorMessage:"Le même plan est déjà disponible"};
                res.status(200).send(return_response);
            }
          });
    }catch(error)
    {
      console.log(error)
        res.status(404).json({
          status: 'fail',
          message: error,
        });
    }
  },
  allPlanNormalUser:async function(req,res,next)
  {
    console.log(req.body);
    try{
      //var { day_number , price } = req.body;
          UserPlanModel.find({},(errs, record)=>
          {
            if(errs)
            {
              var return_response = {"error":true,success: false,errorMessage:errs};
              res.status(400)
                    .send(return_response);
            }else{
              var return_response = {"error":false,success: true,errorMessage:"Dossier",record:record};
              res.status(200)
                    .send(return_response);
            }
            
             
          });
    }catch(error)
    {
      console.log(error)
        res.status(404).json({
          status: 'fail',
          message: error,
        });
    }
  },
  addPlanProUser:async function(req,res,next)
  {
    console.log(req.body);
    try{

      var { top_price,day_number,maximum_upload , price } = req.body;
          ProUserPlanModel.count({top_price:top_price,day_number:day_number,maximum_upload:maximum_upload,price:price},(errs, allReadyLike)=>
          {
            //console.log("allReadyLike"+allReadyLike);
            if(allReadyLike == 0)
            {
              ProUserPlanModel.create({
              user_type:"pro_user",
              maximum_upload,
              day_number,
              price,
              top_price
            });
            var return_response = {"error":false,success: true,errorMessage:"Plan ajouté avec succès"};
            res.status(200)
                    .send(return_response);
            }else{
              var return_response = {"error":true,success: false,errorMessage:"Le même plan est déjà disponible"};
                res.status(200).send(return_response);
            }
          });
    }catch(error)
    {
      console.log(error)
        res.status(404).json({
          status: 'fail',
          message: error,
        });
    }
  },
  allPlanProUser:async function(req,res,next)
  {
    console.log(req.body);
    try{
      //var { day_number , price } = req.body;
          ProUserPlanModel.find({},(errs, record)=>
          {
            if(errs)
            {
              var return_response = {"error":true,success: false,errorMessage:errs};
              res.status(400)
                    .send(return_response);
            }else{
              var return_response = {"error":false,success: true,errorMessage:"Dossier",record:record};
              res.status(200)
                    .send(return_response);
            }
            
             
          });
    }catch(error)
    {
      console.log(error)
        res.status(404).json({
          status: 'fail',
          message: error,
        });
    }
  },

  addTopUrgentPlan:async function(req,res,next)
  {
    console.log(req.body);
    try{
      var { price } = req.body;
            //console.log("allReadyLike"+allReadyLike);
            await TopUrgentPlan.findOneAndUpdate({ _id:req.body.old_id },{
              user_type:"normal_user",
              price
            });
            var return_response = {"error":false,success: true,errorMessage:"Plan mis à jour avec succès"};
            res.status(200)
            .send(return_response);
             
         
    }catch(error)
    {
      console.log(error)
        res.status(404).json({
          status: 'fail',
          message: error,
        });
    }
  },

  allTopUrgentPlan:async function(req,res,next)
  {
    console.log(req.body);
    try{
      //var { day_number , price } = req.body;
          TopUrgentPlan.find({},(errs, record)=>
          {
            if(errs)
            {
              var return_response = {"error":true,success: false,errorMessage:errs};
              res.status(400)
                    .send(return_response);
            }else{
              var return_response = {"error":false,success: true,errorMessage:"Dossier",record:record};
              res.status(200)
                    .send(return_response);
            }
            
             
          });
    }catch(error)
    {
      console.log(error)
        res.status(404).json({
          status: 'fail',
          message: error,
        });
    }
  },
};
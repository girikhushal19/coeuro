const express = require('express');
const CategoryModel = require("../../models/CategoryModel");
const SubCategoryModel = require("../../models/SubCategoryModel");
const ProjectModel = require("../../models/ProjectModel");
const AdminModel = require('../../models/AdminModel');
const mongoose = require("mongoose");
//const { validate, ValidationError, Joi,body, validationResult } = require('express-validation');
var bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const fs = require('fs');
const app = express();
const customConstant = require('../../helpers/customConstant');

/*/*/
module.exports = {
	addCategorySubmit: async function(req, res,next)
  {
    //console.log(req.body);return false;
  	/*console.log("all data");
  	
    console.log(req.files);
    console.log(req.files.video);
    console.log(req.files.file);return false;*/
    //console.log(req.body.all_text_data);
    /*console.log(req.files.video);
    console.log(req.files.file);
    console.log(req.files.file.length);*/
  	try{
      var {price,category,html,heading,allTitle,donation_type} = req.body;

      //console.log(req.body);return false;


  		var images = [];
      var video = [];
      //var video_title = [];
      if(allTitle)
      {
        var video_title = allTitle.split(",");
      }else{
        var video_title = [];
      }
      
      if(category)
      {
        if(donation_type != '' && donation_type != null)
        {
          if(req.files.file)
          {
            if(req.files.file)
            {
              var p_r_i = req.files.file;
              for(let m=0; m<p_r_i.length; m++)
              {
                let images_name_obj = { "filename":req.files.file[m].filename,"path":req.files.file[m].path  };
                images.push(images_name_obj);
                }
                //console.log(exterior_image);
            }
          }
          if(req.files.video)
          {
            if(req.files.video)
            {
              var p_r_i = req.files.video;
              for(let m=0; m<p_r_i.length; m++)
              {
                let images_name_obj = { "filename":req.files.video[m].filename,"path":req.files.video[m].path  };
                video.push(images_name_obj);
                }
                //console.log(exterior_image);
            }
          }
          if(price == "" || price == "null")
          {
            //console.log("here if condition");
            price = null;
          }
          CategoryModel.count({category},(error,CategoryCount)=>{
            if(error)
            {
              console.log(error);
            }else{
              if(CategoryCount == 0)
              {
                  CategoryModel.create({
                    donation_type:donation_type,
                    heading:heading,
                    description:html,
                    video_title:video_title,
                    price,
                    category,
                    images,
                    video
                  });
                res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Catégorie ajoutée avec succès"
                    });
              }else{
                res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: "Catégorie déjà existante"
                  });
              }
            }
          });
        }else{
          res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: "Veuillez sélectionner un type de don"
          });
        }
      }else{
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "La catégorie est requise"
        });
      }
      
  	}catch(err){
  		console.log(err);
  	}
  },
  editCategorySubmit: async function(req, res,next)
  {
    // console.log("all data");
     //console.log(req.body);
     //return false;
    // console.log(req.body);
    // console.log(req.files);
    // console.log(req.files.video);
    // console.log(req.files.file);
    // return false;
    //console.log(req.body.all_text_data);
    /*console.log(req.files.video);
    console.log(req.files.file);
    console.log(req.files.file.length);*/
    try{
      var {price,category,html,heading,allTitle,donation_type} = req.body;
      var video_title = allTitle.split(",");
      var final_image = {};
      var images = [];
      var new_images = [];
      var video = [];
      var new_video = [];
      var new_video_title = [];
      if(req.files.file)
      {
        if(req.files.file)
        {
          var p_r_i = req.files.file;
          for(let m=0; m<p_r_i.length; m++)
          {
            let images_name_obj = { "filename":req.files.file[m].filename,"path":req.files.file[m].path  };
             images.push(images_name_obj);
            }
            //console.log(exterior_image);
        }
      }


      if(req.files.video)
      {
        if(req.files.video)
        {
          var p_r_i = req.files.video;
          for(let m=0; m<p_r_i.length; m++)
          {
            let images_name_obj = { "filename":req.files.video[m].filename,"path":req.files.video[m].path  };
             video.push(images_name_obj);
            }
            //console.log(exterior_image);
        }
      }
      
       
      CategoryModel.find({ _id:req.body.edit_id }, {images:1,video:1,video_title:1}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
         // console.log("result"+result);
         // console.log("result"+result[0].video);
          //console.log("result"+result[0].video.length);
          if(result.length > 0)
          {
              /*console.log(result[0].images);
              console.log("old new both file");
              console.log(images);*/
              if(video.length>0)
              {
                for (let i = 0; i < result[0].video.length; i++)
                {
                  //console.log("hereeee");
                  new_video.push(result[0].video[i]);
                }
                for (let j = 0; j < video.length; j++)
                {
                  new_video.push(video[j]);
                }
              }else{
                //console.log("video is not here");
                new_video = result[0].video;
              }
              
              if(donation_type == "" || donation_type == null)
              {
                donation_type = result[0].donation_type;
              }
              if(images.length>0)
              {

                if(video_title.length>0)
                {
                  if(result[0].video_title)
                  {
                    for (let i = 0; i < result[0].video_title.length; i++)
                    {
                      new_video_title.push(result[0].video_title[i]);
                    }
                  }
                  for (let j = 0; j < video_title.length; j++)
                  {
                    new_video_title.push(video_title[j]);
                  }
                }

                for (let i = 0; i < result[0].images.length; i++)
                {
                  /*console.log("hereeeeeeee");
                  console.log(result[0].images[i]);*/
                  new_images.push(result[0].images[i]);
                }
                for (let j = 0; j < images.length; j++)
                {
                  //console.log("hereeeeeeee");
                  //console.log(result[0].images[j]);
                  new_images.push(images[j]);
                }
                /*for (let i = 0; i < images.length; i++)
                {
                  console.log("hereeeeeeee");
                  console.log(result[0].images[i]);
                  new_images.push(images[i]);
                }*/
                 
               // console.log(new_video);
              //return false;
                /*console.log("new_images image");
                console.log(new_images);*/
                CategoryModel.updateOne({ "_id":req.body.edit_id }, 
                {
                  donation_type:donation_type,
                  heading:heading,
                  description:html,
                  video_title:new_video_title,
                  category:req.body.category,
                  price:req.body.price,
                  images:new_images,
                  video:new_video,
                }, function (uerr, docs) {
                if (uerr){
                  console.log(uerr);
                  res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: uerr
                    });
                }
                else{
                  res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Enregistrement complet des succès mis à jour"
                    });
                  }
                });
              }else{
                //console.log("elseee");
                CategoryModel.updateOne({ "_id":req.body.edit_id }, 
                {
                  donation_type:donation_type,
                  heading:heading,
                  description:html,
                  video_title:video_title,
                  category:req.body.category,
                  price:req.body.price,
                  images:result[0].images,
                  video:new_video,
                }, function (uerr, docs) {
                if (uerr){
                  //console.log(uerr);
                  res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: uerr
                    });
                }
                else{
                  res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Enregistrement complet des succès mis à jour"
                    });
                  }
                });
              }
          }else{
            var return_response = {"error":true,errorMessage:"Quelque chose a mal tourné","record":result};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(err){
      console.log(err);
    } 
  },
  getAdminCategory:async function(req,res,next)
  {
    var base_url = customConstant.base_url;
      //console.log("get profile");
      try{
        //console.log(user_id);
        var categoryRecord = await CategoryModel.find({ status:1 }); 
        //console.log("categoryRecord"+categoryRecord);
        //console.log("categoryRecord"+categoryRecord.length);
        if(categoryRecord)
        {
          if(categoryRecord.length === 0)
          {
            res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
          }else{
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    base_url:base_url,
                    errorMessage: "Enregistrement de la catégorie",
                    record:categoryRecord,
                });
          }
        }else{
          console.log("here");
          res.status(200)
                .send({
                    error: true,
                    success: false,
                    base_url:base_url,
                    errorMessage: "Pas d'enregistrement"
                });
        }
        
      }catch(err){
        console.log(err);
      }
  },
  allCategoryCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
       CategoryModel.countDocuments({  }).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allCategory: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      CategoryModel.find({  }, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  getSingleCategory: async function(req, res,next)
  {
    try{
      CategoryModel.find({ _id:req.body.id }, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  updateCatStatusApi: async function(req, res,next)
  {
    CategoryModel.updateOne({ "_id":req.body.id }, 
    {status:req.body.status}, function (uerr, docs) {
    if (uerr){
      //console.log(uerr);
      res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: uerr
        });
    }
    else{
      res.status(200)
        .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
        });
      }
    });
  },
  removeCategoryImageApi:async function(req,res,next)
  {
    /*const objArr = [
      { name: 'Eleven', show: 'Stranger Things' },
      { name: 'Jonas', show: 'Dark' },
      { name: 'Mulder', show: 'The X Files' },
      { name: 'Ragnar', show: 'Vikings' },
      { name: 'Scully', show: 'The X Files' },
    ];*/
    /*remainingArr = objArr.filter(data => data.name != 'Ragnar');
    console.log(remainingArr);*/
    var filename = req.body.filename;
    var path = req.body.path;
    var new_images = [];
    //console.log(req.body);
    CategoryModel.find({ _id:req.body.id }, {images:1}).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        //console.log("here");
        if(result.length >0)
        {
          //console.log("here 1 if");
          if(result[0].images.length > 0)
          {
            var uploadDir = './public/uploads/category/';
            let fileNameWithPath = uploadDir + filename;
            //console.log(fileNameWithPath);
            //console.log(userImage);
            if (fs.existsSync(fileNameWithPath))
            {
              fs.unlink(uploadDir + filename, (err) => 
              {
                console.log("unlink file error "+err);
              });
            }

            //console.log(result[0].images);
            //console.log("here 2 if");
            var objArr = result[0].images;
            remainingArr = objArr.filter(data => data.filename != filename);
            //console.log(remainingArr);
            for (let j = 0; j < remainingArr.length; j++)
            {
              //console.log("hereeeeeeee");
              //console.log(result[0].images[j]);
              new_images.push(remainingArr[j]);
            }
            CategoryModel.updateOne({ "_id":req.body.id }, 
            {
              images:new_images
            }, function (uerr, docs) {
            if (uerr){
              //console.log(uerr);
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: uerr
                });
            }
            else{
              res.status(200)
                .send({
                    error: false,
                    success: true,
                    errorMessage: "Enregistrement complet des succès mis à jour"
                });
              }
            });
          }else{
            res.status(200)
              .send({
                error: false,
                success: true,
                errorMessage: "Aucune image"
            });
          }
        }else{
          res.status(200)
            .send({
            error: false,
            success: true,
            errorMessage: "Aucune image"
          });
        }
      }
    });
  },


  removeCategoryVideo:async function(req,res,next)
  {
    
    var filename = req.body.filename;
    var path = req.body.path;
    var new_images = [];
    //console.log(req.body);
    CategoryModel.find({ _id:req.body.id }, {video:1}).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        //console.log("here");
        if(result.length >0)
        {
          //console.log("here 1 if");
          if(result[0].video.length > 0)
          {
            //console.log(result[0].images);
            //console.log("here 2 if");

            
            var uploadDir = './public/uploads/category/';
            let fileNameWithPath = uploadDir + filename;
            //console.log(fileNameWithPath);
            //console.log(userImage);
            if (fs.existsSync(fileNameWithPath))
            {
              fs.unlink(uploadDir + filename, (err) => 
              {
                console.log("unlink file error "+err);
              });
            }


            var objArr = result[0].video;
            remainingArr = objArr.filter(data => data.filename != filename);
            //console.log(remainingArr);
            for (let j = 0; j < remainingArr.length; j++)
            {
              //console.log("hereeeeeeee");
              //console.log(result[0].images[j]);
              new_images.push(remainingArr[j]);
            }
            CategoryModel.updateOne({ "_id":req.body.id }, 
            {
              video:new_images
            }, function (uerr, docs) {
            if (uerr){
              //console.log(uerr);
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: uerr
                });
            }
            else{
              res.status(200)
                .send({
                    error: false,
                    success: true,
                    errorMessage: "Enregistrement complet des succès mis à jour"
                });
              }
            });
          }else{
            res.status(200)
              .send({
                error: false,
                success: true,
                errorMessage: "Aucune image"
            });
          }
        }else{
          res.status(200)
            .send({
            error: false,
            success: true,
            errorMessage: "Aucune image"
          });
        }
      }
    });
  },

  removeVideoTitleApi:async function(req,res,next)
  {
    
    var filename = req.body.filename;
    var path = req.body.path;
    var new_images = [];
    //console.log(req.body);
    CategoryModel.find({ _id:req.body.id }, {video_title:1}).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        //console.log("here");
        if(result.length >0)
        {
          //console.log("here 1 if");
          if(result[0].video_title.length > 0)
          {
            //console.log(result[0].images);
            //console.log("here 2 if");
            var objArr = result[0].video_title;

            remainingArr = objArr.filter(data => data != filename);
            console.log(objArr);
            for (let j = 0; j < remainingArr.length; j++)
            {
              //console.log("hereeeeeeee");
              //console.log(result[0].images[j]);
              new_images.push(remainingArr[j]);
            }

            // console.log("objArr "+objArr);
            // console.log("filename "+filename);
            // console.log("remainingArr "+remainingArr);
            // console.log("hereeee");
            // return false;
            CategoryModel.updateOne({ "_id":req.body.id }, 
            {
              video_title:new_images
            }, function (uerr, docs) {
            if (uerr){
              //console.log(uerr);
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: uerr
                });
            }
            else{
              res.status(200)
                .send({
                    error: false,
                    success: true,
                    errorMessage: "Enregistrement complet des succès mis à jour"
                });
              }
            });
          }else{
            res.status(200)
              .send({
                error: false,
                success: true,
                errorMessage: "Aucune image"
            });
          }
        }else{
          res.status(200)
            .send({
            error: false,
            success: true,
            errorMessage: "Aucune image"
          });
        }
      }
    });
  },
  addSubCategorySubmit: async function(req, res,next)
  {
    /*console.log("all data");
    console.log(req.body);
    console.log(req.files);
    console.log(req.files.video);
    console.log(req.files.file);*/
    //console.log(req.body.all_text_data);
    /*console.log(req.files.video);
    console.log(req.files.file);
    console.log(req.files.file.length);*/
    //console.log(req.files);
    try{
      var {price,category,title,description,donation_type,heading} = req.body;
      var images = [];
      //var video = [];
      if(req.files)
      {
        //console.log(req.files);
          var p_r_i = req.files;
          for(let m=0; m<p_r_i.length; m++)
          {
            let images_name_obj = { "filename":req.files[m].filename,"path":req.files[m].path  };
             images.push(images_name_obj);
            }
            //console.log(exterior_image);
        
      }
      /*console.log(images);
      return false;*/

      if(price == "" || price == "null")
      {
        //console.log("here if condition");
        price = null;
      }
      SubCategoryModel.create({
          heading:heading,
          donation_type:donation_type,
          price,
          category,
          images,
          title,
          description,
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
      
    }catch(err){
      console.log(err);
    }
  },
  editSubCategorySubmit: async function(req, res,next)
  {
    /*console.log("all data");
    console.log(req.body);
    return false;*/
    /*console.log("all data");
    console.log(req.body);
    console.log(req.files);
    console.log(req.files.video);
    console.log(req.files.file);*/
    //console.log(req.body.all_text_data);
    /*console.log(req.files.video);
    console.log(req.files.file);
    console.log(req.files.file.length);*/
    //console.log(req.files);
    try{
      var {price,category,title,description,donation_type,heading} = req.body;
      var images = [];
      //var video = [];
      if(req.files)
      {
        //console.log(req.files);
        var p_r_i = req.files;
        for(let m=0; m<p_r_i.length; m++)
        {
          let images_name_obj = { "filename":req.files[m].filename,"path":req.files[m].path  };
            images.push(images_name_obj);
        }
          //console.log(exterior_image);
        
      }
      /*console.log(images);
      return false;*/
      /*console.log(req.body);
       console.log(req.body.edit_id);
        return false;*/
        
      SubCategoryModel.find({ _id:req.body.edit_id }, {images:1}).exec((err, result)=>
      {
        if(err)
        {
          //console.log("herrrrrerr");
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
              /*console.log(result[0].images);
              console.log("old new both file");
              console.log(images);*/
              if(images.length>0)
              {
                //console.log("images is here");
                if(result[0].images.length > 0)
                {
                  //console.log(result[0].images[0]);
                  if(result[0].images[0].path)
                  {
                    if (fs.existsSync(result[0].images[0].path))
                    {
                      fs.unlink(result[0].images[0].path, (err) => 
                      {
                        console.log("unlink file error "+err);
                      });
                    }
                  }
                }
              }else{
                //console.log("images is not here");
                images = result[0].images;
              }

              if(donation_type == "" || donation_type == null)
              {
                donation_type = result[0].donation_type;
              }

              //console.log(req.body);
              /*console.log(req.body.category);
              console.log(req.body.price);
              console.log(req.body.title);
              console.log(req.body.description);
              console.log(images);
              return false;*/
             

              //console.log("elseee");
              SubCategoryModel.updateOne({ "_id":req.body.edit_id }, 
              {
                heading:req.body.heading,
                category:req.body.category,
                price:req.body.price,
                images:images,
                title:req.body.title,
                description:req.body.description,
                donation_type:donation_type
              }, function (uerr, docs) {
              if (uerr){
                //console.log(uerr);
                res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: uerr
                  });
              }
              else{
                res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "Enregistrement complet des succès mis à jour"
                  });
                }
              });
          }else{
            var return_response = {"error":true,errorMessage:"Quelque chose a mal tourné","record":result};
            res.status(200).send(return_response);
          }
        }
      });
    }catch(err){
      console.log(err);
    }
  },

  
  getSingleSubCategory: async function(req, res,next)
  {
    try{
      SubCategoryModel.find({ _id:req.body.id }, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  allSubCategoryCount: async function(req, res,next)
  {
    let total = 0;
    var totalPageNumber = 1;
    var perPageRecord = 10;
    //console.log("here");
    //console.log(req.body);
    try{
       SubCategoryModel.countDocuments({  }).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
       
    }catch(err){
      console.log(err);
    }
  },
  allSubCategory: async function(req, res,next)
  {
    var xx = 0;
    var total = 0;
    var perPageRecord = 10;
    var fromindex = 0;
    if(req.body.numofpage == 0)
    {
      fromindex = 0;
    }else{
      fromindex = perPageRecord * req.body.numofpage
    }
    try{
      const queryJoin = [
          {
            path:'category',
            select:['category']
          }
        ];

      SubCategoryModel.find({  }, {}).populate(queryJoin).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });

    }catch(err){
      console.log(err);
    }
  },
  updateSubCatStatusApi: async function(req, res,next)
  {
    SubCategoryModel.updateOne({ "_id":req.body.id }, 
    {status:req.body.status}, function (uerr, docs) {
    if (uerr){
      //console.log(uerr);
      res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: uerr
        });
    }
    else{
      res.status(200)
        .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
        });
      }
    });
  },
  addProjectSubmit: async function(req, res,next)
  {
    // console.log("all data");
    // console.log(req.body);
    // return false;
    /*console.log(req.files.video);
    console.log(req.files.file);*/
    //console.log(req.body.all_text_data);
    /*console.log(req.files);
    console.log(req.files);*/
    //console.log(req.files.file.length);
    try{
      var {title,description,category} = req.body;
      var images = [];
      
      if(req.files)
      {
          let images_name_obj = { "filename":req.files[0].filename,"path":req.files[0].path  };
             images.push(images_name_obj);
          /*console.log("images");
          console.log(images);*/
        
      }
      ProjectModel.count({title},(error,CategoryCount)=>{
        if(error)
        {
          console.log(error);
        }else{
          if(CategoryCount == 0)
          {
              ProjectModel.create({
                category,
                title,
                description,
                images
              });
            res.status(200)
                .send({
                    error: false,
                    success: true,
                    errorMessage: "Projet ajouté avec succès"
                });
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Projet existant"
              });
          }
        }
      });
    }catch(err){
      console.log(err);
    }
  },
  addPriceProjectSubmit: async function(req, res,next)
  {
    // console.log("all data");
    // console.log(req.body);
    // return false;
    /*console.log(req.files.video);
    console.log(req.files.file);*/
    //console.log(req.body.all_text_data);
    /*console.log(req.files);
    console.log(req.files);*/
    //console.log(req.files.file.length);
    try{
      var {title,description,html,allPrice,smallDescription,allTitle} = req.body;
      //console.log(req.body);
      // allTitle = JSON.parse(allTitle);
      // allPrice = JSON.parse(allPrice);
      // console.log("allPrice ",allPrice);
      // return false;
      var images = [];
      var is_price = 0;
      if(allPrice)
      {
        if(allPrice.length > 0)
        {
          is_price = 1;
        }
      }
      var images = [];
      
      if(req.files)
      {
          let images_name_obj = { "filename":req.files[0].filename,"path":req.files[0].path  };
             images.push(images_name_obj);
          /*console.log("images");
          console.log(images);*/
        
      }
       
      ProjectModel.create({ 
        project_type:1,
        title:title,
        images:images,
        description:html,
        price:allPrice,
        smallDescription:smallDescription,
        is_price:is_price,
        allTitle:allTitle
      }).then((result)=>{
          res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Projet ajouté avec succès"
          });
      }).catch((error)=>{
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: error.message
          });
      });
    }catch(err){
      res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: err.message
      });
    }
  },
  editProjectSubmit: async function(req, res,next)
  {
    /*console.log("all data");
    console.log(req.body);
    return false;*/
    /*console.log(req.files.video);
    console.log(req.files.file);*/
    //console.log(req.body.all_text_data);
    /*console.log(req.files);
    console.log(req.files);*/
    //console.log(req.files.file.length);
    try{
      var {title,description,category} = req.body;
      var images = [];
      //var video = [];
      if(req.files)
      {
        //console.log(req.files);
          var p_r_i = req.files;
          for(let m=0; m<p_r_i.length; m++)
          {
            let images_name_obj = { "filename":req.files[m].filename,"path":req.files[m].path  };
             images.push(images_name_obj);
            }
            //console.log(exterior_image);
        
      }
       



      ProjectModel.find({ _id:req.body.edit_id }, {images:1}).exec((err, result)=>
      {
        if(err)
        {
          //console.log("herrrrrerr");
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
              /*console.log(result[0].images);
              console.log("old new both file");
              console.log(images);*/
              if(images.length>0)
              {
                //console.log("images is here");
                if(result[0].images.length > 0)
                {
                  //console.log(result[0].images[0]);
                  if(result[0].images[0].path)
                  {
                    if (fs.existsSync(result[0].images[0].path))
                    {
                      fs.unlink(result[0].images[0].path, (err) => 
                      {
                        console.log("unlink file error "+err);
                      });
                    }
                  }
                }
              }else{
                //console.log("images is not here");
                images = result[0].images;
              }
              //console.log(req.body);
              /*console.log(req.body.category);
              console.log(req.body.price);
              console.log(req.body.title);
              console.log(req.body.description);
              console.log(images);
              return false;*/
             

              //console.log("elseee");
              ProjectModel.updateOne({ "_id":req.body.edit_id }, 
              {
                category:req.body.category,
                images:images,
                title:req.body.title,
                description:req.body.description,
              }, function (uerr, docs) {
              if (uerr){
                //console.log(uerr);
                res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: uerr
                  });
              }
              else{
                res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "Enregistrement complet des succès mis à jour"
                  });
                }
              });
          }else{
            var return_response = {"error":true,errorMessage:"Quelque chose a mal tourné","record":result};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(err){
      console.log(err);
    }
  },
  editPriceProjectSubmit: async function(req, res,next)
  {
    // console.log("all data");
    // console.log(req.body);
    // return false;
    /*console.log(req.files.video);
    console.log(req.files.file);*/
    //console.log(req.body.all_text_data);
    /*console.log(req.files);
    console.log(req.files);*/
    //console.log(req.files.file.length);
    try{
      var {title,html,edit_id,smallDescription} = req.body;
      var images = [];
      //var video = [];
       
       
      ProjectModel.find({ _id:req.body.edit_id }, {images:1}).exec((err, result)=>
      {
        if(err)
        {
          //console.log("herrrrrerr");
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
            ProjectModel.updateOne({ "_id":req.body.edit_id }, 
            {
              smallDescription:req.body.smallDescription,
              //images:images,
              title:req.body.title,
              description:req.body.html, 
            }, function (uerr, docs) {
            if (uerr){
              //console.log(uerr);
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: uerr
                });
            }
            else{
              res.status(200)
                .send({
                    error: false,
                    success: true,
                    errorMessage: "Enregistrement complet des succès mis à jour"
                });
              }
            });
          }else{
            var return_response = {"error":true,errorMessage:"Quelque chose a mal tourné","record":result};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(err){
      console.log(err);
    }
  },
  addSubCategoryVideo: async function(req, res,next)
  {
    //  console.log("all data");
    // console.log(req.body);
    // console.log(req.files);
    // console.log(req.files.video);
    // return false;
    // console.log(req.body);
    // console.log(req.files.file);
    // return false;
    //console.log(req.body.all_text_data);
    /*console.log(req.files.video);
    console.log(req.files.file);
    console.log(req.files.file.length);*/
    try{
      var {allTitle} = req.body;
      var video_title = allTitle.split(",");
      var final_image = {};
      var images = [];
      var new_images = [];
      var video = [];
      var new_video = [];
      var new_video_title = [];


      if(req.files.video)
      {
        if(req.files.video)
        {
          var p_r_i = req.files.video;
          for(let m=0; m<p_r_i.length; m++)
          {
            let images_name_obj = { "filename":req.files.video[m].filename,"path":req.files.video[m].path  };
             video.push(images_name_obj);
            }
            //console.log(exterior_image);
        }
      }
      
       
      SubCategoryModel.find({ _id:req.body.edit_id }, {images:1,video:1,video_title:1}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result.length > 0)
          {
              /*console.log(result[0].images);
              console.log("old new both file");
              console.log(images);*/
              
              /*console.log(video);
              return false;*/
              
              if(video_title.length>0)
              {
                if(result[0].video_title)
                {
                  for (let i = 0; i < result[0].video_title.length; i++)
                  {
                    new_video_title.push(result[0].video_title[i]);
                  }
                }
                for (let j = 0; j < video_title.length; j++)
                {
                  new_video_title.push(video_title[j]);
                }
              }
              if(video.length>0)
              {
                for (let i = 0; i < result[0].video.length; i++)
                {
                  new_images.push(result[0].video[i]);
                }
                for (let j = 0; j < video.length; j++)
                {
                  new_images.push(video[j]);
                }

                /*console.log("new_images image");
                console.log(new_images);*/
                SubCategoryModel.updateOne({ "_id":req.body.edit_id }, 
                {
                  video_title:new_video_title,
                  video:new_images,
                }, function (uerr, docs) {
                if (uerr){
                  console.log(uerr);
                  res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: uerr
                    });
                }
                else{
                  res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Enregistrement complet des succès mis à jour"
                    });
                  }
                });
              }else{
                var return_response = {"error":true,errorMessage:"Veuillez sélectionner la vidéo et le titre"};
                res.status(200).send(return_response);
              }
          }else{
            var return_response = {"error":true,errorMessage:"Quelque chose a mal tourné","record":result};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(err){
      console.log(err);
    } 
  },
  
};
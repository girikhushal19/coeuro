const jwt = require("jsonwebtoken");

const UsersModel = require("../models/UsersModel");
//const config = process.env;

const verifyToken = (req, res, next) => {
  var token =
    req.body.token || req.query.token || req.headers["x-access-token"];
    var user_id = req.headers["user-id"];

  if (!token) {
     return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "A token is required for authentication"
        });
  }
  if (!user_id) {
     return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "A token of user id required for authentication"
        });
  }
  try {
    const decoded = jwt.verify(token, "Porschists");
    //console.log(decoded);
    //req.user = decoded;

  } catch (err) {
    //return res.status(401).send("");

     return res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "Invalid Token"
        });
  }
  return next();
};

module.exports = verifyToken;
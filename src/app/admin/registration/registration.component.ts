import { Component, OnInit } from '@angular/core';

import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
 



@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  private adminRegistration:string = "";
  private adminLoginCheck:string = "";
  private base_url:string = "";
  
  fileName = '';
      test:any;formData:any;formValue:any;userImageName:any;
      apiResponse:any;token:any;user_type:any;
      apiStringify:any;user_id:any;loggedInDetail:any;
  constructor(private _http:HttpClient)
  {
    this.adminRegistration = "http://localhost:3000/webadmin/adminRegistration";    
  }

  ngOnInit(): void {
    
    //console.log("on init");
  }



  form = new UntypedFormGroup(
  {
    //name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    firstName: new UntypedFormControl('', [Validators.required]),
    lastName: new UntypedFormControl('', [Validators.required]),
    userName: new UntypedFormControl('', [Validators.required]),
    email: new UntypedFormControl('', [Validators.required, Validators.email]),
    password: new UntypedFormControl('',[Validators.required, Validators.minLength(6)]),
    confirmPassword: new UntypedFormControl('',[Validators.required, Validators.minLength(6)]),
     
  } );
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) {         //{1}
  Object.keys(formGroup.controls).forEach(field => {  //{2}
    const control = formGroup.get(field);             //{3}
    if (control instanceof UntypedFormControl) {             //{4}
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof UntypedFormGroup) {        //{5}
      this.validateAllFormFields(control);            //{6}
    }
  });
}

  submit(){
      if (this.form.valid)
      {
        this.formValue = this.form.value;
        //this.formValue.file = this.userImageName;
          //console.log('form submitted'); 
          //console.log(this.formValue); 
          //this.test = this.fileToUpload;
          this._http.post(this.adminRegistration,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          //console.log("response of api");
          //console.log(JSON.stringify(response));
          this.apiResponse = response;
          //this.apiStringify = JSON.stringify(response);
          //console.log(this.apiStringify);

          //console.log(this.apiResponse.token);
          //console.log(this.apiResponse.data[0]._id);
          //console.log(this.apiResponse.data[0].email);
          console.log("response"+  this.apiResponse);
          
          /*if(this.apiResponse.error == false)
          {
            localStorage.setItem("token",this.apiResponse.token);
          localStorage.setItem("_id",this.apiResponse.data[0]._id);
          localStorage.setItem("email",this.apiResponse.data[0].email);
          localStorage.setItem("user_type",this.apiResponse.user_type);
          
            window.location.href = this.base_url+"admin/dashboard";
          }*/
              this.form.reset();
        });
        //var formData = new FormData();
        //formData.append('userfile', this.form.get('fileSource').value);
        //console.log(this.form);
      }else{
        //console.log('erro form submitted');
        this.validateAllFormFields(this.form); 
        // validate all form fields
      }
  }

}

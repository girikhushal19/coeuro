import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllDonationTitleComponent } from './all-donation-title.component';

describe('AllDonationTitleComponent', () => {
  let component: AllDonationTitleComponent;
  let fixture: ComponentFixture<AllDonationTitleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllDonationTitleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllDonationTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

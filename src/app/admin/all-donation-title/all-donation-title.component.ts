import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-all-donation-title',
  templateUrl: './all-donation-title.component.html',
  styleUrls: ['./all-donation-title.component.css']
})
export class AllDonationTitleComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allGalleryImage:any;queryParam:any;numbers:any;allGalleryImageCount:any;apiStringify:any;deleteGalleryImage:any;base_url_node_plain:any;myJson:any;
  selectedIndex: number;


  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
 
    this.allGalleryImage = this.base_url_node+"allDonationTitle";
    this.deleteGalleryImage = this.base_url_node+"deleteDonationTitle";
    this.getallBanner(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {
    
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allGalleryImage,this.queryParam).subscribe((response:any)=>{
      //console.log("allGalleryImage"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }
  deleteRecord(id=null)
  {
    /*console.log("hii");
    console.log(id);*/
    this.queryParam = {"id":id};
    this._http.post(this.deleteGalleryImage,this.queryParam).subscribe((response:any)=>{ 
      //console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }
}

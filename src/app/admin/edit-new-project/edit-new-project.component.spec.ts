import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditNewProjectComponent } from './edit-new-project.component';

describe('EditNewProjectComponent', () => {
  let component: EditNewProjectComponent;
  let fixture: ComponentFixture<EditNewProjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditNewProjectComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditNewProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

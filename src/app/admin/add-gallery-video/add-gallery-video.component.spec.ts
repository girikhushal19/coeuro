import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGalleryVideoComponent } from './add-gallery-video.component';

describe('AddGalleryVideoComponent', () => {
  let component: AddGalleryVideoComponent;
  let fixture: ComponentFixture<AddGalleryVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddGalleryVideoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGalleryVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

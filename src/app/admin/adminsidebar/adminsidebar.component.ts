import { Component, OnInit } from '@angular/core';
//import { Router,NavigationEnd  } from '@angular/router';
import { Router,NavigationStart, Event as NavigationEvent } from '@angular/router';
import { Location } from '@angular/common';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';

import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-adminsidebar',
  templateUrl: './adminsidebar.component.html',
  styleUrls: ['./adminsidebar.component.css']
})
export class AdminsidebarComponent implements OnInit {

  currentRoute:any;currentUrlSegment:any;urlLength:any;base_url_node:any;getAdminProfile:any;
  record:any;removeCategoryImage:any;old_email:any;old_firstName:any;old_lastName:any;old_userImage:any;old_id:any;getqueryParam:any;base_url_node_plain:any;base_url:any;

  constructor(private router: Router,private Location:Location,private loginAuthObj:LoginauthenticationService,private _http:HttpClient){
      //console.log(this.router.url);
      //console.log(this.Location.path())
      this.currentRoute = this.router.url.split('/');
      //console.log(this.currentRoute);
      //console.log(this.currentRoute.length);
      this.urlLength = this.currentRoute.length-1;
      //this.currentUrlSegment = this.currentRoute[this.urlLength];
      //console.log("currentUrlSegment  "+this.currentUrlSegment);

      if(this.currentRoute.length == 3)
      {
        //console.log("here");
        this.urlLength = this.currentRoute.length-2;
        this.currentUrlSegment = this.currentRoute[this.urlLength];
      }else{
        this.urlLength = this.currentRoute.length-1;
        this.currentUrlSegment = this.currentRoute[this.urlLength];
      }
      console.log("currentUrlSegment  "+this.currentUrlSegment);

      this.base_url = this.loginAuthObj.base_url;
      this.base_url_node_plain = this.loginAuthObj.base_url_node;
      this.base_url_node = this.loginAuthObj.base_url_node_admin;

      this.getqueryParam = {};
      this.getAdminProfile = this.base_url_node+"getAdminProfile"; 
      this._http.post(this.getAdminProfile,this.getqueryParam).subscribe((response:any)=>{
      //console.log("getAdminProfile"+response);
      //this.apiResponse = response;
        this.record = response.record;

        /*console.log(this.record); 
        console.log(this.record.adminImage); */
        //console.log(this.record[0]);

        const date = new Date();
        var hours = Math.abs(date.getTime() - new Date(this.record.loggedin_time).getTime()) / 3600000;
        console.log("hours "+hours);
        if(hours > 24)
        {
          window.location.href = this.base_url+"logout";
        }



        this.old_email = this.record.email;
        this.old_firstName = this.record.firstName;
        this.old_lastName = this.record.lastName;  
        this.old_id = this.record._id; 
        //this.old_userImage = this.record.adminImage; 
        this.old_userImage = this.base_url_node_plain+"public/uploads/adminProfile/"+this.record.adminImage;
        //console.log(this.old_userImage); 
        /*this.old_category = this.record[0].category;
        this.old_price = this.record[0].price;
        this.old_images = this.record[0].images;*/
        /*console.log(this.record[0]._id); 
        console.log(this.record[0].price); */
        //console.log(this.old_category); 
      });



    }

  ngOnInit(): void {
    //console.log("sidebar");

  }

}

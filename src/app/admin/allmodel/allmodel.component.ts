import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-allmodel',
  templateUrl: './allmodel.component.html',
  styleUrls: ['./allmodel.component.css']
})
export class AllmodelComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allModel:any;queryParam:any;numbers:any;allModelCount:any;base_url_node_only:any;
  selectedIndex: number;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
     
    //this.getModel = this.base_url_node+"getModel";
    //console.log("here");
    

    this.allModelCount = this.base_url_node+"modelTotalCount";
    this.allModel = this.base_url_node+"allModelList";
    this.getallModel(0);
    this.selectedIndex = 0;


  }

  ngOnInit(): void {

    this._http.post(this.allModelCount,this.queryParam).subscribe((response:any)=>{
      console.log("allModelCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      console.log("this.numbers "+this.numbers);
    });


  }
  getallModel(numofpage=0)
  {
    this.selectedIndex = numofpage;


    console.log("numofpage"+numofpage);
    this.queryParam = {"numofpage":numofpage};
    //console.log(this.queryParam);
    this._http.post(this.allModel,this.queryParam).subscribe((response:any)=>{
      console.log("allModel"+JSON.stringify(response));
      this.record = response.record;
      
      //console.log("totalPageNumber"+this.totalPageNumber);
      //console.log(this.numbers);
    });
  }
}

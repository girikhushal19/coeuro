import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators,FormArray, FormBuilder,FormGroup} from '@angular/forms';
import { Editor } from 'ngx-editor';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.css']
})
export class EditCategoryComponent implements OnInit {
  editor: Editor;
  html: any;
  
  fileInputLabel: string = "";
  myFiles:string [] = [];
  allTitle:string [] = [];
  myFilesVideo:string [] = [];

  edit_id: string;
  imageSrc: string = '';
  old_html:any;
  old_video:any;
  base_url = "";base_url_node = ""; base_url_node_only = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editCategorySubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;getSingleCategory:any;getqueryParam:any;removeQueryParam:any;old_category:any;old_price:any;old_images:any;record:any;removeCategoryImage:any;productForm: FormGroup;old_heading:any;
  removeCategoryVideo:any;old_video_title:any;removeVideoTitleApi:any;
  constructor(private fb:FormBuilder,private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {

    this.editor = new Editor();
     this.html = "";  

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    //console.log("this.base_url"+this.base_url);
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editCategorySubmit = this.base_url_node+"editCategorySubmit";
    this.removeCategoryImage = this.base_url_node+"removeCategoryImageApi";
    this.removeCategoryVideo = this.base_url_node+"removeCategoryVideo";
    this.removeVideoTitleApi = this.base_url_node+"removeVideoTitleApi";
    
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");




    this.edit_id = this.actRoute.snapshot.params['id'];
    //console.log(this.edit_id);
    this.getqueryParam = {"id":this.edit_id};
    this.getSingleCategory = this.base_url_node+"getSingleCategory"; 
    this._http.post(this.getSingleCategory,this.getqueryParam).subscribe((response:any)=>{
    //console.log("getSingleCategory"+response);
    //this.apiResponse = response;
      this.record = response.record;

      /*console.log(this.record); 
      console.log(this.record[0]); */
 
      this.old_category = this.record[0].category;
      this.old_price = this.record[0].price;
      this.old_images = this.record[0].images;
      this.old_heading = this.record[0].heading;
      this.old_html = this.record[0].description;
      this.old_video = this.record[0].video;
      this.old_video_title = this.record[0].video_title;
      /*console.log(this.record[0]._id); 
      console.log(this.record[0].price); */
      //console.log(this.old_category); 
    });
    this.productForm = this.fb.group({
      donation_type:'',
      heading: '',
      html: '',
      price: '',
      category: '',
      images: '',      
      quantities: this.fb.array([]) ,
    });
   }

  ngOnInit(): void {
      this.myFiles = [];
      //this.video = null;
      this.allTitle = [];
      this.myFilesVideo = [];
      this.editor = new Editor();
  }
  ngOnDestroy(): void {
    this.editor.destroy();
  }
  
  
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }

  onVideoChange(event:any) {
    
    if (event.target.files.length > 0)
    {
      //const video_file = event.target.files[0];
      //this.video = video_file;
      this.myFilesVideo.push(event.target.files[0]);
      //console.log(file);
       
    }
  }

  quantities() : FormArray {
    return this.productForm.get("quantities") as FormArray
  }
  
  newQuantity(): FormGroup {
    return this.fb.group({
      
      video: '',
      title: '',
    })
  }
  
  addQuantity() {
    this.quantities().push(this.newQuantity());
  }
  
  removeQuantity(i:number) {
    this.quantities().removeAt(i);
  }


  onSubmit()
  {
        //this.formData.append('edit_id', this.record[0]._id);
        this.apiResponse = {"error":false,"msg":""};
        
        for(let x=0; x<this.productForm.value.quantities.length; x++)
        {
          this.allTitle.push(this.productForm.value.quantities[x].title);
        }
        
        this.apiResponse = {"error":false,"msg":""};
    
        this.formData = new FormData();
        //this.formData.append('video', this.video);
        
        this.formData.append('donation_type', this.productForm.value.donation_type);
        this.formData.append('category', this.productForm.value.category);
        this.formData.append('price', this.productForm.value.price);
        this.formData.append('html', this.productForm.value.html);
        this.formData.append('heading', this.productForm.value.heading);
        this.formData.append('allTitle', this.allTitle);
        this.formData.append('edit_id', this.edit_id);
        
        //this.formData.append('file', this.images);
        for (var i = 0; i < this.myFiles.length; i++)
        { 
          this.formData.append("file", this.myFiles[i]);
        }
        for (var i = 0; i < this.myFilesVideo.length; i++)
        { 
          this.formData.append("video", this.myFilesVideo[i]);
        }
        //console.log(this.video);
        
      //this.formData.append('parent_id', this.form.value.parent_id);
      //this.formData.append('attribute_type', this.form.value.attribute_type);
      //this.formData.append('images[]', this.myFiles[0]);
      
      //console.log("form valuess");
      //console.log(this.formData);
      
      /*this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);*/
      //formData.append('file', this.images);
      /*this.formValue = this.form.value;
      this.formValue = this.images;*/
      //console.log(this.form.value);
      ///console.log(this.form.value.model_name);
      

      this._http.post(this.editCategorySubmit,this.formData).subscribe((response:any)=>{
          console.log("response of api"+ JSON.stringify(response));
          this.apiResponse = response;
          if(this.apiResponse.error == false)
          {
            setTimeout(() => {
                window.location.href = this.base_url+"allCategory";
            }, 2000); 
          }
      });
    
  }
  removeCategory(id:string,filename:string,path:string)
  {
    console.log(id);
    console.log(filename);
    console.log(path);
    this.removeQueryParam = { "id":id,"filename":filename,"path":path };
    this._http.post(this.removeCategoryImage,this.removeQueryParam).subscribe((response:any)=>{
      console.log(response);
      this._http.post(this.getSingleCategory,this.getqueryParam).subscribe((response:any)=>{
      //console.log("getSingleCategory"+response);
      //this.apiResponse = response;
        this.record = response.record;

        /*console.log(this.record); 
        console.log(this.record[0]); */
   
        this.old_category = this.record[0].category;
        this.old_price = this.record[0].price;
        this.old_images = this.record[0].images;
        /*console.log(this.record[0]._id); 
        console.log(this.record[0].price); */
        //console.log(this.old_category); 
      });
    });
  }


  removeVideoCategory(id:string,filename:string,path:string)
  {
    console.log(id);
    console.log(filename);
    console.log(path);
    this.removeQueryParam = { "id":id,"filename":filename,"path":path };
    console.log(this.removeQueryParam);
    //return false;
    this._http.post(this.removeCategoryVideo,this.removeQueryParam).subscribe((response:any)=>{
      console.log(response);
      this._http.post(this.getSingleCategory,this.getqueryParam).subscribe((response:any)=>{
      //console.log("getSingleCategory"+response);
      //this.apiResponse = response;
        this.record = response.record;

        /*console.log(this.record); 
        console.log(this.record[0]); */
   
        this.old_category = this.record[0].category;
        this.old_price = this.record[0].price;
        this.old_images = this.record[0].images;
        this.old_heading = this.record[0].heading;
        this.old_html = this.record[0].description;
        this.old_video = this.record[0].video;
        this.old_video_title = this.record[0].video_title;
        
        /*console.log(this.record[0]._id); 
        console.log(this.record[0].price); */
        //console.log(this.old_category); 
      });
    });
  }
  removeVideoTitle(id:string,videoTitle:string)
  {
    console.log(id);
    console.log(videoTitle);
    this.removeQueryParam = { "id":id,"filename":videoTitle };
    console.log(this.removeQueryParam);
    //return false;
    this._http.post(this.removeVideoTitleApi,this.removeQueryParam).subscribe((response:any)=>{
      console.log(response);
      this._http.post(this.getSingleCategory,this.getqueryParam).subscribe((response:any)=>{
      //console.log("getSingleCategory"+response);
      //this.apiResponse = response;
        this.record = response.record;

        /*console.log(this.record); 
        console.log(this.record[0]); */
   
        this.old_category = this.record[0].category;
        this.old_price = this.record[0].price;
        this.old_images = this.record[0].images;
        this.old_heading = this.record[0].heading;
        this.old_html = this.record[0].description;
        this.old_video = this.record[0].video;
        this.old_video_title = this.record[0].video_title;
        
        /*console.log(this.record[0]._id); 
        console.log(this.record[0].price); */
        //console.log(this.old_category); 
      });
    });
  }
}

import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-addsubmodel',
  templateUrl: './addsubmodel.component.html',
  styleUrls: ['./addsubmodel.component.css']
})
export class AddsubmodelComponent implements OnInit {

  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;addModelsSubmit:any;getModel:any;allModelList:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addModelsSubmit = this.base_url_node+"addModelsSubmit";
    this.getModel = this.base_url_node+"getModel";
    //console.log("here");
    
  }


  ngOnInit(): void {
    this._http.post(this.getModel,this.formValue).subscribe((response:any)=>{
      console.log("response get model"+response);
      this.allModelList = response.result;
    });
  }



  form = new UntypedFormGroup({
    parent_id: new UntypedFormControl('', []),
    model_name: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

   

  
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      console.log(this.formValue);
      this._http.post(this.addModelsSubmit,this.formValue).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            this.form.reset();
            this._http.post(this.getModel,this.formValue).subscribe((response:any)=>{
              console.log("response get model"+response);
              this.allModelList = response.result;
            });
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllGalleryVideoComponent } from './all-gallery-video.component';

describe('AllGalleryVideoComponent', () => {
  let component: AllGalleryVideoComponent;
  let fixture: ComponentFixture<AllGalleryVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllGalleryVideoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllGalleryVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-invoice-order',
  templateUrl: './invoice-order.component.html',
  styleUrls: ['./invoice-order.component.css']
})
export class InvoiceOrderComponent implements OnInit {

  imageSrc: string = '';
  edit_id: string = '';
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editProjectSubmit:any;getCategory:any;allModelList:any;getCarCategory:any;allCarCategory:any;getSinglePaymentRecord:any;getqueryParam:any;
  old_order_id:any;old_payment_type:any;
  old_price:any;old_lastName:any;
  old_firstName:any;old_category_title:any;
  record:any;old_payment_initiate_date:any;
  old_email:any;
  old_mobileNumber:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.edit_id = this.actRoute.snapshot.params['id'];
    //console.log(this.edit_id);
    this.getqueryParam = {"id":this.edit_id};

    this.getSinglePaymentRecord = this.base_url_node+"getSinglePaymentRecord"; 
    this._http.post(this.getSinglePaymentRecord,this.getqueryParam).subscribe((response:any)=>{
    //console.log("getSinglePaymentRecord"+JSON.stringify(response));
    //this.apiResponse = response;
        if(response.error == false)
        {
          this.record = response.record;
          //console.log(this.record); 
          //console.log(this.record[0]);
           this.old_order_id = this.record.order_id;
           this.old_firstName = this.record.firstName;
           this.old_lastName = this.record.lastName;
           this.old_email = this.record.email;
           this.old_mobileNumber = this.record.mobileNumber;
           this.old_category_title = this.record.category_title;
           this.old_price = this.record.price;
           this.old_payment_type = this.record.payment_type;
           this.old_payment_initiate_date = this.record.payment_initiate_date;

           
        }
      });
    
  }


  ngOnInit(): void {
    
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxEditorModule } from 'ngx-editor';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminheaderComponent } from './adminheader/adminheader.component';
import { AdminfooterComponent } from './adminfooter/adminfooter.component';
import { AdminlogoutComponent } from './adminlogout/adminlogout.component';
import { AdminsidebarComponent } from './adminsidebar/adminsidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { ModelComponent } from './model/model.component';
import { AllmodelComponent } from './allmodel/allmodel.component';
import { SubmodelComponent } from './submodel/submodel.component';
import { AddsubmodelComponent } from './addsubmodel/addsubmodel.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { AddprojectComponent } from './addproject/addproject.component';
import { AddGalleryImageComponent } from './add-gallery-image/add-gallery-image.component';
import { AddGalleryVideoComponent } from './add-gallery-video/add-gallery-video.component';
import { AddSubCategoryComponent } from './add-sub-category/add-sub-category.component';
import { AddBannerComponent } from './add-banner/add-banner.component';
import { SendPushNotificationComponent } from './send-push-notification/send-push-notification.component';
import { AllBannerComponent } from './all-banner/all-banner.component';
import { AllCategoryComponent } from './all-category/all-category.component';
import { AllSubCategoryComponent } from './all-sub-category/all-sub-category.component';
import { AllProjectComponent } from './all-project/all-project.component';
import { AllPushNotificationComponent } from './all-push-notification/all-push-notification.component';
import { AllGalleryImageComponent } from './all-gallery-image/all-gallery-image.component';
import { AllGalleryVideoComponent } from './all-gallery-video/all-gallery-video.component';
import { AllUsersComponent } from './all-users/all-users.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { EditSubCategoryComponent } from './edit-sub-category/edit-sub-category.component';
import { EditProjectComponent } from './edit-project/edit-project.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { EditPaypalKeyComponent } from './edit-paypal-key/edit-paypal-key.component';
import { EditStripeKeyComponent } from './edit-stripe-key/edit-stripe-key.component';
import { EditAdminProfileComponent } from './edit-admin-profile/edit-admin-profile.component';
import { AdminChangePasswordComponent } from './admin-change-password/admin-change-password.component';
import { AllUsersDonationComponent } from './all-users-donation/all-users-donation.component';
import { EditAboutUsComponent } from './edit-about-us/edit-about-us.component';
import { AllForumComponent } from './all-forum/all-forum.component';
import { StripeKeyComponent } from './stripe-key/stripe-key.component';
import { PaypalKeyComponent } from './paypal-key/paypal-key.component';
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { DetailForumComponent } from './detail-forum/detail-forum.component';
import { DetailGalleryComponent } from './detail-gallery/detail-gallery.component';
import { AddSubCategoryVideoComponent } from './add-sub-category-video/add-sub-category-video.component';
import { DonationDetailComponent } from './donation-detail/donation-detail.component';
import { TermsAndConditionComponent } from './terms-and-condition/terms-and-condition.component';
import { PrivacyAndPolicyComponent } from './privacy-and-policy/privacy-and-policy.component';


import { AngularEditorModule } from '@kolkov/angular-editor';
import { InvoiceOrderComponent } from './invoice-order/invoice-order.component';
import { AddNewProjectComponent } from './add-new-project/add-new-project.component';
import { EditNewProjectComponent } from './edit-new-project/edit-new-project.component';
import { AllNewsLaterComponent } from './all-news-later/all-news-later.component';
import { SettingComponent } from './setting/setting.component';
import { EditPriceProjectComponent } from './edit-price-project/edit-price-project.component';
import { AddDonationTitleComponent } from './add-donation-title/add-donation-title.component';
import { AllDonationTitleComponent } from './all-donation-title/all-donation-title.component';
import { EditDonationTitleComponent } from './edit-donation-title/edit-donation-title.component';


@NgModule({
  declarations: [
    AdminheaderComponent,
    AdminfooterComponent,
    AdminlogoutComponent,
    AdminsidebarComponent,
    DashboardComponent,
    HomeComponent,
    RegistrationComponent,
    ModelComponent,
    AllmodelComponent,
    SubmodelComponent,
    AddsubmodelComponent,
    AddCategoryComponent,
    AddprojectComponent,
    AddGalleryImageComponent,
    AddGalleryVideoComponent,
    AddSubCategoryComponent,
    AddBannerComponent,
    SendPushNotificationComponent,
    AllBannerComponent,
    AllCategoryComponent,
    AllSubCategoryComponent,
    AllProjectComponent,
    AllPushNotificationComponent,
    AllGalleryImageComponent,
    AllGalleryVideoComponent,
    AllUsersComponent,
    EditCategoryComponent,
    EditSubCategoryComponent,
    EditProjectComponent,
    EditUserComponent,
    EditPaypalKeyComponent,
    EditStripeKeyComponent,
    EditAdminProfileComponent,
    AdminChangePasswordComponent,
    AllUsersDonationComponent,
    EditAboutUsComponent,
    AllForumComponent,
    StripeKeyComponent,
    PaypalKeyComponent,
    DetailForumComponent,
    DetailGalleryComponent,
    AddSubCategoryVideoComponent,
    DonationDetailComponent,
    TermsAndConditionComponent,
    PrivacyAndPolicyComponent,
    InvoiceOrderComponent,
    AddNewProjectComponent,
    EditNewProjectComponent,
    AllNewsLaterComponent,
    SettingComponent,
    EditPriceProjectComponent,
    AddDonationTitleComponent,
    AllDonationTitleComponent,
    EditDonationTitleComponent
    
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxEditorModule,
    AngularEditorModule,
    NgMultiSelectDropDownModule.forRoot(),
  ]
})
export class AdminModule { }

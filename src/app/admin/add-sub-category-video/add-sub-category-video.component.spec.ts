import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubCategoryVideoComponent } from './add-sub-category-video.component';

describe('AddSubCategoryVideoComponent', () => {
  let component: AddSubCategoryVideoComponent;
  let fixture: ComponentFixture<AddSubCategoryVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSubCategoryVideoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddSubCategoryVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

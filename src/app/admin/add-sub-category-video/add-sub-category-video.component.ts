import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators,FormArray, FormBuilder,FormGroup} from '@angular/forms';
import { Editor } from 'ngx-editor';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-sub-category-video',
  templateUrl: './add-sub-category-video.component.html',
  styleUrls: ['./add-sub-category-video.component.css']
})
export class AddSubCategoryVideoComponent implements OnInit {
  editor: Editor;edit_id: string;
  html: any;
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  allTitle:string [] = [];
  myFilesVideo:string [] = [];
  base_url = "";base_url_node = ""; 
  productForm: FormGroup;
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;addSubCategoryVideo:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;
  constructor(private actRoute: ActivatedRoute,private fb:FormBuilder,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.editor = new Editor();
     this.html = "";  

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addSubCategoryVideo = this.base_url_node+"addSubCategoryVideo";
    //this.getModel = this.base_url_node+"getModel";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    this.productForm = this.fb.group({
      edit_id:'',
      
      quantities: this.fb.array([]) ,
    });
    this.edit_id = this.actRoute.snapshot.params['id'];
  }


  ngOnInit(): void {
    this.allTitle = [];
      this.myFiles = [];
      this.myFilesVideo = [];
      this.editor = new Editor();
  }
  ngOnDestroy(): void {
    this.editor.destroy();
  }
  

  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }

  onVideoChange(event:any) {
    
      if (event.target.files.length > 0)
      {
        //const video_file = event.target.files[0];
        //this.video = video_file;
        this.myFilesVideo.push(event.target.files[0]);
        //console.log(file);
         
      }
  }

  quantities() : FormArray {
    return this.productForm.get("quantities") as FormArray
  }
   
  newQuantity(): FormGroup {
    return this.fb.group({
       
      video: '',
      title: '',
    })
  }
   
  addQuantity() {
    this.quantities().push(this.newQuantity());
  }
   
  removeQuantity(i:number) {
    this.quantities().removeAt(i);
  }

  onSubmit()
  {
    //console.log(this.productForm);
    //console.log(this.productForm.value.quantities);
    for(let x=0; x<this.productForm.value.quantities.length; x++)
    {
      this.allTitle.push(this.productForm.value.quantities[x].title);
    }
    
    this.apiResponse = {"error":false,"msg":""};
  
      this.formData = new FormData();
      //this.formData.append('video', this.video);
        this.formData.append('edit_id', this.edit_id);
        this.formData.append('allTitle', this.allTitle);
        
      //this.formData.append('file', this.images);
        
        for (var i = 0; i < this.myFilesVideo.length; i++)
        { 
          this.formData.append("video", this.myFilesVideo[i]);
        }
        //console.log(this.formData);
        //return false;
      //this.formData.append('parent_id', this.form.value.parent_id);
      //this.formData.append('attribute_type', this.form.value.attribute_type);
      //this.formData.append('images[]', this.myFiles[0]);
      //console.log(this.formData);
      /*this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);*/
      //formData.append('file', this.images);
      /*this.formValue = this.form.value;
      this.formValue = this.images;*/
      //console.log(this.form.value);
      ///console.log(this.form.value.model_name);
      this._http.post(this.addSubCategoryVideo,this.formData).subscribe((response:any)=>{
        console.log("response of api"+response);
        this.apiResponse = response;

        if(this.apiResponse.error == false)
        {
          //this.form.reset();
          //this.myFiles = [];this.video = null;
          setTimeout(() => {
            window.location.href = this.base_url+"allSubCategory";
            //window.location.reload();
          }, 2000);
        }
      });
    
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPushNotificationComponent } from './all-push-notification.component';

describe('AllPushNotificationComponent', () => {
  let component: AllPushNotificationComponent;
  let fixture: ComponentFixture<AllPushNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllPushNotificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllPushNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

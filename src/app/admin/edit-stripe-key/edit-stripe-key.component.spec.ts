import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditStripeKeyComponent } from './edit-stripe-key.component';

describe('EditStripeKeyComponent', () => {
  let component: EditStripeKeyComponent;
  let fixture: ComponentFixture<EditStripeKeyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditStripeKeyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditStripeKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {
  token:any;formData:any;base_url:any;base_url_node:any;user_type:any;apiResponse:any;formValue:any;settingSubmit:any;getModel:any;allModelList:any;getSetting:any;allCategoryType:any;
  additional_price_old:any;default_delivery_time_old:any;default_shipping_old:any;max_delivery_distance_old:any;driver_support_mobile_old:any;driver_support_email_old:any;max_pickup_distance_old:any;
  admin_commission_old:any;
  access_pur_tax_percent_old:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.settingSubmit = this.base_url_node+"settingSubmit";
    //this.getModel = this.base_url_node+"getModel";
    this.getSetting = this.base_url_node+"getSetting";
    //console.log("here");
    
  }
  ngOnInit(): void {
    this._http.post(this.getSetting,{}).subscribe((response:any)=>{
      this.allCategoryType = response.result;
       console.log("response of api"+this.allCategoryType);
      // console.log("response of api"+this.allCategoryType[1]);
      console.log("response of api"+JSON.stringify(this.allCategoryType));
      //this.apiResponse = response;    
      for(let i=0; i<this.allCategoryType.length; i++)
      {
        if(this.allCategoryType[i].attribute_key == "additional_price")
        {
          this.additional_price_old = this.allCategoryType[i].attribute_value;
        }
         
        
      }
    });
  }

  form = new UntypedFormGroup({
    additional_price: new UntypedFormControl('', [Validators.required]),
   // access_pur_tax_percent: new UntypedFormControl('', [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  
  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this._http.post(this.settingSubmit,this.form.value).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllNewsLaterComponent } from './all-news-later.component';

describe('AllNewsLaterComponent', () => {
  let component: AllNewsLaterComponent;
  let fixture: ComponentFixture<AllNewsLaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllNewsLaterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AllNewsLaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

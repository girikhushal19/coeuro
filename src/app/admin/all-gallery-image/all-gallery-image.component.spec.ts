import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllGalleryImageComponent } from './all-gallery-image.component';

describe('AllGalleryImageComponent', () => {
  let component: AllGalleryImageComponent;
  let fixture: ComponentFixture<AllGalleryImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllGalleryImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllGalleryImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminlogoutComponent } from './adminlogout/adminlogout.component';
import { RegistrationComponent } from './registration/registration.component';
import { ModelComponent } from './model/model.component';
import { AllmodelComponent } from './allmodel/allmodel.component';
import { SubmodelComponent } from './submodel/submodel.component';
import { AddsubmodelComponent } from './addsubmodel/addsubmodel.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { AddprojectComponent } from './addproject/addproject.component';
import { AddGalleryImageComponent } from './add-gallery-image/add-gallery-image.component';
import { AddGalleryVideoComponent } from './add-gallery-video/add-gallery-video.component';
import { AddSubCategoryComponent } from './add-sub-category/add-sub-category.component';
import { AddBannerComponent } from './add-banner/add-banner.component';
import { SendPushNotificationComponent } from './send-push-notification/send-push-notification.component';
import { AllBannerComponent } from './all-banner/all-banner.component';
import { AllCategoryComponent } from './all-category/all-category.component';
import { AllSubCategoryComponent } from './all-sub-category/all-sub-category.component';
import { AllProjectComponent } from './all-project/all-project.component';
import { AllPushNotificationComponent } from './all-push-notification/all-push-notification.component';
import { AllGalleryImageComponent } from './all-gallery-image/all-gallery-image.component';
import { AllGalleryVideoComponent } from './all-gallery-video/all-gallery-video.component';
import { AllUsersComponent } from './all-users/all-users.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { EditSubCategoryComponent } from './edit-sub-category/edit-sub-category.component';
import { EditProjectComponent } from './edit-project/edit-project.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { EditPaypalKeyComponent } from './edit-paypal-key/edit-paypal-key.component';
import { EditStripeKeyComponent } from './edit-stripe-key/edit-stripe-key.component';
import { EditAdminProfileComponent } from './edit-admin-profile/edit-admin-profile.component';
import { AdminChangePasswordComponent } from './admin-change-password/admin-change-password.component';
import { AllUsersDonationComponent } from './all-users-donation/all-users-donation.component';
import { EditAboutUsComponent } from './edit-about-us/edit-about-us.component';
import { AllForumComponent } from './all-forum/all-forum.component';
import { StripeKeyComponent } from './stripe-key/stripe-key.component';
import { PaypalKeyComponent } from './paypal-key/paypal-key.component';
import { DetailForumComponent } from './detail-forum/detail-forum.component';
import { DetailGalleryComponent } from './detail-gallery/detail-gallery.component';
import { AddSubCategoryVideoComponent } from './add-sub-category-video/add-sub-category-video.component';
import { DonationDetailComponent } from './donation-detail/donation-detail.component';
import { TermsAndConditionComponent } from './terms-and-condition/terms-and-condition.component';
import { PrivacyAndPolicyComponent } from './privacy-and-policy/privacy-and-policy.component';
import { InvoiceOrderComponent } from './invoice-order/invoice-order.component';
import { AddNewProjectComponent } from './add-new-project/add-new-project.component';
import { EditNewProjectComponent } from './edit-new-project/edit-new-project.component';
import { AllNewsLaterComponent } from './all-news-later/all-news-later.component';
import { SettingComponent } from './setting/setting.component'; 
import { AddDonationTitleComponent } from './add-donation-title/add-donation-title.component';
import { AllDonationTitleComponent } from './all-donation-title/all-donation-title.component';
import { EditDonationTitleComponent } from './edit-donation-title/edit-donation-title.component';


const routes: Routes = [
      {
        path : "",
        component : HomeComponent
      },
      {
        path : "dashboard",
        component : DashboardComponent
      },
      {
        path : "logout",
        component : AdminlogoutComponent
      },
      {
        path : "adminRegistration",
        component : RegistrationComponent
      },
      {
        path : "addCategory",
        component : AddCategoryComponent
      },
      {
        path : "addSubCategory",
        component : AddSubCategoryComponent
      },
      {
        path : "addProject",
        component : AddprojectComponent
      },
      {
        path : "addGalleryImage",
        component : AddGalleryImageComponent
      },
      {
        path : "addGalleryVideo",
        component : AddGalleryVideoComponent
      },
      {
        path : "allModel",
        component : AllmodelComponent
      },
      {
        path : "allSubModel",
        component : SubmodelComponent
      },
      {
        path : "addSubModel",
        component : AddsubmodelComponent
      },
      {
        path : "addBanner",
        component : AddBannerComponent
      },
      {
        path : "sendPushNotification",
        component : SendPushNotificationComponent
      },
      {
        path : "allBanner",
        component : AllBannerComponent
      },
      {
        path : "allCategory",
        component : AllCategoryComponent
      },
      {
        path : "allSubCategory",
        component : AllSubCategoryComponent
      },
      {
        path : "allProject",
        component : AllProjectComponent
      },
      {
        path : "allPushNotification",
        component : AllPushNotificationComponent
      },
      {
        path : "allGalleryImage",
        component : AllGalleryImageComponent
      },
      {
        path : "allGalleryVideo",
        component : AllGalleryVideoComponent
      },
      {
        path : "allUsers",
        component : AllUsersComponent
      },
      {
        path : "editCategory/:id",
        component : EditCategoryComponent
      },
      {
        path : "editSubCategory/:id",
        component : EditSubCategoryComponent
      },
      {
        path : "editProject/:id",
        component : EditProjectComponent
      },
      {
        path : "editUser/:id",
        component : EditUserComponent
      },
      {
        path : "editPaypalKey",
        component : EditPaypalKeyComponent
      },
      {
        path : "editStripeKey",
        component : EditStripeKeyComponent
      },
      {
        path : "editAdminProfile",
        component : EditAdminProfileComponent
      },
      {
        path : "adminChangePassword",
        component : AdminChangePasswordComponent
      },
      {
        path : "allUsersDonation",
        component : AllUsersDonationComponent
      },
      {
        path : "editAboutUs",
        component : EditAboutUsComponent
      },
      {
        path : "allForum",
        component : AllForumComponent
      },
      {
        path : "stripeKey",
        component : StripeKeyComponent
      },
      {
        path : "paypalKey",
        component : PaypalKeyComponent
      },
      {
        path : "detailForum/:id",
        component : DetailForumComponent
      },
      {
        path : "detailGallery/:id",
        component : DetailGalleryComponent
      },
      {
        path : "addSubCategoryVideo/:id",
        component : AddSubCategoryVideoComponent
      },
      {
        path : "donationDetail/:id",
        component : DonationDetailComponent
      },
      {
        path : "termsAndCondition",
        component : TermsAndConditionComponent
      },
      {
        path : "privacyAndPolicy",
        component : PrivacyAndPolicyComponent
      },
      {
        path : "invoiceOrder/:id",
        component : InvoiceOrderComponent
        
      },
      {
        path : "addNewProject",
        component : AddNewProjectComponent
        
      },
      {
        path : "editNewProject/:id",
        component : EditNewProjectComponent
        
      },
      {
        path : "allNewsLater",
        component : AllNewsLaterComponent
        
      },
      {
        path : "setting",
        component : SettingComponent
      },
      {
        path : "addDonationTitle",
        component : AddDonationTitleComponent
      } ,
      {
        path : "allDonationTitle",
        component : AllDonationTitleComponent
      } ,
      {
        path : "editDonationTitle/:id",
        component : EditDonationTitleComponent
      }  
    ];
    
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

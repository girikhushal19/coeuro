import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-all-category',
  templateUrl: './all-category.component.html',
  styleUrls: ['./all-category.component.css']
})
export class AllCategoryComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allCategory:any;queryParam:any;numbers:any;allCategoryCount:any;apiStringify:any;deleteBanner:any;base_url_node_plain:any;myJson:any;
  selectedIndex: number;updateCatStatusApi:any;

  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.allCategoryCount = this.base_url_node+"allCategoryCount";
    console.log(this.allCategoryCount);
    this.allCategory = this.base_url_node+"allCategory";
    this.updateCatStatusApi = this.base_url_node+"updateCatStatusApi";
    this.getallBanner(0);
    this.selectedIndex = 0;
  }

  ngOnInit(): void {
    this._http.post(this.allCategoryCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allCategoryCount"+response);
      this.totalPageNumber = response.totalPageNumber;
      this.numbers = Array(this.totalPageNumber).fill(0).map((m,n)=>n);
      //console.log("this.numbers "+this.numbers);
    });
  }
  getallBanner(numofpage=0){
    this.selectedIndex = numofpage;
    //console.log("numofpage"+numofpage);

    this.queryParam = {"numofpage":numofpage};
    this._http.post(this.allCategory,this.queryParam).subscribe((response:any)=>{
      //console.log("allCategory"+response);
      this.apiStringify = JSON.stringify(response);
      //console.log("result"+this.apiStringify);
      //console.log(this.apiStringify.error);
      this.record = response.record;
      //console.log("this record "+this.record);
    });
  }

  updateCatStatus(id=null,status:number)
  {
    /*console.log(id);
    console.log(status);*/
    this.queryParam = {"id":id,"status":status};
    this._http.post(this.updateCatStatusApi,this.queryParam).subscribe((response:any)=>{ 
      //console.log(response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.getallBanner(0);
      }
    });
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDonationTitleComponent } from './edit-donation-title.component';

describe('EditDonationTitleComponent', () => {
  let component: EditDonationTitleComponent;
  let fixture: ComponentFixture<EditDonationTitleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDonationTitleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditDonationTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

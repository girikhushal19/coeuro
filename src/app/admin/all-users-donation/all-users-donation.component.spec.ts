import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllUsersDonationComponent } from './all-users-donation.component';

describe('AllUsersDonationComponent', () => {
  let component: AllUsersDonationComponent;
  let fixture: ComponentFixture<AllUsersDonationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllUsersDonationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllUsersDonationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPaypalKeyComponent } from './edit-paypal-key.component';

describe('EditPaypalKeyComponent', () => {
  let component: EditPaypalKeyComponent;
  let fixture: ComponentFixture<EditPaypalKeyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditPaypalKeyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPaypalKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-paypal-key',
  templateUrl: './edit-paypal-key.component.html',
  styleUrls: ['./edit-paypal-key.component.css']
})
export class EditPaypalKeyComponent implements OnInit {
   
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; base_url_node_only = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editPaypalSubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;getPaypal:any;getqueryParam:any;removeQueryParam:any;
  old_mode:any;edit_id:any;
  old_client_id:any;
  old_client_secret:any;
  record:any;
  removeCategoryImage:any;
  sandbox:any;live:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    //console.log("this.base_url"+this.base_url);
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editPaypalSubmit = this.base_url_node+"editPaypalSubmit";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");




    ///this.edit_id = this.actRoute.snapshot.params['id'];
    //console.log(this.edit_id);
    this.getqueryParam = {};
    this.getPaypal = this.base_url_node+"getSinglePaypal"; 
    this._http.post(this.getPaypal,this.getqueryParam).subscribe((response:any)=>{
    //console.log("getPaypal"+response);
    //this.apiResponse = response;
      this.record = response.record;

      if(response.error == false)
      {
        //console.log("Here");
        //console.log(this.record); 
        this.edit_id = this.record[0]._id;
        this.old_mode = this.record[0].mode;
        this.old_client_id = this.record[0].client_id;
        this.old_client_secret = this.record[0].client_secret;
      }
      
      //console.log(this.record[0]);
 
      /*this.old_category = this.record[0].category;
      this.old_price = this.record[0].price;
      this.old_images = this.record[0].images;*/
      /*console.log(this.record[0]._id); 
      console.log(this.record[0].price); */
      //console.log(this.old_category); 
    });
    this.sandbox = "sandbox";
    this.live = "live";
   }

  ngOnInit(): void {
       
  }

  form = new UntypedFormGroup({
    client_id: new UntypedFormControl('', [Validators.required]),
    mode: new UntypedFormControl('', [Validators.required]),
    client_secret : new UntypedFormControl('', [Validators.required]),
    edit_id : new UntypedFormControl('', []),
    //fileSource: new FormControl('', [Validators.required])
  });
  get f(){
    return this.form.controls;
  }
   

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
   

   

  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      
        console.log(this.form.value);
        
      //this.formData.append('parent_id', this.form.value.parent_id);
      //this.formData.append('attribute_type', this.form.value.attribute_type);
      //this.formData.append('images[]', this.myFiles[0]);
      
      //console.log("form valuess");
      //console.log(this.formData);
      
      /*this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);*/
      //formData.append('file', this.images);
      /*this.formValue = this.form.value;
      this.formValue = this.images;*/
      //console.log(this.form.value);
      ///console.log(this.form.value.model_name);
      

      this._http.post(this.editPaypalSubmit,this.form.value).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;
          if(this.apiResponse.error == false)
          {
            setTimeout(() => {
                window.location.reload();
            }, 2000); 
          }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }


}

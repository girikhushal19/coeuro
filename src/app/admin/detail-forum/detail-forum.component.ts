import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';


import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-detail-forum',
  templateUrl: './detail-forum.component.html',
  styleUrls: ['./detail-forum.component.css']
})
export class DetailForumComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allForum:any;queryParam:any;numbers:any;detailForum:any;apiStringify:any;updateUserStatusApi:any;base_url_node_plain:any;myJson:any;edit_id:any;comment:any;
   


  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.detailForum = this.base_url_node+"detailForum";
  }
  ngOnInit(): void {

    this.edit_id = this.actRoute.snapshot.params['id'];
    //console.log(this.edit_id);
    this.queryParam = {"id":this.edit_id};
    this._http.post(this.detailForum,this.queryParam).subscribe((response:any)=>{
      console.log("detailForum"+response);
      if(response.error == false)
      {
        this.record = response.record;
        this.comment = response.comment;
        console.log(this.record);
        console.log(this.record[0]);
        console.log(this.record[0].user_id[0].firstName);
        console.log(this.record[0].user_id[0].lastName);
        console.log("description");
        console.log(this.record[0].description);
        console.log(this.record[0].created_at);

         

      }
    });
  }
  
  


  
  
  
  



}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-donation-detail',
  templateUrl: './donation-detail.component.html',
  styleUrls: ['./donation-detail.component.css']
})
export class DonationDetailComponent implements OnInit {
  base_url = "";base_url_node = "";
  token:any;user_type:any;apiResponse:any;formValue:any;editPageSubmit:any;getCarAttribute:any;getCarCategory:any;allCarAttribute:any;allCarCategory:any;edit_id:any;getDonationDetail:any;getqueryParam:any;record:any;old_slug_name:any;old_page_title:any;old_description:any;

  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    
    this.getDonationDetail = this.base_url_node+"getDonationDetail";
     
    this.edit_id = this.actRoute.snapshot.params['id'];
    this.getqueryParam = {"id":this.edit_id};
    this._http.post(this.getDonationDetail,this.getqueryParam).subscribe((response:any)=>{
      //console.log("getSingleModel"+JSON.stringify(response));
      this.record = response.record;
      console.log("this.record "+ JSON.stringify(this.record));
      //this.edit_id = this.record[0]._id;
      //this.old_description = this.record[0].description;
       /* console.log("edit_id "+this.edit_id)
        console.log("old_description "+this.old_description)*/

    });
  }


  ngOnInit(): void {
   
  }
  

}

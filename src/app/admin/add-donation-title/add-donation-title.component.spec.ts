import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDonationTitleComponent } from './add-donation-title.component';

describe('AddDonationTitleComponent', () => {
  let component: AddDonationTitleComponent;
  let fixture: ComponentFixture<AddDonationTitleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDonationTitleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddDonationTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

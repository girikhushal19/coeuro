import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
///import { Editor } from 'ngx-editor';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-stripe-key',
  templateUrl: './stripe-key.component.html',
  styleUrls: ['./stripe-key.component.css']
})
export class StripeKeyComponent implements OnInit {

  
  base_url = "";base_url_node = "";base_url_node_plain:any;
  token:any;user_type:any;apiResponse:any;formValue:any;editPageSubmit:any;getCarAttribute:any;getCarCategory:any;allCarAttribute:any;allCarCategory:any;edit_id:any;getSinglePage:any;getqueryParam:any;record:any;old_slug_name:any;old_stripe_secret:any;old_stripe_key:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editPageSubmit = this.base_url_node+"editStripeSubmit";
    this.getSinglePage = this.base_url_node+"getSingleStripe";
     
    //console.log("here");
     

     
    this.getqueryParam = {"id":this.edit_id};
    this._http.post(this.getSinglePage,this.getqueryParam).subscribe((response:any)=>{
      //console.log("getSingleModel"+JSON.stringify(response));
      this.record = response.result;

      this.edit_id = this.record[0]._id;
      this.old_stripe_key = this.record[0].stripe_key;
      this.old_stripe_secret = this.record[0].stripe_secret;
       /* console.log("edit_id "+this.edit_id)
        console.log("old_description "+this.old_description)*/

    });
  }


  ngOnInit(): void {
     
  }
   


  form = new UntypedFormGroup({
    edit_id: new UntypedFormControl('', []), 
    stripe_key: new UntypedFormControl('', [Validators.required]), 
    stripe_secret: new UntypedFormControl('', [Validators.required]), 
  });
  
  get f(){
    return this.form.controls;
  }

   

  
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  submit()
  {
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formValue = this.form.value;
      console.log(this.formValue);
      this._http.post(this.editPageSubmit,this.formValue).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
              
          }

           
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}

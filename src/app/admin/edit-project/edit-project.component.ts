import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators} from '@angular/forms';

import { ActivatedRoute } from '@angular/router';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.css']
})
export class EditProjectComponent implements OnInit {
  imageSrc: string = '';
  edit_id: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  base_url = "";base_url_node = ""; 
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;editProjectSubmit:any;getCategory:any;allModelList:any;getCarCategory:any;allCarCategory:any;getSingleProject:any;getqueryParam:any;old_category:any;old_price:any;old_images:any;record:any;old_title:any;old_description:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.editProjectSubmit = this.base_url_node+"editProjectSubmit";
    
    this.getCategory = this.base_url_node+"getAdminCategory";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    //console.log(this.getCategory);

    this.edit_id = this.actRoute.snapshot.params['id'];
    //console.log(this.edit_id);
    this.getqueryParam = {"id":this.edit_id};
    this.getSingleProject = this.base_url_node+"getSingleProject"; 
    this._http.post(this.getSingleProject,this.getqueryParam).subscribe((response:any)=>{
    //console.log("getSingleProject"+response);
    //this.apiResponse = response;
      this.record = response.record;

      console.log(this.record); 
      console.log(this.record[0]);
      
      this.old_category = this.record[0].category;
       
      this.old_title = this.record[0].title;
      this.old_description = this.record[0].description;
      });
    
  }


  ngOnInit(): void {
      this.myFiles = [];this.video = null;
      this._http.post(this.getCategory,this.formValue).subscribe((response:any)=>{
        this.allModelList = response.record;
        //console.log("response get model"+JSON.stringify(this.allModelList));
      });
  }

  form = new UntypedFormGroup({
    category: new UntypedFormControl('', [Validators.required]),
    title: new UntypedFormControl('', [Validators.required]),
    description: new UntypedFormControl('', [Validators.required]),
    images: new UntypedFormControl('',  []),
    
  });
  
  get f(){
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }


  submit()
  {
    
    this.apiResponse = {"error":false,"msg":""};
    if(this.form.valid)
    {
      this.formData = new FormData();
        this.formData.append('category', this.form.value.category);
        this.formData.append('title', this.form.value.title);
        this.formData.append('description', this.form.value.description);
        this.formData.append('edit_id', this.edit_id);
        for (var i = 0; i < this.myFiles.length; i++)
        { 
          this.formData.append("file", this.myFiles[i]);
        }
         
        
      //this.formData.append('parent_id', this.form.value.parent_id);
      //this.formData.append('attribute_type', this.form.value.attribute_type);
      //this.formData.append('images[]', this.myFiles[0]);
      console.log(this.formData);
      /*this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);*/
      //formData.append('file', this.images);
      /*this.formValue = this.form.value;
      this.formValue = this.images;*/
      //console.log(this.form.value);
      ///console.log(this.form.value.model_name);
      this._http.post(this.editProjectSubmit,this.formData).subscribe((response:any)=>{
          console.log("response of api"+response);
          this.apiResponse = response;

          if(this.apiResponse.error == false)
          {
            setTimeout(() => {
                window.location.href = this.base_url+"allProject";
            }, 2000); 
          }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}

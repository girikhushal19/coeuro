import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  //base_url = "http://localhost:4200/";
  token:any;user_type:any;base_url:any;allDonationCount:any;allForumCount:any;
  allUsersCount:any;base_url_node:any;base_url_node_plain:any;
  queryParam:any;allUserCount:any;allActiveUserCount:any;allInActiveUserCount:any;allDonation:any;
  allInactiveForum:any;allForum:any;allActiveForum:any;allLikeDislikeCount:any;allRecentDonation:any;allRecentForum:any;allForumRecord:any;allDonationRecord:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  {
    this.base_url = this.loginAuthObj.base_url;
    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    /*console.log("here"+this.token);
    console.log(this.user_type);*/
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_plain = this.loginAuthObj.base_url_node;
    this.allUsersCount = this.base_url_node+"allUsersCountDashboard";
    this.allDonationCount = this.base_url_node+"allDonationCountDashboard";
    this.allForumCount = this.base_url_node+"allForumCountDashboard";
    this.allLikeDislikeCount = this.base_url_node+"allLikeDislikeCount";

    this.allRecentForum = this.base_url_node+"allRecentForumDashboard";
    this.allRecentDonation = this.base_url_node+"allRecentDonationDashboard";

  }

  ngOnInit(): void {
    this._http.post(this.allUsersCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allUsersCount"+response);
      if(response.error == false)
      {
        this.allUserCount = response.record.allUserCount;
        this.allActiveUserCount = response.record.allActiveUserCount;
        this.allInActiveUserCount = response.record.allInActiveUserCount;
      }
      /*console.log(response.record);
      console.log(response.record.allUserCount);
      console.log(this.allUserCount); */
    });
    this._http.post(this.allForumCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allForumCount"+response);
      if(response.error == false)
      { 
        this.allForum = response.record.allForum;
        this.allActiveForum = response.record.allActiveForum;
        this.allInactiveForum = response.record.allInactiveForum;
      }
       
    });
    this._http.post(this.allDonationCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allDonationCount"+response);
      if(response.error == false)
      {
        //console.log(response.record);
        this.allDonation = response.record;
      }
      /*console.log(response.record); */
    });
    this._http.post(this.allLikeDislikeCount,this.queryParam).subscribe((response:any)=>{
      //console.log("allLikeDislikeCount"+response);
      if(response.error == false)
      {
        //console.log(response.record);
      }
      /*console.log(response.record); */
    });
    this._http.post(this.allRecentForum,this.queryParam).subscribe((response:any)=>{
      //console.log("allRecentForum"+response);
      if(response.error == false)
      {
        this.allForumRecord = response.record;
        //console.log(this.allForumRecord);
      }
      /*console.log(response.record); */
    });

    this._http.post(this.allRecentDonation,this.queryParam).subscribe((response:any)=>{
      //console.log("allRecentForum"+response);
      if(response.error == false)
      {
        this.allDonationRecord = response.record;
        //console.log(this.allDonationRecord);
      }
      /*console.log(response.record); */
    });


  }


  



}

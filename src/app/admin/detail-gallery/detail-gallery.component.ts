import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-detail-gallery',
  templateUrl: './detail-gallery.component.html',
  styleUrls: ['./detail-gallery.component.css']
})
export class DetailGalleryComponent implements OnInit {

  base_url = "";base_url_node = "";
  addModelsSubmit:any;getModel:any;allModelList:any;
  token:any;user_type:any;apiResponse:any;formValue:any;record:any;totalPageNumber:any;allGalleryImage:any;queryParam:any;numbers:any;singleGalleryImage:any;apiStringify:any;deleteGalleryImage:any;base_url_node_only:any;myJson:any;
  edit_id: any;


  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }

    this.singleGalleryImage = this.base_url_node+"singleGalleryImage";
  }

  ngOnInit(): void {
    this.edit_id = this.actRoute.snapshot.params['id'];
    this.queryParam = {"id":this.edit_id};
    this._http.post(this.singleGalleryImage,this.queryParam).subscribe((response:any)=>{
      console.log("singleGalleryImage"+response);
      //console.log("this.numbers "+this.numbers);
      if(response.error == false)
      {
        this.record = response.record;
      }
    });
  }

}

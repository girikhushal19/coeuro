import { Component, OnInit ,ViewChild, ElementRef} from '@angular/core';
import {LoginauthenticationService} from '../../adminservice/loginauthentication.service';
import { UntypedFormGroup, UntypedFormControl, Validators,FormArray, FormBuilder,FormGroup} from '@angular/forms';
import { Editor } from 'ngx-editor';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-add-new-project',
  templateUrl: './add-new-project.component.html',
  styleUrls: ['./add-new-project.component.css']
})
export class AddNewProjectComponent implements OnInit {
  editor: Editor;
  html: any;
  html2:any;
  imageSrc: string = '';
  fileInputLabel: string = "";
  myFiles:string [] = [];
  allPrice:string [] = [];
  allTitle:string [] = [];
  myFilesVideo:string [] = [];
  base_url = "";base_url_node = ""; 
  productForm: FormGroup;
  token:any;formData:any;images:any;video:any;user_type:any;apiResponse:any;formValue:any;addCategorySubmit:any;getModel:any;allModelList:any;getCarCategory:any;allCarCategory:any;
  constructor(private fb:FormBuilder,private _http:HttpClient,private loginAuthObj:LoginauthenticationService)
  { 
    this.editor = new Editor();
     this.html = "";  
     this.html2 = "";  

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;

    this.token = this.loginAuthObj.userLogin();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //console.log(this.token);
    //console.log(this.user_type);
    if(this.token === "" || this.user_type !== "admin")
    {
      window.location.href = this.base_url;
    }
    this.addCategorySubmit = this.base_url_node+"addPriceProjectSubmit";
    //this.getModel = this.base_url_node+"getModel";
    //this.getCarCategory = this.base_url_node+"getCarCategory";
    //console.log("here");
    this.productForm = this.fb.group({
      donation_type:'price',
      smallDescription: '',
      html: '',
      images: '',
      title: '',
      quantities: this.fb.array([]) ,
    });
  }


  ngOnInit(): void {
    this.allPrice = [];
      this.myFiles = [];
      this.myFilesVideo = [];
      this.editor = new Editor();
  }
  ngOnDestroy(): void {
    this.editor.destroy();
  }
  

  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }

  onVideoChange(event:any) {
    
      if (event.target.files.length > 0)
      {
        //const video_file = event.target.files[0];
        //this.video = video_file;
        this.myFilesVideo.push(event.target.files[0]);
        //console.log(file);
         
      }
  }

  quantities() : FormArray {
    return this.productForm.get("quantities") as FormArray
  }
   
  newQuantity(): FormGroup {
    return this.fb.group({
      title:'',
      price: '',
    })
  }
   
  addQuantity() {
    this.quantities().push(this.newQuantity());
  }
   
  removeQuantity(i:number) {
    this.quantities().removeAt(i);
  }

  onSubmit()
  {
    //console.log(this.productForm);
    //console.log(this.productForm.value.quantities);
    this.allPrice = [];
    this.allTitle = [];
    for(let x=0; x<this.productForm.value.quantities.length; x++)
    {
      this.allPrice.push(this.productForm.value.quantities[x].price);
    }
    for(let x=0; x<this.productForm.value.quantities.length; x++)
    {
      this.allTitle.push(this.productForm.value.quantities[x].title);
    }
    
    
    this.apiResponse = {"error":false,"msg":""};
  
      this.formData = new FormData();
      //this.formData.append('video', this.video);
        this.formData.append('donation_type', this.productForm.value.donation_type);
        this.formData.append('html', this.productForm.value.html);
        this.formData.append('title', this.productForm.value.title);
        this.formData.append('smallDescription', this.productForm.value.smallDescription);
        this.formData.append('allPrice', this.allPrice);
        this.formData.append('allTitle', this.allTitle);
        
      //this.formData.append('file', this.images);
        for (var i = 0; i < this.myFiles.length; i++)
        { 
          this.formData.append("file", this.myFiles[i]);
        }
        // for (var i = 0; i < this.myFilesVideo.length; i++)
        // { 
        //   this.formData.append("video", this.myFilesVideo[i]);
        // }
        //console.log(this.formData);
        //return false;
      //this.formData.append('parent_id', this.form.value.parent_id);
      //this.formData.append('attribute_type', this.form.value.attribute_type);
      //this.formData.append('images[]', this.myFiles[0]);
      //console.log(this.formData);
      /*this.formData.append('model_name', this.form.value.model_name);
      this.formData.append('parent_id', this.form.value.parent_id);
      this.formData.append('attribute_type', this.form.value.attribute_type);*/
      //formData.append('file', this.images);
      /*this.formValue = this.form.value;
      this.formValue = this.images;*/
      //console.log(this.form.value);
      ///console.log(this.form.value.model_name);
      this._http.post(this.addCategorySubmit,this.formData).subscribe((response:any)=>{
        console.log("response of api"+response);
        this.apiResponse = response;

        if(this.apiResponse.error == false)
        {
          //this.form.reset();
          this.myFiles = [];this.video = null;
          setTimeout(() => {
            //window.location.href = this.base_url+"allNormalUsers";
            window.location.reload();
          }, 2000);
        }
      });
    
  }
}

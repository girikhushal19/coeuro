import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPriceProjectComponent } from './edit-price-project.component';

describe('EditPriceProjectComponent', () => {
  let component: EditPriceProjectComponent;
  let fixture: ComponentFixture<EditPriceProjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditPriceProjectComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditPriceProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const path = require('path');
const adminRouter = require("./router/adminRouter");
const mobileApiRouter = require("./router/mobileApiRouter");
//var multer  = require('multer');
const mongoose = require('mongoose');
var cors = require('cors')
var db = require("./models/connection");
const ngrok = require('ngrok');
 


const app = express();

app.use(cors());

//const ngrok = require('ngrok');
/*(async function() {
  console.log("here");
  const url = await ngrok.connect(4040,(err, urlrr)=>{
    console.log("err"+err);
        console.log(`Node.js local server is publicly-accessible at ${urlrr}`);
  });
})();*/
 
/*const port = 4040;
const server = http.createServer((req, res) => {
    //res.end('Hello, World!');
});

server.listen(port, (err) => {
  console.log(ngrok);
    if (err) return console.log(`Something bad happened: ${err}`);
    console.log(`Node.js server listening on ${port}`);
    console.log("http://localhost:4040");
    //console.log(ngrok);
    ngrok.connect(port, function (err, url) {
      console.log("err"+err);
        console.log(`Node.js local server is publicly-accessible at ${url}`);
    });

});*/




app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'dist/coeurophelins')));

app.use('/public/uploads', express.static('public/uploads'));

/*
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/coeurophelins/index.html'));
});
*/
//app.use('/webadmin',adminRouter);
app.use('/webadmin',mobileApiRouter);
app.use('/api',mobileApiRouter);





http.createServer(app).listen(3975,()=>{
  console.log('Server Running at http://localhost:3975');
});

//app.listen(4040);

/*const port = 3000;
const host = '0.0.0.0';

app.listen(port, host, () => {
  console.log('Listening on port ' + port);
});*/



///DNS ->  192.168.96.110 IPV4->   192.168.96.242

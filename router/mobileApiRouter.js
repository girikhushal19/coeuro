const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const path = require("path");
const auth = require("../middleware/auth");
const userController = require("../controllers/users/userController");
const HomeController = require("../controllers/users/HomeController");
const StripePaymentController = require("../controllers/users/StripePaymentController");
const ForumController = require("../controllers/users/ForumController");
const PaymentHistoryController = require("../controllers/users/PaymentHistoryController");
const htmlToPdfController = require("../controllers/users/htmlToPdfController");

const adminController = require("../controllers/admin/adminController");
const CategoryController = require("../controllers/admin/CategoryController");
const CommonController = require("../controllers/admin/CommonController");
const ProjectController = require("../controllers/admin/ProjectController");
const UserController = require("../controllers/admin/UserController");
const ForumAdminController = require("../controllers/admin/ForumAdminController");


const validationMiddleware = require('../middleware/validation-middleware');
const userLoginValidation = require('../middleware/userLoginValidation');
const userGetProfile = require('../middleware/userGetProfile');
const userEditProfile = require('../middleware/userEditProfile');
const userChangePassword = require('../middleware/userChangePassword');
const GetCategoryValid = require('../middleware/GetCategoryValid');
const ForumsubmitValid = require('../middleware/ForumsubmitValid');
//const AddForumValid = require('../middleware/AddForumValid');
const ForumUserGetValid = require('../middleware/ForumUserGetValid');
const ForumSingleValid = require('../middleware/ForumSingleValid');

const CommentForumGetValid = require('../middleware/CommentForumGetValid');
const FromContactValid = require('../middleware/FromContactValid');
const MakeDonationValid = require('../middleware/MakeDonationValid');
 const modelsImage = require('../middleware/modelsImage');

 
var multer  = require('multer')
 
var storage = multer.diskStorage({

   destination: function (req, file, cb) {
      cb(null, './public/uploads/userProfile/');
   },
   filename: function (req, file, cb) {
    //console.log("updateProfile"+userEditProfile.updateProfile);
    var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
   }

});
var upload = multer({ 
    storage: storage,
    fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      //return cb("image extension error");
   
    }
  }

});

var storageAccessoires = multer.diskStorage({

   destination: function (req, file, cb) {
      cb(null, './public/uploads/addadvertise/');
   },
   filename: function (req, file, cb) {
    //console.log("updateProfile"+userEditProfile.updateProfile);
    var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
   }

});
var uploadAccessoires = multer({ 
    storage: storageAccessoires,
    fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      //return cb("image extension error");
   
    }
  }

});


const storage2 = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/addadvertise/');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
  
var uploadArray = multer({ 
  storage: storage2,
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      //return cb("image extension error", false);
   
    }
  }
});


const storage3 = multer.diskStorage({

    destination: function(req, file, cb) {
        cb(null, './public/uploads/addadvertise/');
    },
  
    filename: function(req, file, cb) {
      //console.log(req.body);
      //console.log(req.file);
      //console.log(file.originalname);
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
  

var uploadArrayPiwi = multer({ 
  storage: storage3,
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      //return cb("image extension error", false);
   
    }
  }
});
 
const storage4 = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/addadvertise/');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
  

var uploadArrayMainRepImg = multer({ 
  storage: storage4,
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg" || file.mimetype == "pdf") {
      cb(null, true);
    } else {
      cb(null, false);
      //return cb("image extension error", false);
   
    }
  }
});



const storage2Admin = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/category/');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadArrayAdmin = multer({ 
  storage: storage2Admin
});

const storageProject = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/projects/');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadProject = multer({ 
  storage: storageProject
});

const storageGallery = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/gallery/images');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadGallery = multer({ 
  storage: storageGallery
});

const storageGalleryVideo = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/gallery/video');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadGalleryVideo = multer({ 
  storage: storageGalleryVideo
});
const storageuploadCategory = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/subcategory');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadCategory = multer({ 
  storage: storageuploadCategory
});

 
const storageBanner = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/banner');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadBanner = multer({ 
  storage: storageBanner
});

const storageUsersImage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/userProfile');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadUsersImage = multer({ 
  storage: storageUsersImage
});

const storageAdminImage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/adminProfile');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadAdminImage = multer({ 
  storage: storageAdminImage
});

  

/*var upload = multer({ 
    storage: storage,
    fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      //return cb("image extension error");
   
    }
  }
});
*/ 

router.post('/userRegistration', validationMiddleware.signup, userController.userRegistration);
router.post('/userLogin', userLoginValidation.login, userController.userLogin);
router.post("/getUserProfile",auth,userGetProfile.getProfile,userController.getUserProfile);
//router.post("/updateUserProfile",auth,userEditProfile.updateProfile,upload.single('user_image'),userController.updateUserProfile);
router.post("/updateUserProfile",auth,upload.single('userImage'),userEditProfile.updateProfile,userController.updateUserProfile);

router.post("/changePassword",auth,userChangePassword.updateProfile,userController.changePassword);

router.post("/forgotPassword",userController.forgotPassword);

router.post("/logOut",auth,userGetProfile.getProfile,userController.logOut);

router.post("/deleteAC",auth,userController.deleteAC);

router.post("/fromAdminContactUs",FromContactValid.getForumvalidation,userController.fromAdminContactUs);
router.get('/getBannerHomePage',HomeController.getBannerHomePage);
router.get('/getCategory',HomeController.getCategory);
router.get('/getProject',HomeController.getProject);

router.get('/getPriceProject',HomeController.getPriceProject);
router.get("/getSingleProject/:id",HomeController.getSingleProject);
router.post("/submitEmailSubscribe",HomeController.submitEmailSubscribe);

router.get('/getGalleryImage',HomeController.getGalleryImage);
router.get('/getGalleryVideo',HomeController.getGalleryVideo);
router.post('/getSubCategory',HomeController.getSubCategory);
router.post('/getOtherUserProfile',HomeController.getOtherUserProfile);
router.post('/allPushNotificationApi',HomeController.allPushNotificationApi);

router.post('/addForum',auth,ForumsubmitValid.getForumvalidation,ForumController.addForum);
router.post('/userAllForum',auth,ForumUserGetValid.getForumvalidation,ForumController.userAllForum);
router.post('/allForum',ForumController.allForum);
router.post('/singleForum',ForumSingleValid.getForumvalidation,ForumController.singleForum);
router.post('/addComment',auth,CommentForumGetValid.getForumvalidation,ForumController.addComment);
router.post('/likeForum',ForumController.likeForum);
router.post('/disLikeForum',ForumController.disLikeForum);

router.post('/likeComment',ForumController.likeComment);
router.post('/disLikeComment',ForumController.disLikeComment);

router.get('/getAboutUs',ForumController.getAboutUs);


router.get('/getTestStripePayment',StripePaymentController.getTestStripePayment);

router.get('/testStripePayment',StripePaymentController.testStripePayment);
router.get('/testSuccessStripe',StripePaymentController.testSuccessStripe);
router.get('/testCancelStripe',StripePaymentController.testCancelStripe);

router.get('/testPaypalPayment',StripePaymentController.testPaypalPayment);
router.get('/testPaypalSuccessPayment',StripePaymentController.testPaypalSuccessPayment);
router.get('/testPaypalCancelPayment',StripePaymentController.testPaypalCancelPayment);


router.get('/getDonationStripePayment',StripePaymentController.getDonationStripePayment);
router.get('/getDonationStripePaymentNew',StripePaymentController.getDonationStripePaymentNew);
router.get('/getDonationStripeSuccess',StripePaymentController.getDonationStripeSuccess);
router.get('/getDonationStripeCancel',StripePaymentController.getDonationStripeCancel);



router.get('/getPaypalDonationPayment',StripePaymentController.getPaypalDonationPayment);
router.get('/getPaypalDonationPaymentNew',StripePaymentController.getPaypalDonationPaymentNew);
router.get('/getPaypalDonationSuccess',StripePaymentController.getPaypalDonationSuccess);
router.get('/getPaypalDonationCancel',StripePaymentController.getPaypalDonationCancel);


router.get('/getProjectDonation',StripePaymentController.getProjectDonation);
router.get('/getPaypalProjectPaymentSuccess',StripePaymentController.getPaypalProjectPaymentSuccess);
router.get('/getPaypalProjectPaymentCancel',StripePaymentController.getPaypalProjectPaymentCancel);



router.post('/doDonation',MakeDonationValid.getDonationvalidation,StripePaymentController.doDonation);

router.get('/getFCMPushTesting',PaymentHistoryController.getFCMPushTesting);

router.post('/getOwnAllPayment',PaymentHistoryController.getOwnAllPayment);
router.post('/getOwnAllPaymentHistory',PaymentHistoryController.getOwnAllPaymentHistory);

router.get('/getPrivacyPolicy',ForumController.getPrivacyPolicy);
router.get('/getTermsCondition',ForumController.getTermsCondition);

router.get('/testHtmlToPdf',htmlToPdfController.testHtmlToPdf);
router.get('/createPdfForPaymentRecords',htmlToPdfController.createPdfForPaymentRecords);

router.post('/getSinglePaymentRecord',htmlToPdfController.getSinglePaymentRecord);


//router.post('/addForum',auth,AddForumValid.getForumValidation,ForumController.addForum);
//router.post('/step7CarAdName', auth,AdvertisementStep7Valid.getAdvertisementvalidation,AdvertisementController.step7CarAdName);

/* Admin all routes start */

router.post("/adminLogin",adminController.adminLogin);
router.post("/adminRegistration",adminController.adminRegistration);


//router.post("/updateUserProfile",auth,upload.single('userImage'),userEditProfile.updateProfile,userController.updateUserProfile);
//router.post("/adminLoginCheck",adminController.adminLoginCheck);

router.post("/addCategorySubmit",uploadArrayAdmin.fields([{
           name: 'file', maxCount: 100
         }, {
           name: 'video', maxCount: 100
         }]),CategoryController.addCategorySubmit);
router.post("/editCategorySubmit",uploadArrayAdmin.fields([{
           name: 'file', maxCount: 100
         }, {
           name: 'video', maxCount: 100
         }]),CategoryController.editCategorySubmit);

router.post("/getAdminCategory",(CategoryController.getAdminCategory));
router.post("/updateCatStatusApi",(CategoryController.updateCatStatusApi));
router.post("/removeCategoryImageApi",(CategoryController.removeCategoryImageApi));
router.post("/removeCategoryVideo",(CategoryController.removeCategoryVideo));
router.post("/removeVideoTitleApi",(CategoryController.removeVideoTitleApi));

router.post("/allCategory",(CategoryController.allCategory));
router.post("/allCategoryCount",(CategoryController.allCategoryCount));
router.post("/getSingleCategory",(CategoryController.getSingleCategory));


router.post('/addProjectSubmit', uploadProject.array('file',1),CategoryController.addProjectSubmit);


router.post('/addPriceProjectSubmit', uploadProject.array('file',1),CategoryController.addPriceProjectSubmit);

router.post('/editPriceProjectSubmit' ,CategoryController.editPriceProjectSubmit);
router.post('/editProjectSubmit' ,CategoryController.editProjectSubmit);
router.post("/allProject",(ProjectController.allProject));
router.post("/getSingleProject",(ProjectController.getSingleProject));
router.post("/allProjectCount",(ProjectController.allProjectCount));
router.post("/updateProjectStatusApi",(ProjectController.updateProjectStatusApi));

router.get("/exportCsvProjectApi",(ProjectController.exportCsvProjectApi));


router.post("/addSubCategorySubmit",uploadCategory.array('file',1),(CategoryController.addSubCategorySubmit));
router.post("/editSubCategorySubmit",uploadCategory.array('file',1),(CategoryController.editSubCategorySubmit));

router.post("/addSubCategoryVideo",uploadCategory.fields([{
  name: 'video', maxCount: 100
}]),CategoryController.addSubCategoryVideo);


router.post("/allSubCategory",(CategoryController.allSubCategory));
router.post("/allSubCategoryCount",(CategoryController.allSubCategoryCount));
router.post("/updateSubCatStatusApi",(CategoryController.updateSubCatStatusApi));
router.post("/getSingleSubCategory",(CategoryController.getSingleSubCategory));


router.post('/addBannerImageSubmit', uploadBanner.array('file',1),adminController.addBannerImageSubmit);
router.post('/allBannerCount', adminController.allBannerCount);
router.post('/allBanner', adminController.allBanner);
router.post('/deleteBanner', adminController.deleteBanner);

router.post('/allNewslatterCount', adminController.allNewslatterCount);
router.post('/allNewslatter', adminController.allNewslatter);
router.post('/deleteNewslatter', adminController.deleteNewslatter);


router.post("/addDonationTitleSubmit",CommonController.addDonationTitleSubmit);
router.post("/allDonationTitle",CommonController.allDonationTitle);
router.post("/deleteDonationTitle",CommonController.deleteDonationTitle);

router.post("/donationContactUs",CommonController.donationContactUs);



router.post("/getSingleAboutUsPage",CommonController.getSingleAboutUsPage);
router.post("/editAboutUsPageSubmit",CommonController.editAboutUsPageSubmit);

router.post("/getSingleTermsConditionPage",CommonController.getSingleTermsConditionPage);
router.post("/editTermsConditionPageSubmit",CommonController.editTermsConditionPageSubmit);
router.post("/getSinglePrivacyPolicyPage",CommonController.getSinglePrivacyPolicyPage);
router.post("/editPrivacyPolicyPageSubmit",CommonController.editPrivacyPolicyPageSubmit);

router.post("/getSingleStripe",CommonController.getSingleStripe);
router.post("/editStripeSubmit",CommonController.editStripeSubmit);

router.post("/getSinglePaypal",CommonController.getSinglePaypal);
router.post("/editPaypalSubmit",CommonController.editPaypalSubmit);

router.post("/sendPushNotificationSubmit",CommonController.sendPushNotificationSubmit);

router.post("/settingSubmit",CommonController.settingSubmit);
router.post("/getSetting",CommonController.getSetting);

router.post('/allPushNotificationCount', ProjectController.allPushNotificationCount);
router.post('/allPushNotification', ProjectController.allPushNotification);
router.post('/deletePushNotification', ProjectController.deletePushNotification);

router.post('/addGalleryImageSubmit', uploadGallery.array('file',100),adminController.addGalleryImageSubmit);
router.post('/addGalleryVideoSubmit', uploadGalleryVideo.array('file',100),adminController.addGalleryVideoSubmit);

router.post('/allVideoCount', ProjectController.allVideoCount);
router.post('/allVideo', ProjectController.allVideo);
router.post('/deleteVideo', ProjectController.deleteVideo);

router.post('/singleGalleryImage', ProjectController.singleGalleryImage);


router.post('/allGalleryImageCount', ProjectController.allGalleryImageCount);
router.post('/allGalleryImage', ProjectController.allGalleryImage);
router.post('/deleteGalleryImage', ProjectController.deleteGalleryImage);


router.post("/allUsersCountDashboard",(UserController.allUsersCountDashboard));
router.post("/allDonationCountDashboard",(UserController.allDonationCountDashboard));
router.post('/allForumCountDashboard',ForumAdminController.allForumCountDashboard);
router.post('/allLikeDislikeCount',ForumAdminController.allLikeDislikeCount);

router.get('/exportCsvForumApi',ForumAdminController.exportCsvForumApi);

router.post('/allRecentForumDashboard',ForumAdminController.allRecentForumDashboard);

router.post('/allForumAdmin',ForumAdminController.allForumAdmin);
router.post('/allForumCount',ForumAdminController.allForumCount);
router.post('/updateForumApi',ForumAdminController.updateForumApi);
router.post('/detailForum',ForumAdminController.detailForum);


router.get("/exportCsvUserApi",(UserController.exportCsvUserApi));
router.post("/getAllActiveUserAdminApi",(UserController.getAllActiveUserAdminApi));
router.post("/allUsers",(UserController.allUsers));
router.post("/allUsersCount",(UserController.allUsersCount));
router.post("/getSingleUserApi",(UserController.getSingleUserApi));
router.post("/editUserSubmit",uploadUsersImage.array('file',1),(UserController.editUserSubmit));

router.post("/updateUserStatusApi",(UserController.updateUserStatusApi));

router.post("/getAdminProfile",(adminController.getAdminProfile ));
router.post("/editAdminProfileSubmitApi",uploadAdminImage.array('file',1),(adminController.editAdminProfileSubmitApi ));
router.post("/adminChangePasswordSubmitApi",(adminController.adminChangePasswordSubmitApi ));


router.post("/getDonationDetail",(UserController.getDonationDetail));
router.post("/allUsersDonation",(UserController.allUsersDonation));
router.post("/allUsersDonationCount",(UserController.allUsersDonationCount));
router.get("/exportCsvUsersDonationApi",(UserController.exportCsvUsersDonationApi));

router.post("/allRecentDonationDashboard",(UserController.allRecentDonationDashboard));
router.post("/submitProjectPayment",StripePaymentController.submitProjectPayment);
router.post("/submitProjectPaymentSecond",StripePaymentController.submitProjectPaymentSecond);
router.post("/submitProjectPaymentThird",StripePaymentController.submitProjectPaymentThird);


module.exports = router;
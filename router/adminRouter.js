const express = require("express");
const router = express.Router();
const adminController = require("../controllers/admin/adminController");
const CategoryController = require("../controllers/admin/CategoryController");
//var multer  = require('multer');
var bodyParser = require('body-parser');
 const modelsImage = require('../middleware/modelsImage');
//const bodyParser = require('body-parser');
var path = require('path')
var multer  = require('multer')
 
var storage = multer.diskStorage({

   destination: function (req, file, cb) {
   	console.log("here");
      cb(null, './public/uploads/models/');
   },
   filename: function (req, file, cb) {
    //console.log("updateProfile"+userEditProfile.updateProfile);
    var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'models_'+ dynamicFileName + '-' + file.originalname);
   }

});
var upload = multer({ storage: storage });


const storage2Admin = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/category/');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadArrayAdmin = multer({ 
  storage: storage2Admin
});

const storageProject = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/projects/');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadProject = multer({ 
  storage: storageProject
});

const storageGallery = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/gallery/images');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadGallery = multer({ 
  storage: storageGallery
});

const storageGalleryVideo = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/gallery/video');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadGalleryVideo = multer({ 
  storage: storageGalleryVideo
});
const storageuploadCategory = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads/subcategory');
    },
  
    filename: function(req, file, cb) {
        var dynamicFileName = Math.floor(Math.random() * Date.now());
      cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
    }
});
var uploadCategory = multer({ 
  storage: storageuploadCategory
});

  

/*var upload = multer({ 
    storage: storage,
    fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      //return cb("image extension error");
   
    }
  }
});
*/
router.post("/adminLogin",adminController.adminLogin);
router.post("/adminRegistration",adminController.adminRegistration);


//router.post("/updateUserProfile",auth,upload.single('userImage'),userEditProfile.updateProfile,userController.updateUserProfile);
//router.post("/adminLoginCheck",adminController.adminLoginCheck);

router.post("/addCategorySubmit",uploadArrayAdmin.fields([{
           name: 'file', maxCount: 10
         }, {
           name: 'video', maxCount: 1
         }]),CategoryController.addCategorySubmit);
router.post('/addProjectSubmit', uploadProject.array('file',1),CategoryController.addProjectSubmit);
router.post('/addGalleryImageSubmit', uploadGallery.array('file',100),adminController.addGalleryImageSubmit);
router.post('/addGalleryVideoSubmit', uploadGalleryVideo.array('file',100),adminController.addGalleryVideoSubmit);
router.post("/getAdminCategory",(CategoryController.getAdminCategory));
router.post("/addSubCategorySubmit",uploadCategory.array('file',1),(CategoryController.addSubCategorySubmit));
/*
router.post("/allCategory",(CategoryController.allCategory));
router.post("/categoryTotalCount",(CategoryController.categoryTotalCount));*/


module.exports = router;